<?php
// CUSTOM WORDPRESS
//logo
$nd_options_customizer_header_2_logo = get_option('nd_options_customizer_header_2_logo');
if ($nd_options_customizer_header_2_logo == '') {
	$nd_options_customizer_header_2_logo = plugins_url() . '/nd-shortcodes/addons/customizer/header/img/logo.png';
} else {
	$nd_options_customizer_header_2_logo = wp_get_attachment_url($nd_options_customizer_header_2_logo);
}

//logo responsive
$nd_options_customizer_header_2_logo_responsive = get_option('nd_options_customizer_header_2_logo_responsive');
if ($nd_options_customizer_header_2_logo_responsive == '') {
	$nd_options_customizer_header_2_logo_responsive = plugins_url() . '/nd-shortcodes/addons/customizer/header/img/logo.png';
} else {
	$nd_options_customizer_header_2_logo_responsive = wp_get_attachment_url($nd_options_customizer_header_2_logo_responsive);
}


//icon menu responsive
$nd_options_customizer_header_2_icon_responsive_menu = get_option('nd_options_customizer_header_2_icon_responsive_menu');
if ($nd_options_customizer_header_2_icon_responsive_menu == '') {
	$nd_options_customizer_header_2_icon_responsive_menu = plugins_url() . '/nd-shortcodes/addons/customizer/header/img/icon-menu.svg';
} else {
	$nd_options_customizer_header_2_icon_responsive_menu = wp_get_attachment_url($nd_options_customizer_header_2_icon_responsive_menu);
}


$nd_options_customizer_header_2_logo_width = get_option('nd_options_customizer_header_2_logo_width');
if ($nd_options_customizer_header_2_logo_width == '') {
	$nd_options_customizer_header_2_logo_width = '170';
}

$nd_options_customizer_header_2_logo_margin_top = get_option('nd_options_customizer_header_2_logo_margin_top');
if ($nd_options_customizer_header_2_logo_margin_top == '') {
	$nd_options_customizer_header_2_logo_margin_top = '24';
}


//get all datas
$nd_options_customizer_header_2_margin = get_option('nd_options_customizer_header_2_margin');
if ($nd_options_customizer_header_2_margin == '') {
	$nd_options_customizer_header_2_margin = '10';
}

$nd_options_customizer_header_2_bg = get_option('nd_options_customizer_header_2_bg');
if ($nd_options_customizer_header_2_bg == '') {
	$nd_options_customizer_header_2_bg = '#ffffff';
}

$nd_options_customizer_header_2_bg_responsive = get_option('nd_options_customizer_header_2_bg_responsive');
if ($nd_options_customizer_header_2_bg_responsive == '') {
	$nd_options_customizer_header_2_bg_responsive = '#000';
}

$nd_options_customizer_header_2_bg_transparent = get_option('nd_options_customizer_header_2_bg_transparent');
if ($nd_options_customizer_header_2_bg_transparent == '') {
	$nd_options_customizer_header_2_bg_transparent = 0;
}

$nd_options_customizer_header_2_menu_color = get_option('nd_options_customizer_header_2_menu_color');
if ($nd_options_customizer_header_2_menu_color == '') {
	$nd_options_customizer_header_2_menu_color = '#727475';
}

$nd_options_customizer_header_2_divider_color = get_option('nd_options_customizer_header_2_divider_color');
if ($nd_options_customizer_header_2_divider_color == '') {
	$nd_options_customizer_header_2_divider_color = '#f1f1f1';
}

//top header
$nd_options_customizer_top_header_2_bg = get_option('nd_options_customizer_top_header_2_bg');
if ($nd_options_customizer_top_header_2_bg == '') {
	$nd_options_customizer_top_header_2_bg = '#444444';
}

$nd_options_customizer_top_header_2_text_color = get_option('nd_options_customizer_top_header_2_text_color');
if ($nd_options_customizer_top_header_2_text_color == '') {
	$nd_options_customizer_top_header_2_text_color = '#a3a3a3';
}

$nd_options_customizer_top_header_2_left_content = get_option('nd_options_customizer_top_header_2_left_content');
if ($nd_options_customizer_top_header_2_left_content == '') {
	$nd_options_customizer_top_header_2_left_content = 'ADD SOME TEXT THROUGH CUSTOMIZER';
}

$nd_options_customizer_top_header_2_right_content = get_option('nd_options_customizer_top_header_2_right_content');
if ($nd_options_customizer_top_header_2_right_content == '') {
	$nd_options_customizer_top_header_2_right_content = 'ADD SOME TEXT THROUGH CUSTOMIZER';
}

$nd_options_customizer_top_header_2_display = get_option('nd_options_customizer_top_header_2_display');
if ($nd_options_customizer_top_header_2_display == '') {
	$nd_options_customizer_top_header_2_display = '0';
}



//get font family H
$nd_options_customizer_font_family_h = get_option('nd_options_customizer_font_family_h', 'Montserrat:400,700');
$nd_options_font_family_h_array = explode(":", $nd_options_customizer_font_family_h);
$nd_options_font_family_h = str_replace("+", " ", $nd_options_font_family_h_array[0]);
$nd_options_customizer_font_color_h = get_option('nd_options_customizer_font_color_h', '#727475');
?>

<div id="nd_options_site_filter"></div>

<!--START js-->
<script type="text/javascript">
//<![CDATA[

	jQuery(document).ready(function () {

		//START
		jQuery(function ($) {

			//OPEN sidebar content ( navigation 2 )
			$('.nd_options_open_navigation_2_sidebar_content,.nd_options_open_navigation_3_sidebar_content,.nd_options_open_navigation_4_sidebar_content,.nd_options_open_navigation_5_sidebar_content').on("click", function (event) {
				$('.nd_options_navigation_2_sidebar_content,.nd_options_navigation_3_sidebar_content,.nd_options_navigation_4_sidebar_content,.nd_options_navigation_5_sidebar_content').css({
					'right': '0px',
				});
			});
			//CLOSE	sidebar content ( navigation 2 )
			$('.nd_options_close_navigation_2_sidebar_content,.nd_options_close_navigation_3_sidebar_content,.nd_options_close_navigation_4_sidebar_content,.nd_options_close_navigation_5_sidebar_content').on("click", function (event) {
				$('.nd_options_navigation_2_sidebar_content,.nd_options_navigation_3_sidebar_content,.nd_options_navigation_4_sidebar_content,.nd_options_navigation_5_sidebar_content').css({
					'right': '-300px'
				});
			});
			///////////


		});
		//END

	});

//]]>
</script>
<!--END js-->




<?php do_action('nd_options_hook_start_navigation'); ?>



<!--START menu responsive-->
<div style="background-color: <?php echo $nd_options_customizer_header_2_bg_responsive; ?> ;" class="nd_options_navigation_2_sidebar_content nd_options_padding_40 nd_options_box_sizing_border_box nd_options_overflow_hidden nd_options_overflow_y_auto nd_options_transition_all_08_ease nd_options_height_100_percentage nd_options_position_fixed nd_options_width_300 nd_options_right_300_negative nd_options_z_index_999">

    <img alt="" width="25" class="nd_options_close_navigation_2_sidebar_content nd_options_cursor_pointer nd_options_right_20 nd_options_top_20 nd_options_position_absolute" src="<?php echo plugins_url(); ?>/nd-shortcodes/addons/customizer/header/header-2/img/icon-close-white.svg">

    <div class="nd_options_navigation_2_sidebar">
		<?php wp_nav_menu(array('menu' => '36')); ?>
    </div>

</div>
<!--END menu responsive-->




<?php if ($nd_options_customizer_top_header_2_display != 1) { ?>

	<!--start TOP header-->
	<div class="nd_options_section">

		<div id="nd_options_navigation_2_top_header" style="background-color: <?php echo $nd_options_customizer_top_header_2_bg; ?> ;" class="nd_options_section">

			<!--start nd_options_container-->
			<div class="nd_options_container nd_options_clearfix">

				<div style="color: <?php echo $nd_options_customizer_top_header_2_text_color; ?> ;" class="nd_options_grid_6 nd_options_padding_botttom_10 nd_options_padding_bottom_0_responsive nd_options_padding_top_10 nd_options_text_align_center_responsive">
					<div id="nd_options_navigation_top_header_2_left" class="nd_options_navigation_top_header_2 nd_options_display_inline_block_responsive">
						<div class=" nd_options_display_table nd_options_float_left">


							<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
								<a href="">
									<img alt="" width="15" height="15" class="nd_options_margin_right_10 nd_options_float_left" src="/wp-content/plugins/nd-shortcodes/addons/customizer/shortcodes/top-header/img/world-grey.svg">
								</a>

							</div>

							<div class="nd_options_display_table_cell nd_options_vertical_align_middle">
								<!--Mexico, Perú, Chile, Argetina, Colombia, Costa Rica, República Dominicana, Honduras, Nicaragua, Panamá, Paraguay, Uruguay, Venezuela-->
<!--								<div class="switcher2 notranslate">
									<div class="selected">
										<?php
											$country = get_current_country($_SERVER['HTTP_HOST']);
										?>
											<a href="#" onclick="return false;" class="">
												<img src="/wp-content/plugins/gtranslate/flags/16/<?php echo $country['flag'] ?>.png" height="16" width="16" alt="<?php echo $country['country'] ?>"> <?php echo $country['country'] ?>
											</a>
									</div>
									<div class="option" style="display: none;">
										<a href="http://aulasmarteditorial.mx" title="México" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-mx.png" height="16" width="16" alt="mx">
											México
										</a>
										<a href="http://aulasmarteditorial.pe" title="Perú" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-pe.png" height="16" width="16" alt="mx">
											Perú
										</a>
										<a href="http://aulasmarteditorial.cl" title="Chile" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-ch.png" height="16" width="16" alt="mx">
											Chile
										</a>
										<a href="http://aulasmarteditorial.arg" title="Argentina" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-ar.png" height="16" width="16" alt="mx">
											Argentina
										</a>
										<a href="http://aulasmarteditorial.co" title="Colombia" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-co.png" height="16" width="16" alt="mx">
											Colombia
										</a>
										<a href="http://cr.aulasmarteditorial.com" title="Costa Rica" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-cr.png" height="16" width="16" alt="mx">
											Costa Rica
										</a>
										<a href="http://aulasmarteditorial.es" title="España" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-es.png" height="16" width="16" alt="es">
											España
										</a>
										<a href="http://hn.aulasmarteditorial.com" title="Honduras" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-ho.png" height="16" width="16" alt="mx">
											Honduras
										</a>
										<a href="http://ni.aulasmarteditorial.com" title="Nicaragua" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-ni.png" height="16" width="16" alt="mx">
											Nicaragua
										</a>
										<a href="http://pa.aulasmarteditorial.com" title="Panamá" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-pa.png" height="16" width="16" alt="mx">
											Panamá
										</a>
										<a href="http://py.aulasmarteditorial.com" title="Paraguay" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-pr.png" height="16" width="16" alt="mx">
											Paraguay
										</a>
										<a href="http://rd.aulasmarteditorial.com" title="República Dominicana" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-do.png" height="16" width="16" alt="mx">
											República Dominicana
										</a>
										<a href="http://uy.aulasmarteditorial.com" title="Uruguay" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-ur.png" height="16" width="16" alt="mx">
											Uruguay
										</a>
										<a href="http://ve.aulasmarteditorial.com" title="Venezuela" class="nturl">
											<img src="/wp-content/plugins/gtranslate/flags/16/es-ve.png" height="16" width="16" alt="mx">
											Venezuela
										</a>
									</div>
								</div>-->
								<script type="text/javascript" src="/wp-content/plugins/gtranslate/js/custom-gtranslate.js"></script>


								<div id="google_translate_element2"><div class="skiptranslate goog-te-gadget" dir="ltr"><div id=":0.targetLanguage"><select class="goog-te-combo"><option value="">Seleccionar idioma</option><option value="af">afrikáans</option><option value="sq">albanés</option><option value="de">alemán</option><option value="am">amhárico</option><option value="ar">árabe</option><option value="hy">armenio</option><option value="az">azerí</option><option value="bn">bengalí</option><option value="be">bielorruso</option><option value="my">birmano</option><option value="bs">bosnio</option><option value="bg">búlgaro</option><option value="km">camboyano</option><option value="kn">canarés</option><option value="ca">catalán</option><option value="ceb">cebuano</option><option value="cs">checo</option><option value="ny">chichewa</option><option value="zh-CN">chino (simplificado)</option><option value="zh-TW">chino (tradicional)</option><option value="si">cingalés</option><option value="ko">coreano</option><option value="co">corso</option><option value="ht">criollo haitiano</option><option value="hr">croata</option><option value="da">danés</option><option value="sk">eslovaco</option><option value="sl">esloveno</option><option value="eo">esperanto</option><option value="et">estonio</option><option value="eu">euskera</option><option value="fi">finlandés</option><option value="fr">francés</option><option value="fy">frisio</option><option value="gd">gaélico escocés</option><option value="cy">galés</option><option value="gl">gallego</option><option value="ka">georgiano</option><option value="el">griego</option><option value="gu">gujarati</option><option value="ha">hausa</option><option value="haw">hawaiano</option><option value="iw">hebreo</option><option value="hi">hindi</option><option value="hmn">hmong</option><option value="hu">húngaro</option><option value="ig">igbo</option><option value="id">indonesio</option><option value="en">inglés</option><option value="ga">irlandés</option><option value="is">islandés</option><option value="it">italiano</option><option value="ja">japonés</option><option value="jw">javanés</option><option value="kk">kazajo</option><option value="ky">kirguís</option><option value="ku">kurdo</option><option value="lo">lao</option><option value="la">latín</option><option value="lv">letón</option><option value="lt">lituano</option><option value="lb">luxemburgués</option><option value="mk">macedonio</option><option value="ml">malayalam</option><option value="ms">malayo</option><option value="mg">malgache</option><option value="mt">maltés</option><option value="mi">maorí</option><option value="mr">maratí</option><option value="mn">mongol</option><option value="nl">neerlandés</option><option value="ne">nepalí</option><option value="no">noruego</option><option value="pa">panyabí</option><option value="ps">pastún</option><option value="fa">persa</option><option value="pl">polaco</option><option value="pt">portugués</option><option value="ro">rumano</option><option value="ru">ruso</option><option value="sm">samoano</option><option value="sr">serbio</option><option value="st">sesoto</option><option value="sn">shona</option><option value="sd">sindhi</option><option value="so">somalí</option><option value="sw">suajili</option><option value="sv">sueco</option><option value="su">sundanés</option><option value="tl">tagalo</option><option value="th">tailandés</option><option value="ta">tamil</option><option value="tg">tayiko</option><option value="te">telugu</option><option value="tr">turco</option><option value="uk">ucraniano</option><option value="ur">urdu</option><option value="uz">uzbeco</option><option value="vi">vietnamita</option><option value="xh">xhosa</option><option value="yi">yidis</option><option value="yo">yoruba</option><option value="zu">zulú</option></select></div>Con la tecnología de <span style="white-space:nowrap"><a class="goog-logo-link" href="https://translate.google.com" target="_blank"><img src="https://www.gstatic.com/images/branding/googlelogo/1x/googlelogo_color_42x16dp.png" width="37px" height="14px" style="padding-right: 3px" alt="Google Traductor de Google">Traductor de Google</a></span></div></div>
								<script type="text/javascript">
									function googleTranslateElementInit2() {
										new google.translate.TranslateElement({pageLanguage: 'es', autoDisplay: false}, 'google_translate_element2');}
								</script>
								<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>

							</div>

							<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
								<?php echo do_shortcode('[gtranslate]'); ?>
							</div>
							
							
							<script type="text/javascript">
								// Si el pais no es España, ocultamos los idiomas propios de España
								var country = <?php print_r(json_encode($country)); ?>;
								if(country && country.country != "España"){
									var options = $('.switcher.notranslate .option a');
									for (var i = 0; i < options.length; i++) {
										if(options[i].title != "Español"){
											$(options[i]).css( "display", "none" );
										}
									}
								}
							</script>
						</div>
						
						
						<?php
						// echo do_shortcode($nd_options_customizer_top_header_2_left_content);
						?>


						<div class="nd_options_display_table nd_options_float_left">
							<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
								<a class="" target="_self"><img alt="" width="15" height="15" class="nd_options_margin_right_10 nd_options_float_left" src="/wp-content/plugins/nd-shortcodes/addons/customizer/shortcodes/top-header/img/share-grey.svg"></a>
							</div>
							<div class=" nd_options_display_none_all_iphone nd_options_display_table_cell nd_options_vertical_align_middle    ">
								<a style="" class="nd_options_margin_right_20"><?php echo __('SIGUENOS', 'custom-translation') ?></a>
							</div>
							<div class="  nd_options_display_table nd_options_float_left">
								<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
									<a href="https://www.facebook.com/fundacionaulasmart/" target="_blank"><img alt="" width="15" height="15" class="nd_options_margin_right_10 nd_options_float_left" src="/wp-content/plugins/nd-shortcodes/addons/customizer/shortcodes/top-header/img/facebook-grey.svg"></a>
								</div>
							</div>
							<div class="  nd_options_display_table nd_options_float_left">
								<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
									<a href="https://twitter.com/AULA_SMART" target="_blank"><img alt="" width="15" height="15" class="nd_options_margin_right_10 nd_options_float_left" src="/wp-content/plugins/nd-shortcodes/addons/customizer/shortcodes/top-header/img/twitter-grey.svg"></a>
								</div>
							</div>
							<div class="  nd_options_display_table nd_options_float_left">
								<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
									<a href="https://www.linkedin.com/company/fundaci%C3%B3n-aula-smart" target="_blank"><img alt="" width="15" height="15" class="nd_options_margin_right_10 nd_options_float_left" src="/wp-content/plugins/nd-shortcodes/addons/customizer/shortcodes/top-header/img/linkedin-grey.svg"></a>
								</div>
							</div>
							<div class="  nd_options_display_table nd_options_float_left">
								<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
									<a href="https://www.youtube.com/channel/UCemJy2V8SKhxBxMTuSMStKQ" target="_blank"><img alt="" width="15" height="15" class="nd_options_margin_right_10 nd_options_float_left" src="/wp-content/plugins/nd-shortcodes/addons/customizer/shortcodes/top-header/img/youtube-grey-2.svg"></a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div style="color: <?php echo $nd_options_customizer_top_header_2_text_color; ?> ;" class="nd_options_grid_6 nd_options_text_align_right nd_options_text_align_center_responsive nd_options_padding_top_0_responsive nd_options_padding_botttom_10 nd_options_padding_top_10">
					<!--	           		<div id="nd_options_navigation_top_header_2_right" class="nd_options_navigation_top_header_2 nd_options_display_inline_block_responsive">
								<?php
// echo do_shortcode($nd_options_customizer_top_header_2_right_content);  
								?>
										</div>-->

					<div id="nd_options_navigation_top_header_2_right" class="nd_options_navigation_top_header_2 nd_options_display_inline_block_responsive">

						<div class="  nd_options_display_table nd_options_float_right">
							<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
								<a><img alt="" width="15" height="15" class="nd_options_margin_left_20 nd_options_float_left" src="/wp-content/plugins/nd-shortcodes/addons/customizer/shortcodes/top-header/img/login-grey.svg"></a>
							</div>
							<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
								<?php if (isLoginCampus()) { ?>
									<a style="" class="nd_options_margin_left_10" href="<?php echo get_logout_campus_page(); ?>"><?php echo __('SALIR', 'custom-translation') ?></a>
	<?php } else { ?>
									<a style="" class="nd_options_margin_left_10" href="<?php echo get_register_campus_page(); ?>"><?php echo __('REGISTRO', 'custom-translation') ?></a>
	<?php } ?>
							</div>
						</div>

						<div class="  nd_options_display_table nd_options_float_right">
							<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
								<a ><img alt="" width="15" height="15" class="nd_options_margin_left_20 nd_options_float_left" src="/wp-content/plugins/nd-shortcodes/addons/customizer/shortcodes/top-header/img/user-grey.svg"></a>
							</div>
							<div class="nd_options_display_table_cell nd_options_vertical_align_middle">
									<?php if (isLoginCampus()) { ?>
											<a class="nd_options_margin_left_10" href="<?php echo get_profile_campus_page(); ?>"><?php echo __('MICUENTA', 'custom-translation') ?></a>
									<?php } else { ?>
											<a class="nd_options_margin_left_10" href="<?php echo get_login_campus_page(); ?>"><?php echo __('LOGIN', 'custom-translation') ?></a>
									<?php } ?>
							</div>
						</div>
						
						<?php if(!empty($_SESSION['userCampus']['centro_ase']) && $_SESSION['userCampus']['centro_ase']== 1) { ?>
							<div class="  nd_options_display_table nd_options_float_right">
								<div class="nd_options_display_table_cell nd_options_vertical_align_middle    ">
									<!--<a ><img alt="" width="15" height="15" class="nd_options_margin_left_20 nd_options_float_left" src="/wp-content/plugins/nd-shortcodes/addons/customizer/shortcodes/top-header/img/pin-white.svg"></a>-->
									<a ><img alt="" width="15" height="15" class="nd_options_margin_left_20 nd_options_float_left" src="/wp-content/plugins/nd-shortcodes/addons/customizer/header/img/icon-menu.svg"></a>
								</div>
								<div class="nd_options_display_table_cell nd_options_vertical_align_middle">
									<a class="nd_options_margin_left_10 nd_options_text_transform_uppercase" href="<?php echo get_profile_acuerdo(); ?>"><?php echo __('AcuerdoColaboracion', 'custom-translation') ?></a>
								</div>
							</div>
						<?php } ?>

					</div>
				</div>

			</div>
			<!--end container-->

		</div>

	</div>
	<!--END TOP header-->

<?php } ?>





<!--START navigation-->
<div id="nd_options_navigation_2_container" class="nd_options_section nd_options_position_relative ">

    <div style="background-color: <?php echo $nd_options_customizer_header_2_bg; ?> ; border-bottom: 1px solid <?php echo $nd_options_customizer_header_2_divider_color; ?> ;" class="nd_options_section">

        <!--start nd_options_container-->
        <div class="nd_options_container nd_options_clearfix nd_options_position_relative">

            <div class="nd_options_grid_12 nd_options_display_none_all_responsive">

                <div style="height: <?php echo $nd_options_customizer_header_2_margin; ?>px;" class="nd_options_section"></div>

                <!--LOGO-->
                <a href="<?php echo home_url(); ?>">
					<img style="top:<?php echo $nd_options_customizer_header_2_logo_margin_top; ?>px;" alt="" class="nd_options_position_absolute nd_options_left_15" width="<?php echo $nd_options_customizer_header_2_logo_width; ?>" 
						 src="<?php echo $nd_options_customizer_header_2_logo; ?>">
				</a>

                <!--LOGO AENOR-->
                <a>
					<img style="top:<?php echo $nd_options_customizer_header_2_logo_margin_top; ?>px;" alt="" class="nd_options_position_absolute nd_options_left_15 logo-aenor" width="<?php echo $nd_options_customizer_header_2_logo_width; ?>" 
						 src="/wp-content/uploads/logos/Certificacion2.png">
				</a>

                <div class="nd_options_navigation_2 nd_options_navigation_type nd_options_text_align_right nd_options_float_right nd_options_display_none_all_responsive">

                    <div class="nd_options_display_table">
						<div class="nd_options_display_table_cell nd_options_vertical_align_middle">
<?php wp_nav_menu(array('menu' => '36')); ?>
						</div>

<?php do_action('nd_options_hook_icons_navigation'); ?>

					</div>

                </div>






                <div style="height: <?php echo $nd_options_customizer_header_2_margin; ?>px;" class="nd_options_section"></div>

            </div>



            <!--RESPONSIVE-->
			<div class="nd_options_section nd_options_text_align_center nd_options_display_none nd_options_display_block_responsive">
				<div class="nd_options_section nd_options_height_20"></div>
				<!--LOGO-->
				<a class="nd_options_display_inline_block" href="<?php echo home_url(); ?>">
					<img alt="" class="nd_options_float_left" width="<?php echo $nd_options_customizer_header_2_logo_width; ?>" 
						 src="<?php echo $nd_options_customizer_header_2_logo_responsive; ?>">
				</a>
				<!--LOGO AENOR-->
				<a class="nd_options_display_inline_block" href="<?php echo home_url(); ?>">
					<img alt="" class="nd_options_float_left logo-aenor" width="<?php echo $nd_options_customizer_header_2_logo_width; ?>" 
						 src="/wp-content/uploads/logos/Certificacion2.png">
				</a>
				<div class="nd_options_section nd_options_height_10"></div>

				<div class="nd_options_section">
					<a class="nd_options_open_navigation_2_sidebar_content nd_options_open_navigation_2_sidebar_content" href="#">
						<img alt="" class="" width="25" src="<?php echo $nd_options_customizer_header_2_icon_responsive_menu; ?>">
					</a>
				</div>

				<div class="nd_options_section nd_options_height_20"></div>
			</div>
			<!--RESPONSIVE-->



        </div>
        <!--end container-->

    </div>


</div>
<!--END navigation-->



