<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php // if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<!--<span class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span></span>-->

	<?php // endif; ?>

	<?php // echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php // echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

	
	 <!--CUSTOM META-->
	<?php
	
		$attributes = $product->get_attributes();
		$ids = wc_get_product_terms( $product->id, $attributes['pa_cursosrelacionados']['name'], array( 'fields' => 'names' ) );
		$dates_course = array();
		foreach($ids as $id){
			$array = array();
			$d = get_next_date_course($id);
			$title = get_the_title($id);
			array_push($array, $id);
			array_push($array, $d);
			array_push($array, $title);
			
			if(!empty($d)){
				array_push($dates_course, $array);
			}
		}
		$dates_course=sortByDate($dates_course);

//		echo '<pre>';
//		print_r($dates_course);
//		echo '</pre>';
		
		
		$nd_learning_checkout_page = get_option('nd_learning_checkout_page');
		$nd_learning_checkout_page_url = get_permalink($nd_learning_checkout_page);

		$nd_learning_booking_button_single_course_page .= '

		<form class="no-login" action="'.$nd_learning_checkout_page_url .'" method="post">
			<h4>Fechas de inicio</h4>
			<select  id="selectIdCourse" onchange="updateValues()" class="nd_learning_width_100_percentage nd_learning_cursor_pointer" style="background-color: #f9f9f9;" name="nd_learning_id_course">';
				foreach($dates_course as $course){
					$nd_learning_booking_button_single_course_page .= '<option value="'.$course[0].'">'.$course[1].' - '.$course[2].'</option>';
				}
		$nd_learning_booking_button_single_course_page .= '
			</select>
		  <input type="hidden" id="nd_learning_date_course" name="date" value="">
		  <input class="nd_learning_width_100_percentage nd_learning_cursor_pointer" type="submit" value="'.__('BOOK','nd-learning').'">
		</form>';
		
		
		$nd_learning_booking_button_single_course_page .= '
			<script>
				function updateValues(){
					var select = document.getElementById("selectIdCourse")
					var text = select.options[select.selectedIndex].text;
					var date = text.split(" - ")[0];
					document.getElementById("nd_learning_date_course").value = date;
				}
			</script>
			';
		echo $nd_learning_booking_button_single_course_page;

	?>
</div>
