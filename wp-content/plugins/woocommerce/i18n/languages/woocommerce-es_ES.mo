��    '      T  5   �      `     a     x     �     �     �     �     �     �  '   �          &     7     C     Q     g     |     �     �  	   �     �     �     �     �     �     �     �     
       J   +     v  
   }     �     �     �     �  8   �     �       �       �     �     �     �       	        %     9  4   M     �     �     �     �     �     �     �     	     %	  	   <	     F	     Y	     p	     y	  
   �	     �	     �	     �	     �	  >   �	     
     $
  	   9
     C
  	   \
  	   f
  *   p
     �
  
   �
                                                                   	                 '          %      !   $           "                                
          &   #                  %s product %s products Add a review Add products Add to cart Additional Information Admin menu nameTags All Product Tags All Products Be the first to review &ldquo;%s&rdquo; Category: Categories: Choose an option Description List of tags. No Product Tags found Popular Product Tags Product Category Product Description Product Tags Read more Recent Reviews Related Products Reviews Reviews (%d) SKU SKU: Search Product Tags Select a category Select options Sorry, this product is unavailable. Please choose a different combination. Submit Tag: Tags: Tags There are no reviews yet. View Product View products You must be <a href="%s">logged in</a> to post a review. Your Rating Your Review Project-Id-Version: WooCommerce 2.6.14
Report-Msgid-Bugs-To: https://github.com/woothemes/woocommerce/issues
POT-Creation-Date: 2017-04-01 16:50+0200
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-04-24 17:51+0200
Language-Team: LANGUAGE <EMAIL@ADDRESS>
X-Generator: Poedit 1.8.12
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_ES
 producto productos Añadir reseña Añadir productos Añadir al carrito Información adicional Etiquetas Todas las etiquetas Todos los productos Sé el primero en dejar una reseña &ldquo;%s&rdquo; Categoría Categorías Elija una opción Descripción Listado de etiquetas No hay etiquetas Etiquetas populares Categoría del producto Descripción del producto Etiquetas del producto Leer más Reseñas recientes Productos relacionados Reseñas Reseñas (%d) Referencia Referencia: Buscador de etiquetas Escoja una categoría Selecciona opciones Perdone, este producto no esta disponible. Escoja otra opción Enviar Etiqueta: Etiquetas: Etiquetas No hay reseñas todavía Leer más Leer más Debe estar logueado para dejar una reseña Tu puntuación Tu reseña 