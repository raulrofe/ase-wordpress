<?php

$nd_learning_str .= '

  <div class="' . $nd_learning_class . ' nd_learning_section">

    ' . $nd_learning_title_output . '


    <form class="" action="' . $nd_learning_action . '" method="GET">

      <input type="hidden" value="true" name="nd_learning_arrive_from_advsearch">

    ';


//get all taxonmies
$nd_learning_taxonomies = get_object_taxonomies($nd_learning_posttype);

//call the functions for each tax
$nd_learning_i = 0;
foreach ($nd_learning_taxonomies as $nd_learning_tax) {

	$nd_learning_str .= nd_learning_build_select($nd_learning_tax, $nd_learning_i, '', $nd_learning_layout, $nd_learning_width);
	$nd_learning_i = $nd_learning_i + 1;
}

// CUSTOM WORDPRESS
// ITINERARIOS
global $wpdb;

$request = "SELECT * FROM {$wpdb->posts} WHERE post_type='product' AND post_status='publish'";
$products = $wpdb->get_results($request);

$nd_learning_str .= '<div id="nd_learning_search_components_tax_4" class=" nd_learning_width_100_percentage_all_iphone  nd_learning_width_33_percentage nd_learning_float_left nd_learning_float_left nd_learning_padding_15 nd_learning_box_sizing_border_box">
		<select class="nd_learning_section nd_learning_font_size_20 nd_learning_color_white_important nd_learning_background_transparent_important nd_learning_border_radius_3_important nd_learning_border_2_solid_white_important" name="itinerarios">
		<option value="">'.__('Itinerario', 'custom-translation').'</option>
	';
foreach ($products as $product) {
	$nd_learning_str .= '
		<option value="'.$product->ID.'">'.$product->post_title.'</option>
	';
}
$nd_learning_str .= '
		</select>
		</div>
	';



$nd_learning_str .= '

      <div class="nd_learning_width_100_percentage nd_learning_text_align_center nd_learning_float_left nd_learning_padding_15 nd_learning_box_sizing_border_box">
        <input class="nd_learning_section nd_learning_width_initial nd_learning_float_none" type="submit" value="' . __('SEARCH', 'nd-learning') . '">
      </div>


    </form>

</div>
';
