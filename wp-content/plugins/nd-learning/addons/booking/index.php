<?php
// CUSTOM WORDPRESS


// FUNCIONES AUXILIARES
if (!function_exists('notification')) {
	function notification($status, $msg) {
		$_SESSION['notification']['msg'] = $msg;
		$_SESSION['notification']['status'] = $status;
	}
}

if (!function_exists('redirectBack')) {
	function redirectBack($steps = 1) {
		echo "<script type='text/javascript'>window.history.go(-".$steps.")</script>";
		exit();
	}
}

//page admin settings
require_once dirname( __FILE__ ) . '/inc/admin/admin-settings.php';



//START add book button to single course page
add_action('nd_learning_sidebar_single_course_page_3','nd_learning_add_booking_button_single_course_page');
function nd_learning_add_booking_button_single_course_page(){

	//recover datas from plugin settings
	$nd_learning_checkout_page = get_option('nd_learning_checkout_page');
//	$nd_learning_checkout_page_url = get_permalink($nd_learning_checkout_page);
	$nd_learning_checkout_page_url = get_checkout_page();

  //data to be passed to function
  $nd_learning_course_id = get_the_ID();
  $nd_learning_current_user = wp_get_current_user();
  $nd_learning_current_user_id = $nd_learning_current_user->ID;

  //metabox
  $nd_learning_meta_box_max_availability = get_post_meta( $nd_learning_course_id, 'nd_learning_meta_box_max_availability', true );

  
  
	//declare
	$nd_learning_booking_button_single_course_page = '';


	//check if the user is logged
  //  if (is_user_logged_in() == 1){
  if(isLoginCampus()){



	  if ( nd_learning_check_user_purchased_free_course($nd_learning_course_id, $nd_learning_current_user_id) === 0 ){


		  if ( $nd_learning_meta_box_max_availability - nd_learning_get_all_orders_by_id($nd_learning_course_id) === 0 ) {

				  $nd_learning_booking_button_single_course_page .= '

				  <form class="logged">
					<input class="nd_learning_width_100_percentage" type="submit" disabled value="'.__('COURSE COMPLETED','nd-learning').'">
				  </form>';

		  }else{

				  $dates_course = get_dates_course();

				  $nd_learning_booking_button_single_course_page .= '

					<form id="formulario-fechas" class="logged" action="'. $nd_learning_checkout_page_url.'" method="post">
					  <h4>'.__('FechaInicio', 'custom-translation').'</h4>
					  <select data-dates-course="1" class="nd_learning_width_100_percentage nd_learning_cursor_pointer" style="background-color: #f9f9f9;" name="date">';
						  $nd_learning_booking_button_single_course_page .= '<option value="" selected="">'. __('SeleccioneFecha', 'custom-translation').'</option>';
						  foreach($dates_course as $date){
							  if($date['homologable']=='on'){
								  $nd_learning_booking_button_single_course_page .= '<option class="homologable" value="'.$date['date'].'">'.$date['date'].'</option>';
							  }else{
								  $nd_learning_booking_button_single_course_page .= '<option value="'.$date['date'].'">'.$date['date'].'</option>';
							  }
						  }
				  $nd_learning_booking_button_single_course_page .= '
					  </select>
					  <p class="info-homologable">
						  <span></span> Fechas homologables
					  </p>
					  <input type="hidden" name="nd_learning_id_course" value="'.get_the_ID().'">
					  <input type="hidden" id="input-homologado" name="input-homologado" value="false">
					  <input class="nd_learning_width_100_percentage nd_learning_cursor_pointer nd_learning_margin_top_20" type="submit" value="'.__('BOOK','nd-learning').'">
					</form>

				  ';
		  }

	  }else{

		$nd_learning_booking_button_single_course_page .= '

		  <form class="logged">
			<input class="nd_learning_width_100_percentage" type="submit" disabled value="'.__('ALREADY BOOKED','nd-learning').'">
		  </form>

		';

	  }

	}else{


			$dates_course = get_dates_course();

			$nd_learning_booking_button_single_course_page .= '

			<form id="formulario-fechas" class="no-login" action="'.$nd_learning_checkout_page_url .'" method="post">
				<h4>'.__('FechaInicio', 'custom-translation').'</h4>
				<select data-dates-course="1" class="nd_learning_width_100_percentage nd_learning_cursor_pointer" style="background-color: #f9f9f9;" name="date">';
					$nd_learning_booking_button_single_course_page .= '<option value="" selected="">'. __('SeleccioneFecha', 'custom-translation').'</option>';

					foreach($dates_course as $date){
						if($date['homologable']=='on'){
							$nd_learning_booking_button_single_course_page .= '<option class="homologable" value="'.$date['date'].'">'.$date['date'].'</option>';
						}else{
							$nd_learning_booking_button_single_course_page .= '<option value="'.$date['date'].'">'.$date['date'].'</option>';
						}
					}
			$nd_learning_booking_button_single_course_page .= '
				</select>

				<p class="info-homologable">
					<span></span> Fechas homologables
				</p>

				<input type="hidden" name="nd_learning_id_course" value="'.get_the_ID().'">
				<input type="hidden" id="input-homologado" name="input-homologado" value="false">
				<input class="nd_learning_width_100_percentage nd_learning_cursor_pointer" type="submit" value="'.__('BOOK','nd-learning').'">
			</form>';

	}
	//end if the user is logged



		
			echo $nd_learning_booking_button_single_course_page;  

  }
//END




//Start nd_learning_check_user_purchased_free_course
function nd_learning_check_user_purchased_free_course($nd_learning_course_id, $nd_learning_current_user_id){

  global $wpdb;

  $nd_learning_table_name = $wpdb->prefix . 'nd_learning_courses';
  $nd_learning_action_type = "'free'";

  //START select for items
  $nd_learning_order_ids = $wpdb->get_results( "SELECT id_course FROM $nd_learning_table_name WHERE id_user = $nd_learning_current_user_id AND action_type = $nd_learning_action_type AND id_course = $nd_learning_course_id");

  //no results
  if ( empty($nd_learning_order_ids) ) { 

    return 0;
    
  }else{

    return 1;

  }
  //END select for items

}
//END




//START  nd_learning_checkout
function nd_learning_shortcode_checkout() {

  
  //recover datas from plugin settings
  $nd_learning_checkout_page = get_option('nd_learning_checkout_page');
  $nd_learning_checkout_page_url = get_permalink($nd_learning_checkout_page);

  $nd_learning_account_page = get_option('nd_learning_account_page');
  $nd_learning_account_page_url = get_permalink($nd_learning_account_page);

  $nd_learning_paypal_email = get_option('nd_learning_paypal_email');
  $nd_learning_paypal_currency = get_option('nd_learning_paypal_currency');
  $nd_learning_paypal_token = get_option('nd_learning_paypal_token');

  $nd_learning_paypal_developer = get_option('nd_learning_paypal_developer');
  if ( $nd_learning_paypal_developer == 1) {
    $nd_learning_paypal_action_1 = 'https://www.sandbox.paypal.com/cgi-bin';
    $nd_learning_paypal_action_2 = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; 
  }
  else{  
    $nd_learning_paypal_action_1 = 'https://www.paypal.com/cgi-bin';
    $nd_learning_paypal_action_2 = 'https://www.paypal.com/cgi-bin/webscr';
  }


  $nd_learning_shortcode_checkout = '';

  
  //START IF 1 - check if the user is logged
//  if (is_user_logged_in() == 1){
  if (isLoginCampus()){

    
    //START IF 2 - if arrive from course single page
    if ( isset( $_POST['nd_learning_id_course']) ){
      
		// Si no llega la fecha del curso por POST
		if(!empty($_POST["date"])){

			$nd_learning_id_course = $_POST['nd_learning_id_course'];
			$input_homologado = $_POST['input-homologado'];

			//recover datas from plugin settings
			$nd_learning_settings_paypal = get_option('nd_learning_paypal');
			$nd_learning_settings_currency = get_option('nd_learning_currency');

			//get all user informations
			$nd_learning_current_user = wp_get_current_user();
			$nd_learning_current_user_login =  $_SESSION['userCampus']['email'];
			$nd_learning_current_user_email =  $_SESSION['userCampus']['email'];
			$nd_learning_current_user_firstname =  ucwords(strtolower($_SESSION['userCampus']['nombre']));
			$nd_learning_current_user_lastname =  ucwords(strtolower($_SESSION['userCampus']['apellido1'])).' '.ucwords(strtolower($_SESSION['userCampus']['apellido2']));
			$nd_learning_current_user_display_name =  ucwords(strtolower($_SESSION['userCampus']['nombre']));
			$nd_learning_current_user_id =  $_SESSION['userCampus']['idalumnos'];

			//default wp value
			$nd_learning_title_course = get_the_title($nd_learning_id_course);

			//metabox course
			$nd_learning_meta_box_price = get_post_meta( $nd_learning_id_course, 'nd_learning_meta_box_price', true );
			$nd_learning_meta_box_date = get_post_meta( $nd_learning_id_course, 'nd_learning_meta_box_date', true );


			$nd_learning_shortcode_checkout .= '


			<div class="nd_learning_section">
				<div class="nd_learning_width_66_percentage nd_learning_float_left nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive">

					<h2><strong>'.__('Main Details ( Go to your account to change datas ) :','nd-learning').'</strong></h2>

					<form action="'.$nd_learning_checkout_page_url.'" method="post">
						<input name="nd_learning_free_id_course" type="hidden" readonly value="'.$nd_learning_id_course.'">
						<input name="nd_learning_free_title_course" type="hidden" readonly value="'.$nd_learning_title_course.'">
						<input name="nd_learning_free_price_course" type="hidden" readonly value="'.$nd_learning_meta_box_price.'">
						<input name="nd_learning_free_user_id" type="hidden" readonly value="'.$nd_learning_current_user_id.'">
						<input name="nd_learning_free_user_login" type="hidden" readonly value="'.$nd_learning_current_user_login.'">
						<input name="nd_learning_free_display_name" type="hidden" readonly value="'.$nd_learning_current_user_display_name.'">

						<div class="nd_learning_section nd_learning_height_20"></div>
						<input class="nd_learning_section" placeholder="'.__('Email','nd-learning').'" name="nd_learning_free_current_user_email" type="email" readonly value="'.$nd_learning_current_user_email.'"></p>
						<div class="nd_learning_section nd_learning_height_20"></div>
						<input class="nd_learning_section" placeholder="'.__('First Name','nd-learning').'" name="nd_learning_free_current_user_firstname" type="text" readonly value="'.$nd_learning_current_user_firstname.' '.$nd_learning_current_user_lastname . '"></p>
					</form>';



			//START if 3 -  course is free or not
//			if ( $nd_learning_meta_box_price == 0 ){
//
//			  $nd_learning_shortcode_checkout .= '
//
//			  <div class="nd_learning_section nd_learning_height_50"></div>
//			  <h2><strong>'.__('Other Details :','nd-learning').'</strong></h2>
//
//			  <div class="nd_learning_section nd_learning_height_20"></div>
//			  <input class="nd_learning_section" name="nd_learning_free_user_country" placeholder="'.__('Country','nd-learning').'" type="text" value=""></p>
//			  <div class="nd_learning_section nd_learning_height_20"></div>
//			  <input class="nd_learning_section" name="nd_learning_free_user_address" placeholder="'.__('Address','nd-learning').'" type="text" value=""></p>
//			  <div class="nd_learning_section nd_learning_height_20"></div>
//			  <input class="nd_learning_section" name="nd_learning_free_user_city" placeholder="'.__('City','nd-learning').'" type="text" value=""></p>
//			  <div class="nd_learning_section nd_learning_height_20"></div>
//			  <input class="nd_learning_section" name="nd_learning_free_user_zip" placeholder="'.__('Zip Code','nd-learning').'" type="text" value=""></p>
//
//			  <div class="nd_learning_section nd_learning_height_50"></div>
//			  <h2><strong>'.__('Book For Free :','nd-learning').'</strong></h2>
//			  <div class="nd_learning_section nd_learning_height_20"></div>
//			  <input type="submit" value="'.__('BOOK','nd-learning').'"></form>';
//
//			//END IF 3 - 
//			}else{

			
			
			
				$nd_learning_shortcode_checkout .= '<div style="height: 150px;" class="clearfix"></div>';
				
				
				

				// ACUERDO 
				$nd_learning_shortcode_checkout .= '<form action="/acuerdo-colaboracion/" method="post">';
				

					if($input_homologado == 'true'){
						$nd_learning_shortcode_checkout .= 
						'<p style="text-align: center; margin: 20px 0px;">'
						.'Según la Orden EDU/2886/2011 el ministerio expedirá certificados o diligencias a las personas habilitadas para la docencia que se encuentren en activo y presenten la ficha de registro y certificado de centro en el que ejercen. Existen unas plazas asignadas a aquellas personas que no habiendo ejercido la docencia cumplen todos los requisitos. Para ello la ficha de registro deberá ir acompañada de la documentación acreditativa de estar habilitado para ejercer la docencia'  
						.'<br>Con carácter general se expedirán los certificados o diligencias siempre que la actividad alcance los 30 participantes.'
						. '</p>';
					}
					
					$nd_learning_shortcode_checkout .=
						'<p>'
						.__('QuieresAcuerdo', 'custom-translation').
						'</p>
						<div class="nd_learning_section nd_learning_height_20"></div>
						<input type="hidden" name="idCurso" value="' . $nd_learning_id_course . '" />
						<input type="hidden" name="nombreCurso" value="' . $nd_learning_title_course . '" />
						<input type="hidden" name="fechaCurso" value="' . $_POST["date"] . '" />
						<input type="hidden" name="input_homologado" value="' . $input_homologado . '" />';

					if($input_homologado == 'true'){
						$nd_learning_shortcode_checkout .= 
						'<input required="true" type="checkbox" name="check-homologado"/> He leido y acepto las condiciones para la homologación<br><br>';
					}
					
					$nd_learning_shortcode_checkout .=
						'<input type="submit" value="'.__('BOOK','nd-learning').'">
						</form>';
//			}
		  //END



			$nd_learning_shortcode_checkout .= '</div>


				<div class="nd_learning_width_33_percentage nd_learning_float_left nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive">

				  <div class="nd_learning_section nd_learning_bg_grey nd_learning_border_1_solid_grey nd_learning_padding_20 nd_learning_box_sizing_border_box">

					<table class="nd_learning_section">
						<thead>
							<tr class="nd_learning_border_bottom_2_solid_grey">
								<td class="nd_learning_padding_20_10 nd_learning_width_25_percentage">
									<h6 class="nd_learning_text_transform_uppercase">'.__('CursoSeleccionado', 'custom-translation').'</h6>    
								</td>
								<td class="nd_learning_padding_20_10 nd_learning_width_75_percentage">   
								</td>
							</tr>
						</thead>
						<tbody>


							<tr class="">
								<td class="nd_learning_padding_20_10">
									<img alt="" class="nd_learning_section" src="'.nd_learning_get_course_image_url($nd_learning_id_course).'">   
								</td>
								<td class="nd_learning_padding_20_10"> 
									<h5><strong>'.$nd_learning_title_course.'</strong></h5>
								</td>
							</tr>


						</tbody>
					</table>





					<table class="nd_learning_section">
						<tbody>
							<tr class="">
								<td class="nd_learning_padding_20_10 nd_learning_width_50_percentage">
									<p>'.__('Fecha', 'custom-translation').'</p>
								</td>
								<td class="nd_learning_padding_20_10 nd_learning_text_align_right nd_learning_width_50_percentage">  
									<p class="nd_learning_color_greydark">'.$_POST["date"].'</p> 
								</td>
							</tr>';

					  $country = get_current_country($_SERVER['HTTP_HOST']);
					  $nd_learning_shortcode_checkout .='
							<tr class="">
								<td class="nd_learning_padding_20_10">
									<p>'.__('Total','nd-learning').'</p>   
								</td>
								<td class="nd_learning_padding_20_10 nd_learning_text_align_right">  
									   <h3><strong>'.
										  currencyr_exchange( array( 'amount' => nd_learning_get_course_price($nd_learning_id_course) , 'to' => $country['money'] ) )
									  .'</strong></h3> 
								</td>
							</tr>
							<tr class="">
								<td class="nd_learning_padding_20_10">
									<p>'.__('Acuerdo', 'custom-translation').'</p>   
								</td>
								<td class="nd_learning_padding_20_10 nd_learning_text_align_right">  
									  <h3><strong>'.
										  currencyr_exchange( array( 'amount' => 0 , 'to' => $country['money'] ) )
									  .'</strong></h3> 
								</td>
							</tr>
						</tbody>
					</table>
				  </div>
				</div>
			</div>';





			echo $nd_learning_shortcode_checkout;



		}else{
			notification('error', __('NoMatriculadoFecha', 'custom-translation'));
			redirectBack();
		}





    //START ELSE IF 2 - arrive from paypal paypal payment
    }elseif ( isset($_GET['tx']) ){

      
      //START process after paypla payment
      $nd_learning_tx = $_GET['tx'];

      // Init cURL
      $nd_learning_request = curl_init();

      // Set request options
      curl_setopt_array($nd_learning_request, array
      (
        CURLOPT_URL => $nd_learning_paypal_action_2,
        CURLOPT_POST => TRUE,
        CURLOPT_POSTFIELDS => http_build_query(array
          (
            'cmd' => '_notify-synch',
            'tx' => $nd_learning_tx,
            'at' => $nd_learning_paypal_token,
          )),
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HEADER => FALSE,
      ));

      // Execute request and get response and status code
      $nd_learning_response = curl_exec($nd_learning_request);
      $nd_learning_status   = curl_getinfo($nd_learning_request, CURLINFO_HTTP_CODE);

      // Close connection
      curl_close($nd_learning_request);


      //START IF 4
      if($nd_learning_status == 200 AND strpos($nd_learning_response, 'SUCCESS') === 0){

        
        // Remove SUCCESS part (7 characters long)
        $nd_learning_response = substr($nd_learning_response, 7);

        // URL decode
        $nd_learning_response = urldecode($nd_learning_response);

        // Turn into associative array
        preg_match_all('/^([^=\s]++)=(.*+)/m', $nd_learning_response, $m, PREG_PATTERN_ORDER);
        $nd_learning_response = array_combine($m[1], $m[2]);

        // Fix character encoding if different from UTF-8 (in my case)
        if(isset($nd_learning_response['charset']) AND strtoupper($nd_learning_response['charset']) !== 'UTF-8')
        {
          foreach($nd_learning_response as $key => &$value)
          {
            $value = mb_convert_encoding($value, 'UTF-8', $nd_learning_response['charset']);
          }
          $nd_learning_response['charset_original'] = $nd_learning_response['charset'];
          $nd_learning_response['charset'] = 'UTF-8';
        }

        // Sort on keys for readability (handy when debugging)
        ksort($nd_learning_response);


        $nd_learning_response_result = '';
        $nd_learning_response_result .= '

          <h2><strong>'.__('Thanks for your order','nd-learning').'</strong></h2>
          <p class="nd_learning_margin_top_20"><strong>'.__('Item Name : ','nd-learning').' : </strong> '.$nd_learning_response['item_name'].' with ID '.$nd_learning_response['item_number'].' </p>
          <p><strong>'.__('Price : ','nd-learning').'</strong> '.$nd_learning_response['mc_gross'].' x '.$nd_learning_response['quantity'].''.__(' Qnt','nd-learning').'</p>
          <p><strong>'.__('Payment Status : ','nd-learning').'</strong> '.$nd_learning_response['payment_status'].' </p>
          <p><strong>'.__('Transiction ID : ','nd-learning').'</strong> '.$nd_learning_response['txn_id'].' </p>

          <a class="nd_learning_bg_green nd_learning_margin_top_20 nd_learning_display_inline_block nd_learning_color_white_important nd_learning_text_decoration_none nd_learning_padding_5_10 nd_learning_border_radius_3 nd_options_first_font" href="'.$nd_learning_account_page_url.'">'.__('My Orders','nd-learning').'</a>

        ';


        echo $nd_learning_response_result;

        //START add order in db
        if ( nd_learning_check_if_order_is_present($nd_learning_response['txn_id'],$nd_learning_response['item_number'],$nd_learning_response['custom']) == 0 ) {

          nd_learning_add_order_in_db(

            $nd_learning_response['item_number'],
            $nd_learning_response['mc_gross'],
            $nd_learning_response['payment_date'],
            $nd_learning_response['quantity'],
            $nd_learning_response['payment_status'],
            $nd_learning_response['mc_currency'],
            $nd_learning_response['payer_email'],
            $nd_learning_response['txn_id'],
            $nd_learning_response['custom'],
            $nd_learning_response['address_country'],
            $nd_learning_response['address_street'].' '.$nd_learning_response['address_zip'],
            $nd_learning_response['first_name'],
            $nd_learning_response['last_name'],
            $nd_learning_response['address_city'],
            'paypal'

          );

        }
        //END add order in db

        

      }
      //END IF 4
      else{
        echo '

        <p>'.__('An error has occurred, contact the site administrator','nd-learning').'</p>
        <div class="nd_options_section nd_options_height_20"></div>
        <a class="nd_learning_bg_green nd_options_first_font nd_learning_display_inline_block nd_learning_color_white_important nd_learning_text_decoration_none nd_learning_padding_5_10 nd_learning_border_radius_3" href="'.get_post_type_archive_link('courses').'">'.__('Back To Courses','nd-learning').'</a>

      ';   
      }
      //END


    



    //START ELSE IF 2 -  arrive from booking free item
    }elseif ( isset($_POST['nd_learning_free_user_id']) ) {

      //recover datas
      $nd_learning_free_user_id = $_POST['nd_learning_free_user_id'];
      $nd_learning_free_user_login = $_POST['nd_learning_free_user_login'];
      $nd_learning_free_display_name = $_POST['nd_learning_free_display_name'];
      $nd_learning_free_current_user_email = $_POST['nd_learning_free_current_user_email'];
      $nd_learning_free_current_user_firstname = $_POST['nd_learning_free_current_user_firstname'];
      $nd_learning_free_current_user_lastname = $_POST['nd_learning_free_current_user_lastname'];
      $nd_learning_free_user_country = $_POST['nd_learning_free_user_country'];
      $nd_learning_free_user_address = $_POST['nd_learning_free_user_address'];
      $nd_learning_free_user_city = $_POST['nd_learning_free_user_city'];
      $nd_learning_free_user_zip = $_POST['nd_learning_free_user_zip'];
      $nd_learning_free_id_course = $_POST['nd_learning_free_id_course'];
      $nd_learning_free_title_course = $_POST['nd_learning_free_title_course'];
      $nd_learning_free_price_course = $_POST['nd_learning_free_price_course'];

      //get current date
      $nd_learning_free_date = date('H:m:s F j Y');

      $nd_learning_response_result = '';

      $nd_learning_response_result .= '

        <h2><strong>'.__('Thanks for your order','nd-learning').'</strong></h2>
        <p class="nd_learning_margin_top_20"><strong>'.__('Item Name : ','nd-learning').'</strong> '.$nd_learning_free_title_course.''.__(' with ID ','nd-learning').''.$nd_learning_free_id_course.'</p>
        <p><strong>'.__('Price : ','nd-learning').'</strong>'.__('FREE','nd-learning').'</p>

        <a class="nd_learning_bg_green nd_learning_margin_top_20 nd_learning_display_inline_block nd_learning_color_white_important nd_learning_text_decoration_none nd_learning_padding_5_10 nd_options_first_font nd_learning_border_radius_3" href="'.$nd_learning_account_page_url.'">'.__('My Orders','nd-learning').'</a>
 

      ';

      echo $nd_learning_response_result;



      //START add order in db
      if ( nd_learning_check_if_order_is_present(0,$nd_learning_free_id_course,$nd_learning_free_user_id) == 0 ) {
        
        nd_learning_add_order_in_db(

          $nd_learning_free_id_course,
          0,
          $nd_learning_free_date,
          1,
          0,
          0,
          $nd_learning_free_current_user_email,
          0,
          $nd_learning_free_user_id,
          $nd_learning_free_user_country,
          $nd_learning_free_user_address.' '.$nd_learning_free_user_zip,
          $nd_learning_free_current_user_firstname,
          $nd_learning_free_current_user_lastname,
          $nd_learning_free_user_city,
          'free'

        );
      }
      //END add order in db


    //END IF 2 - try to reach the page directly from browser
    }else{
      
      echo '

        <p>'.__('You have not selected any course','nd-learning').'</p>
        <div class="nd_options_section nd_options_height_20"></div>
        <a class="nd_learning_bg_green nd_options_first_font nd_learning_display_inline_block nd_learning_color_white_important nd_learning_text_decoration_none nd_learning_padding_5_10 nd_learning_border_radius_3" href="'.get_post_type_archive_link('courses').'">'.__('Back To Courses','nd-learning').'</a>

      ';

    }


  //END IF 1 - the user is not logged in the site
  }else{

	echo "<script type='text/javascript'>window.location='". home_url() ."/login'</script>";
	exit();

//    $nd_learning_registration_enable_result = '';
//
//    $nd_learning_registration_enable_result .= '
//
//      <div class="nd_learning_section">
//          
//
//          <div class="nd_learning_width_50_percentage nd_learning_float_left nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive">
//            
//            <div class="nd_learning_section nd_learning_border_radius_3 nd_learning_border_1_solid_grey nd_learning_padding_20 nd_learning_box_sizing_border_box">
//              
//              <h6 class="nd_options_second_font nd_learning_bg_green nd_learning_padding_5 nd_learning_border_radius_3 nd_learning_color_white_important nd_learning_display_inline_block">'.__('ALREADY A MEMBER','nd-learning').'</h6>
//              <div class="nd_learning_section nd_learning_height_5"></div>
//              <h2><strong>'.__('Log In','nd-learning').'</strong></h2>
//
//              '.do_shortcode("[nd_learning_login]").' 
//            </div>
//
//            
//          </div>
//          
//
//    ';
//
//
//
//    //START check if registration is enable
//    if ( get_option( 'users_can_register' ) == 1 ) {
//
//        $nd_learning_registration_enable_result .='
//
//        <div class="nd_learning_width_50_percentage nd_learning_float_left nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive">
//
//            <div class="nd_learning_section nd_learning_bg_white nd_learning_border_radius_3 nd_learning_border_1_solid_grey nd_learning_padding_20 nd_learning_box_sizing_border_box">
//            
//                <h6 class="nd_options_second_font nd_learning_bg_orange nd_learning_padding_5 nd_learning_border_radius_3 nd_learning_color_white_important nd_learning_display_inline_block">'.__('I DO NOT HAVE AN ACCOUNT','nd-learning').'</h6>
//                <div class="nd_learning_section nd_learning_height_5"></div>
//                <h2><strong>'.__('Register','nd-learning').'</strong></h2>
//
//                '.do_shortcode("[nd_learning_register]").'
//
//            </div>
//            
//          </div>';
//
//    }else{
//
//        $nd_learning_registration_enable_result .='
//
//        <div class="nd_learning_width_50_percentage nd_learning_float_left nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive">
//
//
//            <div class="nd_learning_opacity_04 nd_learning_section nd_learning_bg_white nd_learning_border_radius_3 nd_learning_border_1_solid_grey nd_learning_padding_20 nd_learning_box_sizing_border_box">
//            
//                <h6 class="nd_options_second_font nd_learning_bg_orange nd_learning_padding_5 nd_learning_border_radius_3 nd_learning_color_white_important nd_learning_display_inline_block">'.__('I DO NOT HAVE AN ACCOUNT','nd-learning').'</h6>
//                <div class="nd_learning_section nd_learning_height_5"></div>
//                <h2><strong>'.__('Registration Disabled','nd-learning').'</strong></h2>
//
//                
//                <form action="#" method="post">
//                    <p>
//                      <label class="nd_learning_section nd_learning_margin_top_20">'.__('Username','nd-learning').' *</label>
//                      <input readonly type="text" name="nd_learning_username" class=" nd_learning_section" value="">
//                    </p>
//                    <p>
//                      <label class="nd_learning_section nd_learning_margin_top_20">'.__('Password','nd-learning').' *</label>
//                      <input readonly type="password" name="nd_learning_password" class=" nd_learning_section" value="">
//                    </p>
//                    <p>
//                      <label class="nd_learning_section nd_learning_margin_top_20">'.__('Email','nd-learning').' *</label>
//                      <input readonly type="text" name="nd_learning_email" class=" nd_learning_section" value="">
//                    </p>
//                    <p>
//                      <label class="nd_learning_section nd_learning_margin_top_20">'.__('Website','nd-learning').'</label>
//                      <input readonly type="text" name="nd_learning_website" class=" nd_learning_section" value="">
//                    </p>
//                    <p>
//                      <label class="nd_learning_section nd_learning_margin_top_20">'.__('First Name','nd-learning').'</label>
//                      <input readonly type="text" name="nd_learning_first_name" class="nd_learning_section" value="">
//                    </p>
//                    <p>
//                      <label class="nd_learning_section nd_learning_margin_top_20">'.__('Last Name','nd-learning').'</label>
//                      <input readonly type="text" name="nd_learning_last_name" class="nd_learning_section" value="">
//                    </p>
//                    <p>
//                      <label class="nd_learning_section nd_learning_margin_top_20">'.__('Nickname','nd-learning').'</label>
//                      <input readonly type="text" name="nd_learning_nickname" class="nd_learning_section" value="">
//                    </p>
//                    <p>
//                      <label class="nd_learning_section nd_learning_margin_top_20">'.__('About / Bio','nd-learning').'</label>
//                      <textarea readonly class="nd_learning_section" name="nd_learning_bio"></textarea>
//                    </p>
//                    <input disabled class="nd_learning_section nd_learning_margin_top_20" type="submit" name="submit" value="'.__('Registration Disabled','nd-learning').'">
//                </form>
//
//
//            </div>
//
//
//          </div>';
//
//    }
//    //END check if registration is enable
//
//
//    $nd_learning_registration_enable_result .='</div>';
//
//
//    echo $nd_learning_registration_enable_result;


  }
  //end if

}

add_shortcode('nd_learning_checkout', 'nd_learning_shortcode_checkout');
//END nd_learning_checkout






//START nd_learning_add_order_in_db
function nd_learning_add_order_in_db(
  
  $nd_learning_id_course,
  $nd_learning_course_price,
  $nd_learning_date,
  $nd_learning_qnt,
  $nd_learning_paypal_payment_status,
  $nd_learning_paypal_currency,
  $nd_learning_paypal_email,
  $nd_learning_paypal_tx,
  $nd_learning_id_user,
  $nd_learning_user_country,
  $nd_learning_user_address,
  $nd_learning_user_first_name,
  $nd_learning_user_last_name,
  $nd_learning_user_city,
  $nd_learning_action_type

) {

  global $wpdb;
  $nd_learning_table_name = $wpdb->prefix . 'nd_learning_courses';


  //START INSERT DB
  $nd_learning_add_order = $wpdb->insert( 
    
    $nd_learning_table_name, 
    
    array( 
      'id_course' => $nd_learning_id_course,
      'course_price' => $nd_learning_course_price,
      'date' => $nd_learning_date,
      'qnt' => $nd_learning_qnt,
      'paypal_payment_status' => $nd_learning_paypal_payment_status,
      'paypal_currency' => $nd_learning_paypal_currency,
      'paypal_email' => $nd_learning_paypal_email,
      'paypal_tx' => $nd_learning_paypal_tx,
      'id_user' => $nd_learning_id_user,
      'user_country' => $nd_learning_user_country,
      'user_address' => $nd_learning_user_address,
      'user_first_name' => $nd_learning_user_first_name,
      'user_last_name' => $nd_learning_user_last_name,
      'user_city' => $nd_learning_user_city,
      'action_type' => $nd_learning_action_type
    )

  );
  
  if ($nd_learning_add_order){

    //order added in db

  }else{

    $wpdb->show_errors();
    $wpdb->print_error();

  }
  //END INSERT DB
  
        
  //close the function to avoid wordpress errors
  //die();

}
//END










	add_action('nd_learning_shortcode_account_tab_list','nd_learning_add_order_tab_list');

	function nd_learning_add_order_tab_list(){
			$nd_learning_add_order_tab_list = '
				<li class="nd_learning_display_inline_block">
					<h4>
						<a class="nd_learning_outline_0 nd_learning_padding_20 nd_learning_display_inline_block nd_options_first_font nd_options_color_greydark" href="#nd_learning_account_page_tab_order">'.__('My Orders','nd-learning').'</a>
					</h4>
			</li>';
	  echo $nd_learning_add_order_tab_list;
	}


//START show orders in account page
	add_action('nd_learning_shortcode_account_tab_list_content','nd_learning_show_orders');
	
function nd_learning_show_orders(){

  //declare variable
  $nd_learning_result = '';


  //recover datas from plugin settings
  $nd_learning_settings_page_order = get_option('nd_learning_order_page');
  $nd_learning_order_page_url = get_permalink($nd_learning_settings_page_order);

  //get current user id
  $nd_learning_current_user = wp_get_current_user();
  $nd_learning_current_user_id = $nd_learning_current_user->ID;

  global $wpdb;

  $nd_learning_table_name = $wpdb->prefix . 'nd_learning_courses';
  $nd_learning_paypal_payment_status = "'Completed'";

  //START select for items

  function getCurrentCourse() {
    $conn = connectDBCampus();
    $sql = "SELECT * FROM acuerdo_colaboracion_ase WHERE id_alumno = '" . $_SESSION['userCampus']['idalumnos'] . "'";
	
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      if ($user = $result->fetch_assoc()) {
        return $user;
      }
    }
  }

  $currentCourse = getCurrentCourse();

  // $nd_learning_order_ids = $currentCourse['idcurso_matriculado'];

  //title section
  $nd_learning_result .= '
    <div class="nd_learning_section" id="nd_learning_account_page_tab_order">
      <div class="nd_learning_section nd_learning_height_40"></div>
      <h3><strong>'.__('CursoMatriculado','custom-translation').'</strong></h3>
      <div class="nd_learning_section nd_learning_height_20"></div>
  ';

  //no results
  if (empty($currentCourse['idcurso_matriculado']) ) { 
    // $nd_learning_result .= '<p>'.__('You do not have any order','nd-learning').'</p>'; 
    $nd_learning_result .= '<p>'.__('NoCursoMatriculado','custom-translation').'</p>';
  }else{


    $nd_learning_result .= '
      
      <div class="nd_learning_section nd_learning_box_sizing_border_box nd_learning_overflow_hidden nd_learning_overflow_x_auto nd_learning_cursor_move_responsive">
      <table>
        <thead>
          <tr class="nd_learning_border_bottom_1_solid_grey">
              <td class="nd_learning_padding_20 nd_learning_width_20_percentage">
                  <h6 class="nd_learning_text_transform_uppercase">'.__('COURSE','nd-learning').'</h6>    
              </td>
              <td class="nd_learning_padding_20 nd_learning_width_50_percentage nd_learning_display_none_all_iphone">
                      
              </td>
              <td class="nd_learning_padding_20 nd_learning_width_20_percentage">
                  <h6 class="nd_learning_text_transform_uppercase">'.__('FECHA','custom-translation').'</h6>    
              </td>
              <td class="nd_learning_padding_20 nd_learning_width_20_percentage">
                  <h6 class="nd_learning_text_transform_uppercase">'.__('FECHAFIN','custom-translation').'</h6>    
              </td>
              <td class="nd_learning_padding_20 nd_learning_width_10_percentage">
                    
              </td>
          </tr>
      </thead>
      <tbody>
    ';

    // foreach ( $nd_learning_order_ids as $nd_learning_order_id ) 
    // {
      $nd_learning_result .= '

        <tr class="nd_learning_border_bottom_1_solid_grey">
            <td class="nd_learning_padding_20">  
				<a href="' . get_permalink( $currentCourse['idcurso_matriculado'] ) . '">'
					. '<img alt="" class="nd_learning_section" src="'. nd_learning_get_course_image_url($currentCourse['idcurso_matriculado']) .'"> 
				</a>
            </td>
            <td class="nd_learning_padding_20">  
                <a href="' . get_permalink( $currentCourse['idcurso_matriculado'] ) . '"><h4><strong>'. get_the_title($currentCourse['idcurso_matriculado']) .'</strong></h4></a>
                <div class="nd_learning_section nd_learning_height_5"></div>
            </td>
            <td class="nd_learning_padding_20 nd_learning_display_none_all_iphone">
                <p class="nd_options_color_greydark">' . $currentCourse['fecha_matriculado'] . '</p>    
            </td>
            <td class="nd_learning_padding_20 nd_learning_display_none_all_iphone">
                <p class="nd_options_color_greydark">' . $currentCourse['fecha_fin'] . '</p>    
            </td>
            <td class="nd_learning_padding_20">   

              <form method="post" action="http://campusaulainteractiva.com" target="_blank">
                <input class="nd_learning_display_inline_block nd_learning_color_white_important nd_learning_cursor_pointer nd_learning_bg_green nd_options_first_font nd_learning_padding_8 nd_learning_border_radius_3 nd_learning_font_size_13" type="submit" value="ENTRAR">
              </form>

            </td>
        </tr>

      ';
    // }
    $nd_learning_result .= '</tbody></table></div>';

  }
  //END select for items

  echo $nd_learning_result;
  

}
//END


//START show free orders in account page
add_action('nd_learning_shortcode_account_tab_list_content','nd_learning_show_free_orders');
function nd_learning_show_free_orders(){

	$modulo = historialCursos($_SESSION['userCampus']['idalumnos']);	
	//declare variable
	$nd_learning_result = '';

	//recover datas from plugin settings
	$nd_learning_settings_page_order = get_option('nd_learning_order_page');
	$nd_learning_order_page_url = get_permalink($nd_learning_settings_page_order);

	//get current user id
	$nd_learning_current_user = wp_get_current_user();
	$nd_learning_current_user_id = $nd_learning_current_user->ID;

	global $wpdb;

	$nd_learning_table_name = $wpdb->prefix . 'nd_learning_courses';
	$nd_learning_action_type = "'free'";

	//START select for items
	$nd_learning_order_ids = $wpdb->get_results( "SELECT id_course, id FROM $nd_learning_table_name WHERE id_user = $nd_learning_current_user_id AND action_type = $nd_learning_action_type");

	//title section
	$nd_learning_result .= '
		<div class="nd_learning_section nd_learning_height_40"></div>
		<h3><strong>'.__('CursosRealizados','custom-translation').'</strong></h3>
		<div class="nd_learning_section nd_learning_height_20"></div>
	';

	//no results
	if ($modulo->num_rows == 0) { 
	  $nd_learning_result .= '<p>'.__('NoCursosRealizados','custom-translation').'</p>'; 
	}else{


	  $nd_learning_result .= '

		<div class="nd_learning_section nd_learning_box_sizing_border_box nd_learning_overflow_hidden nd_learning_overflow_x_auto nd_learning_cursor_move_responsive">
		  <table>
			<thead>
			  <tr class="nd_learning_border_bottom_1_solid_grey">
				  <td class="nd_learning_padding_20 nd_learning_width_20_percentage">
					  <h6 class="nd_learning_text_transform_uppercase">'.__('COURSE','nd-learning').'</h6>    
				  </td>
				  <td class="nd_learning_padding_20 nd_learning_width_50_percentage nd_learning_display_none_all_iphone">

				  </td>
				  <td class="nd_learning_padding_20 nd_learning_width_20_percentage"></td>
				  <td class="nd_learning_padding_20 nd_learning_width_10_percentage">

				  </td>
			  </tr>
		  </thead>
		  <tbody>
	  ';

	  while ( $mod = $modulo->fetch_assoc()) 
	  {
		  if(!empty($mod['id_ase'])){
				$nd_learning_result .= '
				  <tr class="nd_learning_border_bottom_1_solid_grey">
					  <td class="nd_learning_padding_20">  
						  <a href="' . get_permalink( $mod['id_ase'] ) . '">
							  <img alt="" class="nd_learning_section" src="'.nd_learning_get_course_image_url($mod['id_ase']).'"> 
						  </a>
					  </td>
					  <td class="nd_learning_padding_20">  
						  <a href="' . get_permalink( $mod['id_ase'] ) . '"><h4><strong>'.get_the_title($mod['id_ase']).'</strong></h4></a> 
					  </td>
					  <td class="nd_learning_padding_20 nd_learning_display_none_all_iphone"></td>
					  <td class="nd_learning_padding_20"></td>
				  </tr>
				';	
		  }
	  }
	  $nd_learning_result .= '</tbody></table></div>';

	}
	//END select for items


	$nd_learning_result .= '</div>';


	echo $nd_learning_result;
  

}
//END


//include all files
foreach ( glob ( plugin_dir_path( __FILE__ ) . "shortcodes/*.php" ) as $file ){
  include_once $file;
}


?>