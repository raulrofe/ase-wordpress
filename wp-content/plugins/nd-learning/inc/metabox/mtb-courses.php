<?php
// CUSTOM WORDPRESS
///////////////////////////////////////////////////METABOX ///////////////////////////////////////////////////////////////


add_action('add_meta_boxes', 'nd_learning_box_add');

function nd_learning_box_add() {
	add_meta_box('my-meta-box-id', __('Course Options', 'nd-learning'), 'nd_learning_meta_box', 'courses', 'normal', 'high');
}

function nd_learning_meta_box() {

	//date picker
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_style('jquery-ui-datepicker-css', plugins_url() . '/nd-learning/assets/css/jquery-ui-datepicker.css');

	//jquery-ui-autocomplete
	wp_enqueue_script('jquery-ui-autocomplete');

	//iris color picker
	wp_enqueue_script('iris');

	// $post is already set, and contains an object: the WordPress post
	global $post;
	$nd_learning_values = get_post_custom($post->ID);

	$nd_learning_meta_box_id_modulo = get_post_meta(get_the_ID(), 'nd_learning_meta_box_id_modulo', true);
	$nd_learning_meta_box_price = get_post_meta(get_the_ID(), 'nd_learning_meta_box_price', true);
	$nd_learning_meta_box_max_availability = get_post_meta(get_the_ID(), 'nd_learning_meta_box_max_availability', true);
	$nd_learning_meta_box_teacher = get_post_meta(get_the_ID(), 'nd_learning_meta_box_teacher', true);
	$nd_learning_meta_box_teachers = get_post_meta(get_the_ID(), 'nd_learning_meta_box_teachers', true);
	$nd_learning_meta_box_form = get_post_meta(get_the_ID(), 'nd_learning_meta_box_form', true);
	
	// FECHAS INICIO
	$nd_learning_meta_box_date = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date', true);
	$nd_learning_meta_box_date2 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date2', true);
	$nd_learning_meta_box_date3 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date3', true);
	$nd_learning_meta_box_date4 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date4', true);
	$nd_learning_meta_box_date5 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date5', true);
	$nd_learning_meta_box_date6 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date6', true);
	$nd_learning_meta_box_date7 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date7', true);
	$nd_learning_meta_box_date8 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date8', true);
	$nd_learning_meta_box_date9 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date9', true);
	$nd_learning_meta_box_date10 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date10', true);
	$nd_learning_meta_box_date11 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date11', true);
	$nd_learning_meta_box_date12 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date12', true);
	$nd_learning_meta_box_date13 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date13', true);
	$nd_learning_meta_box_date14 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date14', true);
	$nd_learning_meta_box_date15 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date15', true);
	$nd_learning_meta_box_date16 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date16', true);
	$nd_learning_meta_box_date17 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date17', true);
	$nd_learning_meta_box_date18 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date18', true);
	$nd_learning_meta_box_date19 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date19', true);
	$nd_learning_meta_box_date20 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date20', true);
	
	// FECHAS FIN
	$nd_learning_meta_box_date_end = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end', true);
	$nd_learning_meta_box_date_end2 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end2', true);
	$nd_learning_meta_box_date_end3 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end3', true);
	$nd_learning_meta_box_date_end4 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end4', true);
	$nd_learning_meta_box_date_end5 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end5', true);
	$nd_learning_meta_box_date_end6 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end6', true);
	$nd_learning_meta_box_date_end7 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end7', true);
	$nd_learning_meta_box_date_end8 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end8', true);
	$nd_learning_meta_box_date_end9 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end9', true);
	$nd_learning_meta_box_date_end10 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end10', true);
	$nd_learning_meta_box_date_end11 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end11', true);
	$nd_learning_meta_box_date_end12 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end12', true);
	$nd_learning_meta_box_date_end13 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end13', true);
	$nd_learning_meta_box_date_end14 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end14', true);
	$nd_learning_meta_box_date_end15 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end15', true);
	$nd_learning_meta_box_date_end16 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end16', true);
	$nd_learning_meta_box_date_end17 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end17', true);
	$nd_learning_meta_box_date_end18 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end18', true);
	$nd_learning_meta_box_date_end19 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end19', true);
	$nd_learning_meta_box_date_end20 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_end20', true);
	
	// FECHAS HOMOLOGABLE
	$nd_learning_meta_box_date_check = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check', true);
	$nd_learning_meta_box_date_check2 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check2', true);
	$nd_learning_meta_box_date_check3 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check3', true);
	$nd_learning_meta_box_date_check4 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check4', true);
	$nd_learning_meta_box_date_check5 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check5', true);
	$nd_learning_meta_box_date_check6 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check6', true);
	$nd_learning_meta_box_date_check7 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check7', true);
	$nd_learning_meta_box_date_check8 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check8', true);
	$nd_learning_meta_box_date_check9 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check9', true);
	$nd_learning_meta_box_date_check10 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check10', true);
	$nd_learning_meta_box_date_check11 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check11', true);
	$nd_learning_meta_box_date_check12 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check12', true);
	$nd_learning_meta_box_date_check13 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check13', true);
	$nd_learning_meta_box_date_check14 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check14', true);
	$nd_learning_meta_box_date_check15 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check15', true);
	$nd_learning_meta_box_date_check16 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check16', true);
	$nd_learning_meta_box_date_check17 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check17', true);
	$nd_learning_meta_box_date_check18 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check18', true);
	$nd_learning_meta_box_date_check19 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check19', true);
	$nd_learning_meta_box_date_check20 = get_post_meta(get_the_ID(), 'nd_learning_meta_box_date_check20', true);
	
	$nd_learning_meta_box_color = get_post_meta(get_the_ID(), 'nd_learning_meta_box_color', true);
	$nd_learning_meta_box_MECD = get_post_meta(get_the_ID(), 'nd_learning_meta_box_MECD', true);
	$nd_learning_meta_box_URJC = get_post_meta(get_the_ID(), 'nd_learning_meta_box_URJC', true);
	$nd_learning_meta_box_nuevo = get_post_meta(get_the_ID(), 'nd_learning_meta_box_nuevo', true);
	$nd_learning_meta_box_video = get_post_meta(get_the_ID(), 'nd_learning_meta_box_video', true);
	?>

	<p><strong>ID del modulo en CAMPUS (NO TOCAR)</strong></p>
	<p><input type="text" name="nd_learning_meta_box_id_modulo" id="nd_learning_meta_box_id_modulo" value="<?php echo $nd_learning_meta_box_id_modulo; ?>" /></p>
	
	
	<p><strong>¿CURSO NUEVO?</strong></p>
	<label for="nd_learning_meta_box_nuevo" class="selectit">
		<input name="nd_learning_meta_box_nuevo" type="checkbox" id="nd_learning_meta_box_nuevo"
			   <?php if($nd_learning_meta_box_nuevo=='on') echo 'checked="checked"' ?>
		> Marcar como Nuevo
	</label>
	<br>
	<br>
	<p><strong>HOMOLOGACIONES</strong></p>
	<label for="nd_learning_meta_box_MECD" class="selectit">
		<input name="nd_learning_meta_box_MECD" type="checkbox" id="nd_learning_meta_box_MECD"
			   <?php if($nd_learning_meta_box_MECD=='on') echo 'checked="checked"' ?>
		> Homologado MECD
	</label>
	<br>
	<label for="nd_learning_meta_box_URJC" class="selectit">
		<input name="nd_learning_meta_box_URJC" type="checkbox" id="nd_learning_meta_box_URJC"
			   <?php if($nd_learning_meta_box_URJC=='on') echo 'checked="checked"' ?>
		> Acreditado URJC
	</label>
	<br>
	<br>
	
	<p><strong>URL Video Youtube </strong></p>
	<p><input class="regular-text" type="text" name="nd_learning_meta_box_video" id="nd_learning_meta_box_video" value="<?php echo $nd_learning_meta_box_video; ?>" /></p>
	<!--******************************IMAGE VIDEO******************************-->
	<?php
		$nd_learning_meta_box_course_header_img_video = get_post_meta(get_the_ID(), 'nd_learning_meta_box_course_header_img_video', true);
	?>
	<p><strong>Imagen previa del video</strong></p>
	<p><input class="large-text" type="text" name="nd_learning_meta_box_course_header_img_video" id="nd_learning_meta_box_course_header_img_video" value="<?php echo $nd_learning_meta_box_course_header_img_video; ?>" /></p>
	<p>
		<input class="button nd_learning_meta_box_course_header_img_button_video" type="button" name="nd_learning_meta_box_course_header_img_button_video" id="nd_learning_meta_box_course_header_img_button_video" value="<?php _e('Upload', 'nd-learning'); ?>" />
	</p>
	
	
	<p><strong><?php _e('Price ( set 0 for free )', 'nd-learning'); ?></strong></p>
	<p><input type="text" name="nd_learning_meta_box_price" id="nd_learning_meta_box_price" value="<?php echo $nd_learning_meta_box_price; ?>" /></p>

	<p><strong><?php _e('Max Availability', 'nd-learning'); ?></strong></p>
	<p><input type="text" name="nd_learning_meta_box_max_availability" id="nd_learning_meta_box_max_availability" value="<?php echo $nd_learning_meta_box_max_availability; ?>" /></p>

	<p><strong><?php _e('Main Teacher', 'nd-learning'); ?></strong></p>
	<p>
		<select name="nd_learning_meta_box_teacher">
			<?php
			$nd_learning_meta_box_teacher = get_post_meta(get_the_ID(), 'nd_learning_meta_box_teacher', true);
			$nd_learning_teachers_args = array('posts_per_page' => -1, 'post_type' => 'teachers');
			$nd_learning_teachers = get_posts($nd_learning_teachers_args);
			?>
			<?php foreach ($nd_learning_teachers as $nd_learning_teacher) : ?>
				<option 

					<?php
					if ($nd_learning_meta_box_teacher == $nd_learning_teacher->ID) {
						echo 'selected="selected"';
					}
					?>

					value="<?php echo $nd_learning_teacher->ID; ?>">
						<?php echo $nd_learning_teacher->post_title; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</p>


	<p><strong><?php _e('Other Teachers', 'nd-learning'); ?></strong></p>
	<p><input class="regular-text" type="text" name="nd_learning_meta_box_teachers" id="nd_learning_meta_box_teachers" value="<?php echo $nd_learning_meta_box_teachers; ?>" /></p>
	<p class="description"><?php _e('Start writing your teacher\'s name, this is an intuitive field', 'nd-learning'); ?></p>

	<script type="text/javascript">
		//<![CDATA[

		jQuery(document).ready(function ($) {
			var nd_learning_available_teachers = [
				//start all teachers list
	<?php
	$nd_learning_teachers_args = array('posts_per_page' => -1, 'post_type' => 'teachers');
	$nd_learning_teachers = get_posts($nd_learning_teachers_args);

	foreach ($nd_learning_teachers as $nd_learning_teacher) :
		echo '"' . $nd_learning_teacher->post_name . '",';
	endforeach;
	?>
				//end all teachers list

			];
			function split(val) {
				return val.split(/,\s*/);
			}
			function extractLast(term) {
				return split(term).pop();
			}

			$("#nd_learning_meta_box_teachers")
				// don't navigate away from the field on tab when selecting an item
				.on("keydown", function (event) {
					if (event.keyCode === $.ui.keyCode.TAB &&
						$(this).autocomplete("instance").menu.active) {
						event.preventDefault();
					}
				})
				.autocomplete({
					minLength: 0,
					source: function (request, response) {
						// delegate back to autocomplete, but extract the last term
						response($.ui.autocomplete.filter(
							nd_learning_available_teachers, extractLast(request.term)));
					},
					focus: function () {
						// prevent value inserted on focus
						return false;
					},
					select: function (event, ui) {
						var terms = split(this.value);
						// remove the current input
						terms.pop();
						// add the selected item
						terms.push(ui.item.value);
						// add placeholder to get the comma-and-space at the end
						terms.push("");
						this.value = terms.join(",");
						return false;
					}
				});
		});

		//]]>
	</script>


	
	<style>
		div.tabla-fechas{
			display: block;
			width: 100%;
			margin:40px 0px;;
		}
		div.tabla-fechas table {
			
		}
		div.tabla-fechas table thead th:first-child{
			text-align: center;
		}
		div.tabla-fechas table thead th{
			text-align: left;
			padding: 0px 15px;
		}
		div.tabla-fechas table td{
			text-align: center;
			padding: 0px 15px;
		}
	</style>

	
	
	<div class="tabla-fechas">
		<table>
			<thead>
				<th>#</th>
				<th>Homologable</th>
				<th>Fecha de inicio</th>
				<th>Fecha de fin</th>
			</thead>
				
			<tbody>
				<!-- 1 -->
				<tr>
					<td>
						1
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check" class="selectit">
							<input id="nd_learning_meta_box_date_check" name="nd_learning_meta_box_date_check" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker" data-dapicker="1" type="text" name="nd_learning_meta_box_date" value="<?php echo $nd_learning_meta_box_date; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end" value="<?php echo $nd_learning_meta_box_date_end; ?>" />
					</td>
				</tr>
				
				<!-- 2 -->
				<tr>
					<td>
						2
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check2" class="selectit">
							<input id="nd_learning_meta_box_date_check2" name="nd_learning_meta_box_date_check2" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check2=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker2" data-dapicker="1" type="text" name="nd_learning_meta_box_date2" value="<?php echo $nd_learning_meta_box_date2; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end2" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end2" value="<?php echo $nd_learning_meta_box_date_end2; ?>" />
					</td>
				</tr>
				
				<!-- 3 -->
				<tr>
					<td>
						3
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check3" class="selectit">
							<input id="nd_learning_meta_box_date_check3" name="nd_learning_meta_box_date_check3" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check3=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker3" data-dapicker="1" type="text" name="nd_learning_meta_box_date3" value="<?php echo $nd_learning_meta_box_date3; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end3" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end3" value="<?php echo $nd_learning_meta_box_date_end3; ?>" />
					</td>
				</tr>
				
				<!-- 4 -->
				<tr>
					<td>
						4
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check4" class="selectit">
							<input id="nd_learning_meta_box_date_check4" name="nd_learning_meta_box_date_check4" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check4=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker4" data-dapicker="1" type="text" name="nd_learning_meta_box_date4" value="<?php echo $nd_learning_meta_box_date4; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end4" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end4" value="<?php echo $nd_learning_meta_box_date_end4; ?>" />
					</td>
				</tr>
				
				<!-- 5 -->
				<tr>
					<td>
						5
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check5" class="selectit">
							<input id="nd_learning_meta_box_date_check5" name="nd_learning_meta_box_date_check5" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check5=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker5" data-dapicker="1" type="text" name="nd_learning_meta_box_date5" value="<?php echo $nd_learning_meta_box_date5; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end5" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end5" value="<?php echo $nd_learning_meta_box_date_end5; ?>" />
					</td>
				</tr>
				
				<!-- 6 -->
				<tr>
					<td>
						6
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check6" class="selectit">
							<input id="nd_learning_meta_box_date_check6" name="nd_learning_meta_box_date_check6" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check6=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker6" data-dapicker="1" type="text" name="nd_learning_meta_box_date6" value="<?php echo $nd_learning_meta_box_date6; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end6" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end6" value="<?php echo $nd_learning_meta_box_date_end6; ?>" />
					</td>
				</tr>
				
				<!-- 7 -->
				<tr>
					<td>
						7
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check7" class="selectit">
							<input id="nd_learning_meta_box_date_check7" name="nd_learning_meta_box_date_check7" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check7=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker7" data-dapicker="1" type="text" name="nd_learning_meta_box_date7" value="<?php echo $nd_learning_meta_box_date7; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end7" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end7" value="<?php echo $nd_learning_meta_box_date_end7; ?>" />
					</td>
				</tr>
				
				<!-- 8 -->
				<tr>
					<td>
						8
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check8" class="selectit">
							<input id="nd_learning_meta_box_date_check8" name="nd_learning_meta_box_date_check8" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check8=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker8" data-dapicker="1" type="text" name="nd_learning_meta_box_date8" value="<?php echo $nd_learning_meta_box_date8; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end8" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end8" value="<?php echo $nd_learning_meta_box_date_end8; ?>" />
					</td>
				</tr>
				
				<!-- 9 -->
				<tr>
					<td>
						9
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check9" class="selectit">
							<input id="nd_learning_meta_box_date_check9" name="nd_learning_meta_box_date_check9" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check9=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker9" data-dapicker="1" type="text" name="nd_learning_meta_box_date9" value="<?php echo $nd_learning_meta_box_date9; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end9" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end9" value="<?php echo $nd_learning_meta_box_date_end9; ?>" />
					</td>
				</tr>
				
				<!-- 10 -->
				<tr>
					<td>
						10
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check10" class="selectit">
							<input id="nd_learning_meta_box_date_check10" name="nd_learning_meta_box_date_check10" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check10=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker10" data-dapicker="1" type="text" name="nd_learning_meta_box_date10" value="<?php echo $nd_learning_meta_box_date10; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end10" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end10" value="<?php echo $nd_learning_meta_box_date_end10; ?>" />
					</td>
				</tr>
				
				<!-- 11 -->
				<tr>
					<td>
						11
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check11" class="selectit">
							<input id="nd_learning_meta_box_date_check11" name="nd_learning_meta_box_date_check11" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check11=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker11" data-dapicker="1" type="text" name="nd_learning_meta_box_date11" value="<?php echo $nd_learning_meta_box_date11; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end11" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end11" value="<?php echo $nd_learning_meta_box_date_end11; ?>" />
					</td>
				</tr>
				
				<!-- 12 -->
				<tr>
					<td>
						12
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check12" class="selectit">
							<input id="nd_learning_meta_box_date_check12" name="nd_learning_meta_box_date_check12" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check12=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker12" data-dapicker="1" type="text" name="nd_learning_meta_box_date12" value="<?php echo $nd_learning_meta_box_date12; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end12" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end12" value="<?php echo $nd_learning_meta_box_date_end12; ?>" />
					</td>
				</tr>
				
				<!-- 13 -->
				<tr>
					<td>
						13
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check13" class="selectit">
							<input id="nd_learning_meta_box_date_check13" name="nd_learning_meta_box_date_check13" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check13=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker13" data-dapicker="1" type="text" name="nd_learning_meta_box_date13" value="<?php echo $nd_learning_meta_box_date13; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end13" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end13" value="<?php echo $nd_learning_meta_box_date_end13; ?>" />
					</td>
				</tr>
				
				<!-- 14 -->
				<tr>
					<td>
						14
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check14" class="selectit">
							<input id="nd_learning_meta_box_date_check14" name="nd_learning_meta_box_date_check14" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check14=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker14" data-dapicker="1" type="text" name="nd_learning_meta_box_date14" value="<?php echo $nd_learning_meta_box_date14; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end14" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end14" value="<?php echo $nd_learning_meta_box_date_end14; ?>" />
					</td>
				</tr>
				
				<!-- 15 -->
				<tr>
					<td>
						15
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check15" class="selectit">
							<input id="nd_learning_meta_box_date_check15" name="nd_learning_meta_box_date_check15" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check15=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker15" data-dapicker="1" type="text" name="nd_learning_meta_box_date15" value="<?php echo $nd_learning_meta_box_date15; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end15" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end15" value="<?php echo $nd_learning_meta_box_date_end15; ?>" />
					</td>
				</tr>
				
				<!-- 16 -->
				<tr>
					<td>
						16
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check16" class="selectit">
							<input id="nd_learning_meta_box_date_check16" name="nd_learning_meta_box_date_check16" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check16=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker16" data-dapicker="1" type="text" name="nd_learning_meta_box_date16" value="<?php echo $nd_learning_meta_box_date16; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end16" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end16" value="<?php echo $nd_learning_meta_box_date_end16; ?>" />
					</td>
				</tr>
				
				<!-- 17 -->
				<tr>
					<td>
						17
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check17" class="selectit">
							<input id="nd_learning_meta_box_date_check17" name="nd_learning_meta_box_date_check17" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check17=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker17" data-dapicker="1" type="text" name="nd_learning_meta_box_date17" value="<?php echo $nd_learning_meta_box_date17; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end17" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end17" value="<?php echo $nd_learning_meta_box_date_end17; ?>" />
					</td>
				</tr>
				
				<!-- 18 -->
				<tr>
					<td>
						18
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check18" class="selectit">
							<input id="nd_learning_meta_box_date_check18" name="nd_learning_meta_box_date_check18" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check18=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker18" data-dapicker="1" type="text" name="nd_learning_meta_box_date18" value="<?php echo $nd_learning_meta_box_date18; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end18" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end18" value="<?php echo $nd_learning_meta_box_date_end18; ?>" />
					</td>
				</tr>
				
				<!-- 19 -->
				<tr>
					<td>
						19
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check19" class="selectit">
							<input id="nd_learning_meta_box_date_check19" name="nd_learning_meta_box_date_check19" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check19=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker19" data-dapicker="1" type="text" name="nd_learning_meta_box_date19" value="<?php echo $nd_learning_meta_box_date19; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end19" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end19" value="<?php echo $nd_learning_meta_box_date_end19; ?>" />
					</td>
				</tr>
				
				<!-- 20 -->
				<tr>
					<td>
						20
					</td>
					<td>
						<label for="nd_learning_meta_box_date_check20" class="selectit">
							<input id="nd_learning_meta_box_date_check20" name="nd_learning_meta_box_date_check20" type="checkbox"
								   <?php if($nd_learning_meta_box_date_check20=='on') echo 'checked="checked"' ?>
							>
						</label>
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker20" data-dapicker="1" type="text" name="nd_learning_meta_box_date20" value="<?php echo $nd_learning_meta_box_date20; ?>" />
					</td>

					<td style="text-align: center;">
						<input style="width:auto;" id="nd_learning_datepicker_end20" data-dapicker="1" type="text" name="nd_learning_meta_box_date_end20" value="<?php echo $nd_learning_meta_box_date_end20; ?>" />
					</td>
				</tr>
				
			</tbody>
		</table>
	</div>
	
	

	
	

	<script type="text/javascript">
		//<![CDATA[

		jQuery(document).ready(function () {
			jQuery('[data-dapicker]').datepicker({
				dateFormat: 'dd-mm-yy'
			});
		});

		//]]>
	</script>



	<p><strong><?php _e('Color', 'nd-learning'); ?></strong></p>
	<p><input id="nd_learning_colorpicker" type="text" name="nd_learning_meta_box_color" id="nd_learning_meta_box_color" value="<?php echo $nd_learning_meta_box_color; ?>" /></p>

	<script type="text/javascript">
		//<![CDATA[

		jQuery(document).ready(function ($) {
			$('#nd_learning_colorpicker').iris();
		});

		//]]>
	</script>





	<p><strong><?php _e('CF7 Form', 'nd-learning'); ?></strong></p>
	<p>
		<select name="nd_learning_meta_box_form">
			<?php
			$nd_learning_meta_box_form = get_post_meta(get_the_ID(), 'nd_learning_meta_box_form', true);
			$nd_learning_forms_args = array('posts_per_page' => -1, 'post_type' => 'wpcf7_contact_form');
			$nd_learning_forms = get_posts($nd_learning_forms_args);
			?>
			<?php foreach ($nd_learning_forms as $nd_learning_form) : ?>
				<option 

					<?php
					if ($nd_learning_meta_box_form == $nd_learning_form->ID) {
						echo 'selected="selected"';
					}
					?>

					value="<?php echo $nd_learning_form->ID; ?>">
						<?php echo $nd_learning_form->post_title; ?>
				</option>
			<?php endforeach; ?>
		</select>
	</p>




	<?php
}

add_action('save_post', 'nd_learning_meta_box_save');

function nd_learning_meta_box_save($post_id) {

	// FECHAS INICIO
	if (isset($_POST['nd_learning_meta_box_id_modulo']))
		update_post_meta($post_id, 'nd_learning_meta_box_id_modulo', wp_kses($_POST['nd_learning_meta_box_id_modulo'], $allowed));
	
	if (isset($_POST['nd_learning_meta_box_price']))
		update_post_meta($post_id, 'nd_learning_meta_box_price', wp_kses($_POST['nd_learning_meta_box_price'], $allowed));

	if (isset($_POST['nd_learning_meta_box_max_availability']))
		update_post_meta($post_id, 'nd_learning_meta_box_max_availability', wp_kses($_POST['nd_learning_meta_box_max_availability'], $allowed));

	if (isset($_POST['nd_learning_meta_box_teacher']))
		update_post_meta($post_id, 'nd_learning_meta_box_teacher', wp_kses($_POST['nd_learning_meta_box_teacher'], $allowed));

	if (isset($_POST['nd_learning_meta_box_teachers']))
		update_post_meta($post_id, 'nd_learning_meta_box_teachers', wp_kses($_POST['nd_learning_meta_box_teachers'], $allowed));

	if (isset($_POST['nd_learning_meta_box_form']))
		update_post_meta($post_id, 'nd_learning_meta_box_form', wp_kses($_POST['nd_learning_meta_box_form'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date']))
		update_post_meta($post_id, 'nd_learning_meta_box_date', wp_kses($_POST['nd_learning_meta_box_date'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date2']))
		update_post_meta($post_id, 'nd_learning_meta_box_date2', wp_kses($_POST['nd_learning_meta_box_date2'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date3']))
		update_post_meta($post_id, 'nd_learning_meta_box_date3', wp_kses($_POST['nd_learning_meta_box_date3'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date4']))
		update_post_meta($post_id, 'nd_learning_meta_box_date4', wp_kses($_POST['nd_learning_meta_box_date4'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date5']))
		update_post_meta($post_id, 'nd_learning_meta_box_date5', wp_kses($_POST['nd_learning_meta_box_date5'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date6']))
		update_post_meta($post_id, 'nd_learning_meta_box_date6', wp_kses($_POST['nd_learning_meta_box_date6'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date7']))
		update_post_meta($post_id, 'nd_learning_meta_box_date7', wp_kses($_POST['nd_learning_meta_box_date7'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date8']))
		update_post_meta($post_id, 'nd_learning_meta_box_date8', wp_kses($_POST['nd_learning_meta_box_date8'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date9']))
		update_post_meta($post_id, 'nd_learning_meta_box_date9', wp_kses($_POST['nd_learning_meta_box_date9'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date10']))
		update_post_meta($post_id, 'nd_learning_meta_box_date10', wp_kses($_POST['nd_learning_meta_box_date10'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date11']))
		update_post_meta($post_id, 'nd_learning_meta_box_date11', wp_kses($_POST['nd_learning_meta_box_date11'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date12']))
		update_post_meta($post_id, 'nd_learning_meta_box_date12', wp_kses($_POST['nd_learning_meta_box_date12'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date13']))
		update_post_meta($post_id, 'nd_learning_meta_box_date13', wp_kses($_POST['nd_learning_meta_box_date13'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date14']))
		update_post_meta($post_id, 'nd_learning_meta_box_date14', wp_kses($_POST['nd_learning_meta_box_date14'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date15']))
		update_post_meta($post_id, 'nd_learning_meta_box_date15', wp_kses($_POST['nd_learning_meta_box_date15'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date16']))
		update_post_meta($post_id, 'nd_learning_meta_box_date16', wp_kses($_POST['nd_learning_meta_box_date16'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date17']))
		update_post_meta($post_id, 'nd_learning_meta_box_date17', wp_kses($_POST['nd_learning_meta_box_date17'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date18']))
		update_post_meta($post_id, 'nd_learning_meta_box_date18', wp_kses($_POST['nd_learning_meta_box_date18'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date19']))
		update_post_meta($post_id, 'nd_learning_meta_box_date19', wp_kses($_POST['nd_learning_meta_box_date19'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date20']))
		update_post_meta($post_id, 'nd_learning_meta_box_date20', wp_kses($_POST['nd_learning_meta_box_date20'], $allowed));
	
	
	// FECHAS FIN
	if (isset($_POST['nd_learning_meta_box_date_end']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end', wp_kses($_POST['nd_learning_meta_box_date_end'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end2']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end2', wp_kses($_POST['nd_learning_meta_box_date_end2'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end3']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end3', wp_kses($_POST['nd_learning_meta_box_date_end3'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end4']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end4', wp_kses($_POST['nd_learning_meta_box_date_end4'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end5']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end5', wp_kses($_POST['nd_learning_meta_box_date_end5'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end6']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end6', wp_kses($_POST['nd_learning_meta_box_date_end6'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end7']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end7', wp_kses($_POST['nd_learning_meta_box_date_end7'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end8']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end8', wp_kses($_POST['nd_learning_meta_box_date_end8'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end9']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end9', wp_kses($_POST['nd_learning_meta_box_date_end9'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end10']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end10', wp_kses($_POST['nd_learning_meta_box_date_end10'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end11']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end11', wp_kses($_POST['nd_learning_meta_box_date_end11'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end12']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end12', wp_kses($_POST['nd_learning_meta_box_date_end12'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end13']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end13', wp_kses($_POST['nd_learning_meta_box_date_end13'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end14']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end14', wp_kses($_POST['nd_learning_meta_box_date_end14'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end15']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end15', wp_kses($_POST['nd_learning_meta_box_date_end15'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end16']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end16', wp_kses($_POST['nd_learning_meta_box_date_end16'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end17']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end17', wp_kses($_POST['nd_learning_meta_box_date_end17'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end18']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end18', wp_kses($_POST['nd_learning_meta_box_date_end18'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end19']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end19', wp_kses($_POST['nd_learning_meta_box_date_end19'], $allowed));

	if (isset($_POST['nd_learning_meta_box_date_end20']))
		update_post_meta($post_id, 'nd_learning_meta_box_date_end20', wp_kses($_POST['nd_learning_meta_box_date_end20'], $allowed));

	
	// FECHA HOMOLOGABLE
	if (isset($_POST['nd_learning_meta_box_date_check'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check', wp_kses($_POST['nd_learning_meta_box_date_check'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check2'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check2', wp_kses($_POST['nd_learning_meta_box_date_check2'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check2', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check3'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check3', wp_kses($_POST['nd_learning_meta_box_date_check3'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check3', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check4'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check4', wp_kses($_POST['nd_learning_meta_box_date_check4'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check4', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check5'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check5', wp_kses($_POST['nd_learning_meta_box_date_check5'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check5', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check6'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check6', wp_kses($_POST['nd_learning_meta_box_date_check6'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check6', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check7'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check7', wp_kses($_POST['nd_learning_meta_box_date_check7'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check7', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check8'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check8', wp_kses($_POST['nd_learning_meta_box_date_check8'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check8', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check9'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check9', wp_kses($_POST['nd_learning_meta_box_date_check9'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check9', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check10'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check10', wp_kses($_POST['nd_learning_meta_box_date_check10'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check10', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check11'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check11', wp_kses($_POST['nd_learning_meta_box_date_check11'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check11', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check12'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check12', wp_kses($_POST['nd_learning_meta_box_date_check12'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check12', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check13'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check13', wp_kses($_POST['nd_learning_meta_box_date_check13'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check13', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check14'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check14', wp_kses($_POST['nd_learning_meta_box_date_check14'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check14', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check15'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check15', wp_kses($_POST['nd_learning_meta_box_date_check15'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check15', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check16'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check16', wp_kses($_POST['nd_learning_meta_box_date_check16'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check16', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check17'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check17', wp_kses($_POST['nd_learning_meta_box_date_check17'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check17', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check18'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check18', wp_kses($_POST['nd_learning_meta_box_date_check18'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check18', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check19'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check19', wp_kses($_POST['nd_learning_meta_box_date_check19'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check19', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_date_check20'])){
		update_post_meta($post_id, 'nd_learning_meta_box_date_check20', wp_kses($_POST['nd_learning_meta_box_date_check20'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_date_check20', 'off');
	}
	
	
	if (isset($_POST['nd_learning_meta_box_color']))
		update_post_meta($post_id, 'nd_learning_meta_box_color', wp_kses($_POST['nd_learning_meta_box_color'], $allowed));
	
	if (isset($_POST['nd_learning_meta_box_MECD'])){
		update_post_meta($post_id, 'nd_learning_meta_box_MECD', wp_kses($_POST['nd_learning_meta_box_MECD'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_MECD', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_URJC'])){
		update_post_meta($post_id, 'nd_learning_meta_box_URJC', wp_kses($_POST['nd_learning_meta_box_URJC'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_URJC', 'off');
	}
	if (isset($_POST['nd_learning_meta_box_nuevo'])){
		update_post_meta($post_id, 'nd_learning_meta_box_nuevo', wp_kses($_POST['nd_learning_meta_box_nuevo'], $allowed));
	}else{
		update_post_meta($post_id, 'nd_learning_meta_box_nuevo', 'off');
	}
	
	if (isset($_POST['nd_learning_meta_box_video']))
		update_post_meta($post_id, 'nd_learning_meta_box_video', wp_kses($_POST['nd_learning_meta_box_video'], $allowed));
}

/* * *****************************HEADER IMG***************************** */

add_action('add_meta_boxes', 'nd_learning_metabox_courses_header_img');

function nd_learning_metabox_courses_header_img() {
	add_meta_box('nd-learning-meta-box-course-header-img-id', __('Header Image', 'nd-learning'), 'nd_learning_metabox_course_header_img', 'courses', 'normal', 'high');
}

function nd_learning_metabox_course_header_img() {


	// $post is already set, and contains an object: the WordPress post
	global $post;
	$nd_learning_values = get_post_custom($post->ID);

	$nd_learning_meta_box_course_header_img = get_post_meta(get_the_ID(), 'nd_learning_meta_box_course_header_img', true);
	$nd_learning_meta_box_course_header_img_title = get_post_meta(get_the_ID(), 'nd_learning_meta_box_course_header_img_title', true);
	$nd_learning_meta_box_course_header_img_position = get_post_meta(get_the_ID(), 'nd_learning_meta_box_course_header_img_position', true);
	?>

	
	<!--******************************IMAGE******************************-->
	<p><strong><?php _e('Header Image', 'nd-learning'); ?></strong></p>
	<p><input class="large-text" type="text" name="nd_learning_meta_box_course_header_img" id="nd_learning_meta_box_course_header_img" value="<?php echo $nd_learning_meta_box_course_header_img; ?>" /></p>
	<p>
		<input class="button nd_learning_meta_box_course_header_img_button" type="button" name="nd_learning_meta_box_course_header_img_button" id="nd_learning_meta_box_course_header_img_button" value="<?php _e('Upload', 'nd-learning'); ?>" />
	</p>


	<!--******************************POSITION******************************-->
	<p><strong><?php _e('Image Position', 'nd-learning'); ?></strong></p>
	<p>
		<select name="nd_learning_meta_box_course_header_img_position" id="nd_learning_meta_box_course_header_img_position">

			<option <?php
			if ($nd_learning_meta_box_course_header_img_position == 'nd_learning_background_position_center_top') {
				echo 'selected="selected"';
			}
			?> value="nd_learning_background_position_center_top">Position Top</option>
			<option <?php
			if ($nd_learning_meta_box_course_header_img_position == 'nd_learning_background_position_center_bottom') {
				echo 'selected="selected"';
			}
			?> value="nd_learning_background_position_center_bottom">Position Bottom</option>
			<option <?php
			if ($nd_learning_meta_box_course_header_img_position == 'nd_learning_background_position_center') {
				echo 'selected="selected"';
			}
			?> value="nd_learning_background_position_center">Position Center</option>

		</select>
	</p>


	<!--******************************TITLE******************************-->
	<p><strong><?php _e('Title', 'nd-learning'); ?></strong></p>
	<p><input id="nd_learning_meta_box_course_header_img_title" type="text" name="nd_learning_meta_box_course_header_img_title" id="nd_learning_meta_box_course_header_img_title" value="<?php echo $nd_learning_meta_box_course_header_img_title; ?>" /></p>
	<p class="description"><?php _e('Insert the title/slogan over the image', 'nd-learning'); ?></p>



	<!--/* IMAGE VIDEO */-->
	<script type="text/javascript">
		//<![CDATA[

		jQuery(document).ready(function () {
			jQuery(function ($) {
				var file_frame = [],
					$button = $('.nd_learning_meta_box_course_header_img_button_video');
				$('#nd_learning_meta_box_course_header_img_button_video').click(function () {
					var $this = $(this),
						id = $this.attr('id');
					// If the media frame already exists, reopen it.
					if (file_frame[ id ]) {
						file_frame[ id ].open();
						return;
					}
					// Create the media frame.
					file_frame[ id ] = wp.media.frames.file_frame = wp.media({
						title: $this.data('uploader_title'),
						button: {
							text: $this.data('uploader_button_text')
						},
						multiple: false  // Set to true to allow multiple files to be selected
					});
					// When an image is selected, run a callback.
					file_frame[ id ].on('select', function () {
						// We set multiple to false so only get one image from the uploader
						var attachment = file_frame[ id ].state().get('selection').first().toJSON();
						$('#nd_learning_meta_box_course_header_img_video').val(attachment.url);
					});
					// Finally, open the modal
					file_frame[ id ].open();
				});
			});
		});
		//]]>
	</script>
	<!--/* IMAGE HEADER */-->
	<script type="text/javascript">
		//<![CDATA[
		jQuery(document).ready(function () {
			jQuery(function ($) {
				var file_frame = [],
					$button = $('.nd_learning_meta_box_course_header_img_button');
				$('#nd_learning_meta_box_course_header_img_button').click(function () {
					var $this = $(this),
						id = $this.attr('id');
					// If the media frame already exists, reopen it.
					if (file_frame[ id ]) {
						file_frame[ id ].open();
						return;
					}
					// Create the media frame.
					file_frame[ id ] = wp.media.frames.file_frame = wp.media({
						title: $this.data('uploader_title'),
						button: {
							text: $this.data('uploader_button_text')
						},
						multiple: false  // Set to true to allow multiple files to be selected
					});
					// When an image is selected, run a callback.
					file_frame[ id ].on('select', function () {
						// We set multiple to false so only get one image from the uploader
						var attachment = file_frame[ id ].state().get('selection').first().toJSON();
						$('#nd_learning_meta_box_course_header_img').val(attachment.url);
					});
					// Finally, open the modal
					file_frame[ id ].open();
				});
			});
		});
		//]]>
	</script>


	<?php
}

add_action('save_post', 'nd_learning_meta_box_course_header_img_save');
add_action('save_post', 'nd_learning_meta_box_course_header_img_video_save');

function nd_learning_meta_box_course_header_img_save($post_id) {

	// Make sure your data is set before trying to save it
	if (isset($_POST['nd_learning_meta_box_course_header_img']))
		update_post_meta($post_id, 'nd_learning_meta_box_course_header_img', wp_kses($_POST['nd_learning_meta_box_course_header_img'], $allowed));

	if (isset($_POST['nd_learning_meta_box_course_header_img_title']))
		update_post_meta($post_id, 'nd_learning_meta_box_course_header_img_title', wp_kses($_POST['nd_learning_meta_box_course_header_img_title'], $allowed));

	if (isset($_POST['nd_learning_meta_box_course_header_img_position']))
		update_post_meta($post_id, 'nd_learning_meta_box_course_header_img_position', wp_kses($_POST['nd_learning_meta_box_course_header_img_position'], $allowed));
}
function nd_learning_meta_box_course_header_img_video_save($post_id) {
	// Make sure your data is set before trying to save it
	if (isset($_POST['nd_learning_meta_box_course_header_img_video']))
		update_post_meta($post_id, 'nd_learning_meta_box_course_header_img_video', wp_kses($_POST['nd_learning_meta_box_course_header_img_video'], $allowed));
}

/* * *****************************ATTENDEES***************************** */

add_action('add_meta_boxes', 'nd_learning_metabox_courses_attendees');

function nd_learning_metabox_courses_attendees() {
	add_meta_box('nd-learning-meta-box-course-attendees', __('Course Attendees', 'nd-learning'), 'nd_learning_metabox_course_attendees', 'courses', 'normal', 'high');
}

function nd_learning_metabox_course_attendees() {


	global $wpdb;

	$nd_learning_result = '';
	$nd_learning_course_id = get_the_ID();
	$nd_learning_table_name = $wpdb->prefix . 'nd_learning_courses';

	if (nd_learning_get_course_price($nd_learning_course_id) == 0) {
		$nd_learning_action_type = "'free'";
	} else {
		$nd_learning_action_type = "'paypal'";
	}

	//START select for items
	$nd_learning_attendees = $wpdb->get_results("SELECT id_user FROM $nd_learning_table_name WHERE id_course = $nd_learning_course_id AND action_type = $nd_learning_action_type");


	if (empty($nd_learning_attendees)) {

		$nd_learning_result .= '
    <div class="nd_learning_position_relative  nd_learning_width_100_percentage nd_learning_box_sizing_border_box nd_learning_display_inline_block">           
      <p class=" nd_learning_margin_0 nd_learning_padding_0">' . __('Still no participant', 'nd-learning') . '</p>
    </div>';
	} else {

		foreach ($nd_learning_attendees as $nd_learning_attendee) {

			$nd_learning_attendees_avatar_url_args = array(
				'size' => 300
			);

			$nd_learning_attendees_avatar_url = get_avatar_url($nd_learning_attendee->id_user, $nd_learning_attendees_avatar_url_args);
			$nd_learning_attendee_id = $nd_learning_attendee->id_user;

			$nd_learning_result .= '
                                 
          <!--START preview-->
          <div class="nd_learning_position_relative nd_learning_padding_10 nd_learning_width_100_percentage nd_learning_box_sizing_border_box nd_learning_display_inline_block nd_learning_border_bottom_1_solid_eee">           
            <img width="35" alt="" class="nd_learning_top_10 nd_learning_left_0 nd_learning_position_absolute" src="' . $nd_learning_attendees_avatar_url . '">
            <p class="nd_learning_margin_left_35 nd_learning_margin_0 nd_learning_padding_0"><strong>' . get_userdata($nd_learning_attendee_id)->user_login . '</strong> ' . get_userdata($nd_learning_attendee_id)->first_name . ' ' . get_userdata($nd_learning_attendee_id)->last_name . '</p>
            <p class="nd_learning_margin_left_35 nd_learning_margin_0 nd_learning_padding_0"><a class="nd_learning_text_decoration_initial" href="mailto:' . get_userdata($nd_learning_attendee_id)->user_email . '">' . get_userdata($nd_learning_attendee_id)->user_email . '</a> -> <a href="user-edit.php?user_id=' . $nd_learning_attendee_id . '" target="_blank" class="nd_learning_text_decoration_initial">' . __('View Profile', 'nd-learning') . '</a></p>
          </div>               
          <!--END preview-->

      ';
		}
	}

	echo $nd_learning_result;
}
