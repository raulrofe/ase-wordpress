<div class="nd_learning_section nd_learning_position_relative">
	<div class="wpb_wrapper">
		<div class="vc_tta-container" data-vc-action="collapseAll">
			<div class="vc_general vc_tta vc_tta-accordion vc_tta-color-grey vc_tta-style-classic vc_tta-shape-square vc_tta-o-shape-group vc_tta-controls-align-left">
				<div class="vc_tta-panels-container">
					<div class="vc_tta-panels">
						<div class="vc_tta-panel" id="ASE" data-vc-content=".vc_tta-panel-body">
							<div class="vc_tta-panel-heading">
								<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-left">
									<a href="#ASE" data-vc-accordion="" data-vc-container=".vc_tta-container">
										<i class="fa fa-graduation-cap"></i>
										<span class="vc_tta-title-text"><?php echo __('TitulacionPropiaASE', 'custom-translation') ?></span>
										<i class="vc_tta-controls-icon vc_tta-controls-icon-chevron"></i>
									</a>
								</h4>
							</div>
							<div class="vc_tta-panel-body">
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper">
												<h2 style="color: #222222;text-align: left;font-family:Montserrat;font-weight:400;font-style:normal" class="vc_custom_heading"><?php echo __('QueEs', 'custom-translation') ?></h2>
												<div class="vc_empty_space" style="height: 8px">
													<span class="vc_empty_space_inner"></span>
												</div>

												<div class="wpb_text_column wpb_content_element ">
													<div class="wpb_wrapper">
														<p style="text-align: left;">
															<?php echo __('TitulacionPropiaASEInfo', 'custom-translation') ?>
														</p>
													</div>
												</div>
												<div class="vc_empty_space" style="height: 4px">
													<span class="vc_empty_space_inner"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="wpb_column vc_column_container vc_col-sm-12">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper">
												<h2 style="color: #222222;text-align: left;font-family:Montserrat;font-weight:400;font-style:normal" class="vc_custom_heading"><?php echo __('Requisitos', 'custom-translation') ?></h2>
												<div class="vc_empty_space" style="height: 12px">
													<span class="vc_empty_space_inner"></span>
												</div>
												<div class="wpb_text_column wpb_content_element ">
													<div class="wpb_wrapper">
														<p style="text-align: left;">
															<?php echo __('RequisitosInfo', 'custom-translation') ?>
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner ">
											<div class="wpb_wrapper">
												<h2 style="color: #222222;text-align: left;font-family:Montserrat;font-weight:400;font-style:normal" class="vc_custom_heading"><?php echo __('ItinerarioFormativo', 'custom-translation') ?></h2>
												<div class="vc_empty_space" style="height: 14px">
													<span class="vc_empty_space_inner"></span>
												</div>
												<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_top-to-bottom wpb_start_animation">
													<div class="wpb_wrapper">
														<p style="text-align: left;">
															<?php echo __('ItinerarioFormativoInfo', 'custom-translation') ?>
														</p>
														<p style="text-align: left;">
															<?php echo __('ItinerarioFormativoInfo2', 'custom-translation') ?>
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<section class="vc_cta3-container">
									<div class="vc_general vc_cta3 vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-center vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
										<div class="vc_cta3_content-container">
											<div class="vc_cta3-content">
												<header class="vc_cta3-content-header">
													<h2><?php echo __('CursosAS', 'custom-translation') ?></h2>						
												</header>
												<p><?php echo __('CursosASInfo', 'custom-translation') ?></p>
											</div>
											<div class="vc_cta3-actions">
												<div class="vc_btn3-container vc_btn3-center">
													<a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-block vc_btn3-icon-left vc_btn3-color-primary" 
													   href="http://aulasmarteditorial.com/courses/?nd_learning_arrive_from_advsearch=true" title="">
														<i class="vc_btn3-icon fa fa-adjust"></i> <?php echo __('Acceder', 'custom-translation') ?>
													</a>
												</div>
											</div>				
										</div>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>