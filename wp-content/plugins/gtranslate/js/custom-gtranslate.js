jQuery('.switcher2 .selected').click(function () {
	if (!(jQuery('.switcher2 .option').is(':visible'))) {
		jQuery('.switcher2 .option').stop(true, true).delay(100).slideDown(500);
		jQuery('.switcher2 .selected a').toggleClass('open')
	}
});

jQuery('.switcher2 .option').bind('mousewheel', function (e) {
	var options = jQuery('.switcher2 .option');
	if (options.is(':visible'))
		options.scrollTop(options.scrollTop() - e.originalEvent.wheelDelta);
	return false;
});

jQuery('body').not('.switcher2').mousedown(function (e) {
	if (jQuery('.switcher2 .option').is(':visible') && e.target != jQuery('.switcher2 .option').get(0)) {
		jQuery('.switcher2 .option').stop(true, true).delay(100).slideUp(500);
		jQuery('.switcher2 .selected a').toggleClass('open')
	}
});

