<?php

// CUSTOM WORDPRESS


$nicdark_themename = "educationpack";

//TGMPA required plugin
require_once get_template_directory() . '/class-tgm-plugin-activation.php';
add_action('tgmpa_register', 'nicdark_register_required_plugins');

function nicdark_register_required_plugins() {

	$nicdark_plugins = array(
		//learning
		array(
			'name' => esc_html__('Learning Courses', 'educationpack'),
			'slug' => 'nd-learning',
			'required' => true,
		),
		//cf7
		array(
			'name' => esc_html__('Contact Form 7', 'educationpack'),
			'slug' => 'contact-form-7',
			'required' => true,
		),
		//wp import
		array(
			'name' => esc_html__('Wordpress Importer', 'educationpack'),
			'slug' => 'wordpress-importer',
			'required' => true,
		),
		//nd shortcodes
		array(
			'name' => esc_html__('ND Shortcodes', 'educationpack'),
			'slug' => 'nd-shortcodes',
			'required' => true,
		),
		//revslider
		array(
			'name' => esc_html__('Revolution Slider', 'educationpack'),
			'slug' => 'revslider', // The plugin slug (typically the folder name).
			'source' => esc_url('http://www.nicdarkthemes.com/themes/education/wp/plugins/revslider.zip'), // The plugin source.
			'required' => true, // If false, the plugin is only 'recommended' instead of required.
		),
		//Visual Composer
		array(
			'name' => esc_html__('Visual Composer', 'educationpack'),
			'slug' => 'js_composer', // The plugin slug (typically the folder name).
			'source' => esc_url('http://www.nicdarkthemes.com/themes/education/wp/plugins/js_composer.zip'), // The plugin source.
			'required' => true, // If false, the plugin is only 'recommended' instead of required.
		),
		//Time Table
		array(
			'name' => esc_html__('Time Table', 'educationpack'),
			'slug' => 'timetable', // The plugin slug (typically the folder name).
			'source' => esc_url('http://www.nicdarkthemes.com/themes/education/wp/plugins/timetable.zip'), // The plugin source.
			'required' => true, // If false, the plugin is only 'recommended' instead of required.
		),
		//WooCommerce
		array(
			'name' => esc_html__('Woo Commerce', 'educationpack'),
			'slug' => 'woocommerce', // The plugin slug (typically the folder name).
			'source' => esc_url('http://www.nicdarkthemes.com/themes/education/wp/plugins/woocommerce.zip'), // The plugin source.
			'required' => true, // If false, the plugin is only 'recommended' instead of required.
		),
	);


	$nicdark_config = array(
		'id' => 'educationpack', // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '', // Default absolute path to bundled plugins.
		'menu' => 'tgmpa-install-plugins', // Menu slug.
		'has_notices' => true, // Show admin notices or not.
		'dismissable' => true, // If false, a user cannot dismiss the nag message.
		'dismiss_msg' => '', // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false, // Automatically activate plugins after installation or not.
		'message' => '', // Message to output right before the plugins table.
	);

	tgmpa($nicdark_plugins, $nicdark_config);
}

//END tgmpa
//translation
load_theme_textdomain('educationpack', get_template_directory() . '/languages');

//register my menus
function nicdark_register_my_menus() {
	register_nav_menu('main-menu', esc_html__('Main Menu', 'educationpack'));
}

add_action('init', 'nicdark_register_my_menus');


//Content_width
if (!isset($content_width))
	$content_width = 1180;


//automatic-feed-links
add_theme_support('automatic-feed-links');

//post-formats
add_theme_support('post-formats', array('quote', 'image', 'link', 'video', 'gallery', 'audio'));

//title tag
add_theme_support('title-tag');

// Sidebar
function nicdark_add_sidebars() {

	// Sidebar Main
	register_sidebar(array(
		'name' => esc_html__('Sidebar', 'educationpack'),
		'id' => 'nicdark_sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}

add_action('widgets_init', 'nicdark_add_sidebars');

//add css and js
function nicdark_enqueue_scripts() {

	//css
	wp_enqueue_style('nicdark-style', get_stylesheet_uri());
	wp_enqueue_style('nicdark-fonts', nicdark_google_fonts_url(), array(), '1.0.0');

	//comment-reply
	if (is_singular())
		wp_enqueue_script('comment-reply');
}

add_action("wp_enqueue_scripts", "nicdark_enqueue_scripts");
//end js
//logo settings
add_action('customize_register', 'nicdark_customizer_logo');

function nicdark_customizer_logo($wp_customize) {


	//Logo
	$wp_customize->add_setting('nicdark_customizer_logo_img', array(
		'type' => 'option', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => '',
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => 'nicdark_sanitize_callback_logo_img',
		//'sanitize_js_callback' => '', // Basically to_json.
	));
	$wp_customize->add_control(
		new WP_Customize_Media_Control(
		$wp_customize, 'nicdark_customizer_logo_img', array(
		'label' => esc_html__('Logo', 'educationpack'),
		'section' => 'title_tagline',
		'mime_type' => 'image',
		)
		)
	);

	//sanitize_callback
	function nicdark_sanitize_callback_logo_img($nicdark_logo_img_value) {
		return absint($nicdark_logo_img_value);
	}

}

//end logo settings
//woocommerce support
add_theme_support('woocommerce');

//define nicdark theme option
function nicdark_theme_setup() {
	add_option('nicdark_theme_author', 1, '', 'yes');
	update_option('nicdark_theme_author', 1);
}

add_action('after_setup_theme', 'nicdark_theme_setup');

//START add google fonts
function nicdark_google_fonts_url() {
	$nicdark_font_url = '';
	if ('off' !== _x('on', 'Google font: on or off', 'educationpack')) {
		$nicdark_font_url = add_query_arg('family', urlencode('Montserrat:400,700|Varela Round'), "//fonts.googleapis.com/css");
	}
	return $nicdark_font_url;
}

add_filter('widget_text', 'shortcode_unautop');
add_filter('widget_text', 'do_shortcode');
//END add google fonts
//
// CUSTOM WORDPRES
// NO UPDATE PLUGIN, THEMES OR WP
add_action('init', create_function('$a', "remove_action( 'init', 'wp_version_check' );"), 2);
add_filter('pre_option_update_core', create_function('$a', "return null;"));
remove_action('load-update-core.php', 'wp_update_plugins');
add_filter('pre_site_transient_update_plugins', create_function('$a', "return null;"));

// Truncate extracts (overwrite WP)
function wpdocs_custom_excerpt_length($length) {
	return 20;
}

// Truncate extracts
add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 999);

function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt) >= $limit) {
		array_pop($excerpt);
		$excerpt = implode(" ", $excerpt) . '...';
	} else {
		$excerpt = implode(" ", $excerpt);
	}
	$excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
	return $excerpt;
}

function content($limit) {
	$content = explode(' ', get_the_content(), $limit);
	if (count($content) >= $limit) {
		array_pop($content);
		$content = implode(" ", $content) . '...';
	} else {
		$content = implode(" ", $content);
	}
	$content = preg_replace('/\[.+\]/', '', $content);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}

// FUNCTIONS TO CAMPUS DATABASE
function connectDBCampus() {
	$host_name = "87.106.222.75";
	$database = "u75088563_campusaulainteractiva";
	$user_name = "user_campusvirtu";
	$password = "PBD_u75088563";
	$conn = mysqli_connect($host_name, $user_name, $password, $database);
	if (mysqli_connect_errno()) {
		echo '<p>Error al conectar con servidor MySQL: ' . mysqli_connect_error() . '</p>';
		return;
	}
	$conn->query("SET NAMES utf8");
	return $conn;
}

// FUNCTIONS TO CAMPUS DATABASE
function matricularIberform($post) {
	$post['id_modulo'] = get_post_meta($post['idCurso'], 'nd_learning_meta_box_id_modulo', true);

	$curl_handle=curl_init();
	curl_setopt($curl_handle,CURLOPT_URL,'http://aulasmart.iberform.com/index.php?c=script_matricula_exterior&m=matricular');
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $post);
	$buffer = curl_exec($curl_handle);
	curl_close($curl_handle);
	if (!empty($buffer)){
		return $buffer;
	}else{
		return false;
	}
}

// CRUD AÑADIR PROFESORES A TARIFA PLANA
function anadirProfesorTarifaAse($post) {
	$curl_handle=curl_init();
	curl_setopt($curl_handle,CURLOPT_URL,'http://aulasmart.iberform.com/index.php?c=script_matricula_exterior&m=anadirProfesorTarifaAse');
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $post);
	$buffer = curl_exec($curl_handle);
	curl_close($curl_handle);
	if (!empty($buffer)){
		return $buffer;
	}else{
		return false;
	}
}


// BORRAR PROFESORES DE TARIFA PLANA
function borrarProfesorTarifaAse($get) {
	$curl_handle=curl_init();
	curl_setopt($curl_handle,CURLOPT_URL,'http://aulasmart.iberform.com/index.php?c=script_matricula_exterior&m=borrarProfesorTarifaAse');
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $get);
	$buffer = curl_exec($curl_handle);
	curl_close($curl_handle);
	if (!empty($buffer)){
		return $buffer;
	}else{
		return false;
	}
}


function listprofesorTarifaPlanaIberform($emailCentro) {

	$post['emailCentro'] = $emailCentro;

	$curl_handle=curl_init();
	curl_setopt($curl_handle,CURLOPT_URL,'http://aulasmart.iberform.com/index.php?c=script_matricula_exterior&m=tarifaPlanaAseList');
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $post);
	$buffer = curl_exec($curl_handle);
	curl_close($curl_handle);
	if (!empty($buffer)){
		return $buffer;
	}else{
		return false;
	}
}

// CHECK IF LOGGED
function isLoginCampus() {
	if (!empty($_SESSION['userCampus'])) {
		if (!empty($_SESSION['userCampus']['idalumnos'])) {
			return true;
		} else {
			return false;
		}
		return true;
	} else {
		return false;
	}
}

// GET URLS
function get_checkout_page() {
	return 'http://' . $_SERVER['HTTP_HOST'] . '/comprar';
}
function get_register_campus_page() {
	return 'http://' . $_SERVER['HTTP_HOST'] . '/registro';
}

function get_login_campus_page() {
	return 'http://' . $_SERVER['HTTP_HOST'] . '/login';
}

function get_logout_campus_page() {
	return 'http://' . $_SERVER['HTTP_HOST'] . '/salir';
}

function get_profile_campus_page() {
	return 'http://' . $_SERVER['HTTP_HOST'] . '/perfil';
}

function get_myprofile_campus_page() {
	return 'http://' . $_SERVER['HTTP_HOST'] . '/mi-perfil';
}
function get_profile_acuerdo() {
	return 'http://' . $_SERVER['HTTP_HOST'] . '/mi-acuerdo-de-colaboracion';
}

function get_edit_profile_campus_page() {
	return 'http://' . $_SERVER['HTTP_HOST'] . '/script/profile/update.php';
}

function get_avatar_campus_url() {
	$img = 'default.png';
	if (!empty($_SESSION) && !empty($_SESSION['userCampus'] && !empty($_SESSION['userCampus']['foto']))) {
		$img = $_SESSION['userCampus']['foto'];
	}
	return 'http://campusaulainteractiva.aulasmart.net/imagenes/fotos/' . $img;
}

function get_campus_photos_url($id) {
	$img = 'default.png';
	$conn = connectDBCampus();
	$sql = "SELECT * FROM alumnos WHERE idalumnos = '" . $id . "'";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		if ($user = $result->fetch_assoc()) {
			if (!empty($user['foto'])) {
				$img = $user['foto'];
			}
		}
	}
	return 'http://campusaulainteractiva.aulasmart.net/imagenes/fotos/' . $img;
}

function set_title_MECD() {
	$script .= '<script>'
		. '	var elements = document.getElementsByClassName("title-MECD");'
		. '	if(elements){'
		. '		var element = elements[elements.length-1].children[0];'
		. '		element.setAttribute("title", "'.__('HomologadoMECD', 'custom-translation').'");'
		. '	}'
		. '</script>';
	return $script;
}

function set_title_URJC() {
	$script .= '<script>'
		. '	var elements = document.getElementsByClassName("title-URJC");'
		. '	if(elements){'
		. '		var element = elements[elements.length-1].children[0];'
		. '		element.setAttribute("title", "'.__('Creditos', 'custom-translation').'");'
		. '	}'
		. '</script>';
	return $script;
}

// CONSOLE.LOG PHP
function logConsole($data = NULL) {
	$isevaled = false;
	$data = json_encode($data);

	# sanitalize
	$data = $data ? $data : '';
	$search_array = array("#'#", '#""#', "#''#", "#\n#", "#\r\n#");
	$replace_array = array('"', '', '', '\\n', '\\n');
	$data = preg_replace($search_array, $replace_array, $data);
	$data = ltrim(rtrim($data, '"'), '"');
	$data = $isevaled ? $data : ($data[0] === "'") ? $data : "'" . $data . "'";

	$js = <<<JSCODE
\n<script>
 // fallback - to deal with IE (or browsers that don't have console)
 if (! window.console) console = {};
 console.log = console.log || function(name, data){};
 // end of fallback

 console.log($data);
</script>
JSCODE;
	echo $js;
}
// SORT ARRAYS DATES FUNCTIONS
function sortFunction($a, $b) {
	return strtotime($a) - strtotime($b);
}
function sortFunctionDate($a, $b) {
	return strtotime($a['date']) - strtotime($b['date']);
}
function sortByDate($array) {
	usort($array, "sortByDateFunction");
	return $array;
}
function sortByDateFunction($a, $b) {
	return strtotime($a[1]) - strtotime($b[1]);
}
// GET NEXT DATE OF COURSE
function get_next_date_course($id) {
	$dates = array();
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date2', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date3', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date4', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date5', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date6', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date7', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date8', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date9', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date10', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date11', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date12', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date13', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date14', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date15', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date16', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date17', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date18', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date19', true));
	array_push($dates, get_post_meta($id, 'nd_learning_meta_box_date20', true));

	// Unset empty fields in array
	$dates = array_filter($dates);
	usort($dates, "sortFunction");
	
	if(count($dates)>0){
		return $dates[0];
	}else{
		return;
	}
}



function get_dates_course() {
	$id= get_the_ID();
	$dates = array();
	$today = date("d-m-Y"); 
	$daysBonification = 10;
	$difference = $daysBonification*24*60*60;

	$date1 = get_post_meta($id, 'nd_learning_meta_box_date', true);
	if(strtotime($date1)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check', true);
		$array = array('date'=>$date1, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date2 = get_post_meta($id,  'nd_learning_meta_box_date2', true);
	if(strtotime($date2)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check2', true);
		$array = array('date'=>$date2, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date3 = get_post_meta($id,  'nd_learning_meta_box_date3', true);
	if(strtotime($date3)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check3', true);
		$array = array('date'=>$date3, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date4 = get_post_meta($id,  'nd_learning_meta_box_date4', true);
	if(strtotime($date4)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check4', true);
		$array = array('date'=>$date4, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date5 = get_post_meta($id,  'nd_learning_meta_box_date5', true);
	if(strtotime($date5)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check5', true);
		$array = array('date'=>$date5, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date6 = get_post_meta($id,  'nd_learning_meta_box_date6', true);
	if(strtotime($date6)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check6', true);
		$array = array('date'=>$date6, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date7 = get_post_meta($id,  'nd_learning_meta_box_date7', true);
	if(strtotime($date7)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check7', true);
		$array = array('date'=>$date7, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date8 = get_post_meta($id,  'nd_learning_meta_box_date8', true);
	if(strtotime($date8)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check8', true);
		$array = array('date'=>$date8, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date9 = get_post_meta($id,  'nd_learning_meta_box_date9', true);
	if(strtotime($date9)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check9', true);
		$array = array('date'=>$date9, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date10 = get_post_meta($id,  'nd_learning_meta_box_date10', true);
	if(strtotime($date10)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check10', true);
		$array = array('date'=>$date10, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date11 = get_post_meta($id,  'nd_learning_meta_box_date11', true);
	if(strtotime($date11)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check11', true);
		$array = array('date'=>$date11, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date12 = get_post_meta($id,  'nd_learning_meta_box_date12', true);
	if(strtotime($date12)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check12', true);
		$array = array('date'=>$date12, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date13 = get_post_meta($id,  'nd_learning_meta_box_date13', true);
	if(strtotime($date13)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check13', true);
		$array = array('date'=>$date13, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date14 = get_post_meta($id,  'nd_learning_meta_box_date14', true);
	if(strtotime($date14)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check14', true);
		$array = array('date'=>$date14, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date15 = get_post_meta($id,  'nd_learning_meta_box_date15', true);
	if(strtotime($date15)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check15', true);
		$array = array('date'=>$date15, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date16 = get_post_meta($id,  'nd_learning_meta_box_date16', true);
	if(strtotime($date16)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check16', true);
		$array = array('date'=>$date16, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date17 = get_post_meta($id,  'nd_learning_meta_box_date17', true);
	if(strtotime($date17)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check17', true);
		$array = array('date'=>$date17, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date18 = get_post_meta($id,  'nd_learning_meta_box_date18', true);
	if(strtotime($date18)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check18', true);
		$array = array('date'=>$date18, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date19 = get_post_meta($id,  'nd_learning_meta_box_date19', true);
	if(strtotime($date19)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check19', true);
		$array = array('date'=>$date19, 'homologable' => $check);
		array_push($dates, $array);
	}
	$date20 = get_post_meta($id,  'nd_learning_meta_box_date20', true);
	if(strtotime($date20)>(strtotime($today)+$difference)){
		$check =  get_post_meta($id,  'nd_learning_meta_box_date_check20', true);
		$array = array('date'=>$date20, 'homologable' => $check);
		array_push($dates, $array);
	}
	
	// Unset empty fields in array
	$dates = array_filter($dates);
	usort($dates, "sortFunctionDate");

	return $dates;
}

// ENCRYPT - DECRYPT
$key = 'ABCDEFGHIJKLM';

function encrypt($string) {
	$iv = mcrypt_create_iv(
		mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM
	);
	$encrypted = base64_encode(
		$iv .
		mcrypt_encrypt(
			MCRYPT_RIJNDAEL_128, hash('sha256', $key, true), $string, MCRYPT_MODE_CBC, $iv
		)
	);
	return $encrypted;
}

function decrypt($string) {
	$data = base64_decode($string);
	$iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

	$decrypted = rtrim(
		mcrypt_decrypt(
			MCRYPT_RIJNDAEL_128, hash('sha256', $key, true), substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)), MCRYPT_MODE_CBC, $iv
		), "\0"
	);
	return $decrypted;
}

// SEND MAIL
function sendMail($data) {
	if (empty($data['headers'])) {
		$data['headers'] = array('From: AULA_SMART Editorial <info@aulasmarteditorial.com>');
	}
	//Send the email
	add_filter('wp_mail_content_type', create_function('', 'return "text/html"; '));
	remove_filter('wp_mail_content_type', 'set_html_content_type');

	wp_mail($data['email'], $data['subject'], $data['message'], $data['headers'], $data['attachments']);
}

function wp_hide_update() {
	global $current_user;
	get_currentuserinfo();

//        if ($current_user->ID != 1) { // solo el admin lo ve, cambia el ID de usuario si no es el 1 o añade todso los IDs de admin
	remove_action('admin_notices', 'update_nag', 3);
//        }
}

add_action('admin_menu', 'wp_hide_update');

function deleteStringItinerario($string) {
	$string = str_replace("Itinerario Formativo de ", "", $string);
	$string = str_replace("Itinerario Formativo en ", "", $string);
	// Comprobar otros idiomas
	return $string;
}


// Load custom translation
function load_custom_translation(){
	load_plugin_textdomain("custom-translation", false, 'custom-translation/languages');
}
load_custom_translation();



function create_popup($src, $id){
	wp_enqueue_script('jquery');

	$script = '';
	$script .= '<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>';
	$script .= '<script type="text/javascript" src="http://aulasmarteditorial.com/wp-content/plugins/nd-shortcodes/shortcodes/custom/magic-popup/js/jquery.magnific-popup.min.js?ver=4.7.3"></script>';
	$script .= '
		<script>
			$("[ data-custom-popup='.$id.']").magnificPopup({
				items: {
					src: "'.$src.'",
				},
				type: "iframe",
				iframe: {
					markup: "<div class=\'mfp-iframe-scaler\'>" +
						"<div class=\'mfp-close\'></div>" +
						"<iframe class=\'mfp-iframe\' frameborder=\'0\' allowfullscreen></iframe>" +
						"</div>",
					patterns: {
						youtube: {
							index: "youtube.com/",
							id: "v=",
							src: "//www.youtube.com/embed/%id%?autoplay=1&rel=0&cc_load_policy=1"
						}
					},
					srcAction: "iframe_src",
				}
			});
		</script>';
	
	return $script;
}

function get_domain($url){
	$url = str_replace("www.", "", $url);
   	$parsedUrl = parse_url($url);
   	$path = $parsedUrl['path'];
   	$countPoints = substr_count($path, '.');
	$host = explode('.', $path);
   	if($countPoints == 1){
		$result= $host[count($host)-1];
   	}elseif($countPoints > 1){
		$subdomains = array_slice($host, 0, count($host) - 2 );
		$result= $subdomains[0];
   	}
	return $result;
}

function get_current_country($url){
	$extension = get_domain($url);
	$countries=array(
		'com' => array('country'=>'España', 'flag'=>'es-es', 'money'=>'EUR'),
		'org' => array('country'=>'España', 'flag'=>'es-es', 'money'=>'EUR'),
		'net' => array('country'=>'España', 'flag'=>'es-es', 'money'=>'EUR'),
		'es' => array('country'=>'España', 'flag'=>'es-es', 'money'=>'EUR'),
		'ar' => array('country'=>'Argentina', 'flag'=>'es-ar', 'money'=>'ARS'),
		'cl' => array('country'=>'Chile', 'flag'=>'es-ch', 'money'=>'CLP'),
		'co' => array('country'=>'Colombia', 'flag'=>'es-co', 'money'=>'COP'),
		'cr' => array('country'=>'Costa Rica', 'flag'=>'es-cr', 'money'=>'CRC'),
		'rd' => array('country'=>'República Dominicana', 'flag'=>'es-do', 'money'=>'DOP'),
		'hn' => array('country'=>'Honduras', 'flag'=>'es-ho', 'money'=>'HNL'),
		'mx' => array('country'=>'México', 'flag'=>'es-mx', 'money'=>'MXN'),
		'ni' => array('country'=>'Nicaragua', 'flag'=>'es-ni', 'money'=>'NIO'),
		'pa' => array('country'=>'Panamá', 'flag'=>'es-pa', 'money'=>'PAB'),
		'pe' => array('country'=>'Perú', 'flag'=>'es-pe', 'money'=>'PEN'),
		'py' => array('country'=>'Paraguay', 'flag'=>'es-pr', 'money'=>'PYG'),
		'uy' => array('country'=>'Uruguay', 'flag'=>'es-ur', 'money'=>'UYU'),
		've' => array('country'=>'Venezuela', 'flag'=>'es-ve', 'money'=>'VEF'),
	);
	return $countries[$extension];
}

function isFinishedCourse($user){
	$conn = connectDBCampus();
	$sql = "SELECT * FROM acuerdo_colaboracion_ase WHERE id_alumno = '" . $user['idalumnos'] . "' LIMIT 1";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		if ($acuerdo = $result->fetch_assoc()) {
			if($acuerdo['pendiente']==0){
				$now = date("Y-m-d");
				$date_course=fechaReal($acuerdo['fecha_fin']);
				if(!empty($acuerdo['fecha_fin']) && $now>$date_course){
					$sql = "UPDATE acuerdo_colaboracion_ase SET estado_clave='0', idcurso_matriculado='', fecha_matriculado='', fecha_fin=''"
						. " WHERE id_alumno='" . $user['idalumnos'] . "'";
					$conn->query($sql);
//					return 'tiempo finalizado';
				}else{
//					return 'tiempo por delante';
				}
			}
		}
	}
}
						
function fechaReal($fecha){
	$fItem = explode("/",$fecha);
	if(count($fItem)>1){
		$fechaItem = date($fItem[1] . "/" . $fItem[0] . "/" . explode(" ",$fItem[2])[0]);
		$dateReal = date('Y-m-d', strtotime($fechaItem));
		return $dateReal;
	}
	return;
}

function historialCursos($idAlumno){
	$conn = connectDBCampus();

	$sql = "SELECT modulo.id_ase "
		. "FROM matricula "
		. "LEFT JOIN temario ON temario.idcurso = matricula.idcurso "
		. "LEFT JOIN modulo ON temario.idmodulo = modulo.idmodulo "
		. "LEFT JOIN curso ON curso.idcurso = temario.idcurso "
		. "WHERE matricula.idalumnos = " . $idAlumno ." "
		. "AND matricula.borrado='0' "
		. "AND curso.f_fin<'".date("Y-m-d")."'";

	return $result = $conn->query($sql);
}



//Eliminar párrafos automáticos por defecto
remove_filter('the_content', 'wpautop');
remove_filter( 'the_excerpt', 'wpautop' );


