<?php
/*
  Template Name: Perfil
 */

// CUSTOM WORDPRESS
if (empty($_SESSION))
	session_start();


function redirect($path = '') {
	$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $path;
	echo "<script type='text/javascript'>window.location='" . $url . "'</script>";
	exit();
}



if (!isLoginCampus()) {
	redirect('login');
} else {
	// LOGUEADO
	include 'page.php';
}

