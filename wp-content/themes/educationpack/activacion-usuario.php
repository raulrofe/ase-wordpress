<?php
// CUSTOM WORDPRESS
if (empty($_SESSION))
	session_start();
?>
<?php
// CUSTOM WORDPRESS
/*
  Template Name: Activacion Usuario
 */
function redirect($path = '') {
	$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $path;
	echo "<script type='text/javascript'>window.location='" . $url . "'</script>";
	exit();
}

function notification($status, $msg) {
	$_SESSION['notification']['msg'] = $msg;
	$_SESSION['notification']['status'] = $status;
}

function sendMailRegister($name, $email, $token) {
	$link = 'http://' . $_SERVER[HTTP_HOST] . '/activacion-usuario/?token=' . $token;
	ob_start();
	include 'wp-content/themes/educationpack/mail-activation.php';
	$message = ob_get_clean();
	$data = array(
				'email' => $email,
				'subject' => __('RegistroASE', 'custom-translation'),
				'message' => $message,
		);
	sendMail($data);
}

function str_random($length = 16){
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
}

function getToken() {
	return hash_hmac('sha256', str_random(40), 'AULA_SMART Editorial');
}
?>

<?php
if(!empty($_GET) && !empty($_GET['token'])){
	$conn = connectDBCampus();
	$sql = "SELECT * FROM activation_user WHERE token = '" . $_GET['token'] . "'";

	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		if ($register = $result->fetch_assoc()) {
			$time_limit = 1; //days
			$time = $time_limit * 24 * 60 * 60;
			
			// Si la fecha mas el tiempo de caducidad es mayor que ahora, estamos dentro del plazo para usar el token
			if((strtotime($register['date']) + $time) > time() ) {
				$id = $register['id'];
				$iduser = $register['iduser'];
				$sql = "UPDATE alumnos SET activated='1'";
				if ($conn->query($sql) === TRUE) { // Si se ha actualizado correctamente el usuario y queda activado
					// Borramos el token ya usado para activar
					$sql = "DELETE FROM activation_user WHERE id='" . $id . "' AND iduser='" . $iduser . "'";
					if ($conn->query($sql) === TRUE) { // Si se ha borrado correctamente
						notification('success',__('SiUsuario', 'custom-translation'));

						// AUTOLOGIN
						$sql = "SELECT * FROM alumnos WHERE idalumnos = '" . $iduser . "'";
						$result = $conn->query($sql);
						if ($user = $result->fetch_assoc()) {
							$_SESSION['userCampus'] = $user;
						}
					}
				}
				
				
			} else {
				// Si ha caducado el token, generamos un nuevo token y un nuevo correo al usuario
				// Generamos un token de activacion y lo guardamos
				$iduser = $register['iduser'];
				$token = getToken();
				$sql = "UPDATE activation_user SET token='". $token . "' WHERE iduser='" . $iduser . "'";
				if ($conn->query($sql) === TRUE) { // Si se ha actualizado bien el token
					
					$sql = "SELECT * FROM alumnos WHERE idalumnos = '" . $iduser . "'";

					$result = $conn->query($sql);
					if ($result->num_rows > 0) {
						if ($user = $result->fetch_assoc()) {
							sendMailRegister( $user['nombre'], $user['email'], $token);
							notification('success',__('SiActivacion', 'custom-translation'));
						}
					}
				}
			}
		}
	}else{
		notification('error', __('NoActivacion', 'custom-translation'));
	}
	redirect('');
}else{
   redirect('');
}
