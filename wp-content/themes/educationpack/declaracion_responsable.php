<div id="declaracionResponsable" style="display: none;">
	<h3 style="text-align:justify;">ANEXO I</h3>

	<h3 style="text-align:justify;">DECLARACIÓN JURADA/PROMESA</h3></br>

	<p style="text-align:justify;line-height: 25px">D. / Dña._______________________________, con D.N.I__________________ en calidad de representante legal de la empresa______________________________________________., con CIF_________________ y domicilio en ____________________________________________</p><br/>

	<p style="text-align:justify;line-height: 25px">Declara bajo juramento o promete a efectos de gestionar la bonificación de la formación de los trabajadores de la citada empresa, según el Real Decreto Ley 30/2015, de 9 de septiembre, por la que se regula el Sistema de Formación Profesional para el empleo en el ámbito laboral, ante la Fundación Estatal para la Formación en el Empleo, que los trabajadores de la citada empresa son los siguientes:</p><br/>

	<p style="text-align:justify;line-height: 25px">Régimen General de la Seguridad Social (indicar nombre y DNI de los trabajadores en este régimen)</p><br/>

	<?php foreach($noAutonomos as $profesor): ?>
		<p><?php echo $profesor->dni . ' - ' . $profesor->nombre . ' ' . $profesor->apellido1 . ' ' . $profesor->apellido2; ?></p>
	<?php endforeach; ?>

	<p style="text-align:justify;line-height: 25px">Régimen Especial de Trabajadores Autónomos (indicar nombre y DNI de los trabajadores en este régimen)</p><br/>

	<?php foreach($autonomos as $profesor): ?>
		<p><?php echo $profesor->dni . ' - ' . $profesor->nombre . ' ' . $profesor->apellido1 . ' ' . $profesor->apellido2; ?></p>
	<?php endforeach; ?>

	<p style="text-align:justify;line-height: 25px">Además, se compromete a mantener este documento actualizado e informar de cualquier cambio que pudiera haber durante la duración del contrato con AULA SMART Editorial</p><br/> 

	<p style="text-align:justify;line-height: 25px">En _____________, a _____ de ____________ del _________</p></br></br></br><br/>

	<p style="text-align:justify;line-height: 25px">Fdo:____________________________________</p>
</div>

<div id="editor"></div>

<div class="nd_learning_section nd_learning_height_20"></div>

<div class="  nd_options_padding_20">
	<input id="cmd" type="submit" class="wpcf7-form-control wpcf7-submit" value="Generar declaración responsable" style="background-color: #0093C9;">
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>

<script type="text/javascript">
	var doc = new jsPDF();
	var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
	};

	$('#cmd').click(function () {
		doc.fromHTML($('#declaracionResponsable').html(), 15, 15, {
			'width': 170,
			'elementHandlers': specialElementHandlers
		});
		doc.save('declaracion-responsable.pdf');
	});
</script>