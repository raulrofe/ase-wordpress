<?php
// CUSTOM WORDPRESS

/*
  Template Name: MiPerfil
 */
if (empty($_SESSION))
	session_start();

if (!function_exists('redirect')) {
	function redirect($path = '') {
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $path;
		echo "<script type='text/javascript'>window.location='" . $url . "'</script>";
		exit();
	}
}

if (!isLoginCampus()) {
	redirect('login');
} else {
	
?>
<?php get_header();
?>

<?php 
$nd_options_meta_box_page_header_img = get_post_meta( get_the_ID(), 'nd_options_meta_box_page_header_img', true );
$nd_options_meta_box_page_header_img_title = get_post_meta( get_the_ID(), 'nd_options_meta_box_page_header_img_title', true );
$nd_options_meta_box_page_header_img_position = get_post_meta( get_the_ID(), 'nd_options_meta_box_page_header_img_position', true );
?>
<!--start section-->
	<div class="nd_options_section nd_options_background_size_cover <?php echo $nd_options_meta_box_page_header_img_position ?>" style="background-image:url(<?php echo $nd_options_meta_box_page_header_img; ?>);">
        <div class="nd_options_section nd_options_bg_greydark_alpha_gradient_2">
            <!--start nd_options_container-->
            <div class="nd_options_container nd_options_clearfix">
                <div class="nd_options_section nd_options_height_200"></div>
                <div class="nd_options_section nd_options_padding_15 nd_options_box_sizing_border_box">
                    <strong class="nd_options_color_white nd_options_font_size_60 nd_options_font_size_40_all_iphone nd_options_line_height_40_all_iphone nd_options_first_font"><?php echo $nd_options_meta_box_page_header_img_title; ?></strong>
                    <div class="nd_options_section nd_options_height_20"></div>
                </div>
            </div>
            <!--end container-->
        </div>
    </div>
<!--end section-->

<!--Breadcrumbs-->
    <?php do_action('nd_options_end_header_img_page_hook'); ?>
<!--Breadcrumbs-->

<div class="nicdark_section nicdark_height_50"></div>


<!--start nicdark_container-->
<div class="nicdark_container nicdark_clearfix">


	<!--start all posts previews-->

	<div class="nicdark_grid_12">

		<?php
		include 'miperfil-content.php';
		?>

	</div>





</div>
<!--end container-->


<div class="nicdark_section nicdark_height_60"></div>

<?php get_footer(); ?>
<?php } ?>