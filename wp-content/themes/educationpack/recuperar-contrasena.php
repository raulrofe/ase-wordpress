<?php
/*
  Template Name: Recuperar-contrasena
 */
?>


<?php
if (empty($_SESSION))
	session_start();

get_header();


if (!function_exists('notification')) {
	function notification($status, $msg) {
		$_SESSION['notification']['msg'] = $msg;
		$_SESSION['notification']['status'] = $status;
	}
}

if (!function_exists('redirect')) {
	function redirect($path = '') {
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $path;
		echo "<script type='text/javascript'>window.location='" . $url . "'</script>";
		exit();
	}
}

if (!function_exists('cadenaAleatoria')) {
	function cadenaAleatoria($longitud = 10){
		$caracteres =  '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$string = substr(str_shuffle($caracteres), 0, $longitud); 
		return $string;
	}
}

if (!function_exists('sendMailAcuerdo')) {
	function sendMailAcuerdo($asunto, $name, $emails, $linkPlantilla, $dataMail, $adjuntos = array()) {
		ob_start();
		include $linkPlantilla;
		$message = ob_get_clean();
		foreach($emails as $email){
			$data = array(
						'email' 		=> $email,
						'subject' 		=> $asunto,
						'message' 		=> $message,
						'attachments'	=> $adjuntos	
				);
			sendMail($data);
		}
	}
}
?>




<?php
// Si tiene parametros POST venimos desde el propio formulario
if (!empty($_POST)) {

	if (!empty($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		$conn = connectDBCampus();
		$sql = "SELECT * FROM alumnos WHERE email = '".$_POST['email']."'";
		$resultado = $conn->query($sql);
		

		if ($resultado->num_rows == 1) {
			$alumno = $resultado->fetch_object();
			$token = hash_hmac('sha256', cadenaAleatoria(40), $_POST['email']);
			
			$sql = "INSERT INTO recordar_contrasena(idalumnos, token) VALUES (".$alumno->idalumnos.",'".$token."')";
			$resultado = $conn->query($sql);
			$dataMail = array('id' => $alumno->idalumnos, 'token' => $token);
			
			sendMailAcuerdo('Recuperación contraseña AULA_SMART Editorial', 
								$alumno->nombre, 
								[$alumno->email], 
								'wp-content/themes/educationpack/mail-recordar-contrasena.php',
								$dataMail);	
			
			notification('success',  __('RecuperarContrasenaOK', 'custom-translation'));
			redirect('login');
		} else {
			notification('error', __('NoUsuarioEmail', 'custom-translation'));
		}
	} else {
		notification('error', __('IntroduceEmailValido', 'custom-translation'));
	}
	redirect('recuperar-contrasena');
}
// Si tiene parametros GET venimos desde el mail de recuperacion enviado al usuario
elseif (!empty($_GET)) {
	
	if (!empty($_GET['id']) && !empty($_GET['token'])) {
		$conn = connectDBCampus();
		$sql = "SELECT * FROM alumnos WHERE idalumnos = '".$_GET['id']."'";
		$resultado = $conn->query($sql);

		if ($resultado->num_rows == 1) {
			$alumno = $resultado->fetch_object();
			
			// Traemos el ultimo registro en "recordar_contrasena" para ese usuario
			$sql = "SELECT * FROM recordar_contrasena WHERE idalumnos = '".$_GET['id']."' ORDER BY id DESC LIMIT 1";
			$resultado = $conn->query($sql);
			if ($resultado->num_rows > 0) {
				$registro = $resultado->fetch_object();
				// Si el token coincide con el ultimo registro encontrado para ese usuario
				if($registro->token == $_GET['token']){
					// Actualizamos contraseña
					$nuevaContrasena = cadenaAleatoria(10);
					$sql = "UPDATE alumnos SET pass='". md5($nuevaContrasena)."' WHERE idalumnos = ".$_GET['id'];
					$resultado = $conn->query($sql);
					// Borramos los registros de ese usuario en la tabla "recordar_contrasena"
					$sql = "DELETE FROM recordar_contrasena WHERE idalumnos = ".$_GET['id'];
					$conn->query($sql);
					// Enviamos un correo al usuario
					$dataMail = array('nuevaContrasena' => $nuevaContrasena);

					sendMailAcuerdo('Nueva contraseña AULA_SMART Editorial', 
							$alumno->nombre, 
							[$alumno->email], 
							'wp-content/themes/educationpack/mail-nueva-contrasena.php',
							$dataMail);	

					notification('success',  __('ContrasenaNuevaCorreo', 'custom-translation'));
					redirect('login');
				}else{
					notification('error', __('TokenNoValido', 'custom-translation'));
				}
			}else{
				notification('error',  __('NoRecuperacionContrasena', 'custom-translation'));
			}
		}else{
			notification('error',  __('UsuarioNoExiste', 'custom-translation'));
		}
		redirect('recuperar-contrasena');
	}
}
?>








<?php
$nd_options_meta_box_page_header_img = get_post_meta(get_the_ID(), 'nd_options_meta_box_page_header_img', true);
$nd_options_meta_box_page_header_img_title = get_post_meta(get_the_ID(), 'nd_options_meta_box_page_header_img_title', true);
$nd_options_meta_box_page_header_img_position = get_post_meta(get_the_ID(), 'nd_options_meta_box_page_header_img_position', true);
?>
<!--start section-->
<div class="nd_options_section nd_options_background_size_cover <?php echo $nd_options_meta_box_page_header_img_position ?>" style="background-image:url(<?php echo $nd_options_meta_box_page_header_img; ?>);">
	<div class="nd_options_section nd_options_bg_greydark_alpha_gradient_2">
		<!--start nd_options_container-->
		<div class="nd_options_container nd_options_clearfix">
			<div class="nd_options_section nd_options_height_200"></div>
			<div class="nd_options_section nd_options_padding_15 nd_options_box_sizing_border_box">
				<strong class="nd_options_color_white nd_options_font_size_60 nd_options_font_size_40_all_iphone nd_options_line_height_40_all_iphone nd_options_first_font"><?php echo $nd_options_meta_box_page_header_img_title; ?></strong>
				<div class="nd_options_section nd_options_height_20"></div>
			</div>
		</div>
		<!--end container-->
	</div>
</div>
<!--end section-->

<!--Breadcrumbs-->
<?php do_action('nd_options_end_header_img_page_hook'); ?>
<!--Breadcrumbs-->


<!--page margin-->
<div class="nd_options_section nd_options_height_50"></div>

<?php
if (get_post_meta(get_the_ID(), 'nd_options_meta_box_page_margin', true) != 1) {
	echo '<div class="nd_options_section nd_options_height_50"></div>';
}
?>

<!--start nd_options_container-->
<div class="nd_options_section  nd_options_padding_0_15 nd_options_box_sizing_border_box nd_options_clearfix">

	<div class="nd_learning_section">
		<div class="nd_learning_width_50_percentage nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive" style="margin: 0px auto;">

			<div class="nd_learning_section nd_learning_border_radius_3 nd_learning_border_1_solid_grey nd_learning_padding_20 nd_learning_box_sizing_border_box">

				<h3 class="nd_options_second_font nd_learning_bg_green nd_learning_padding_5 nd_learning_border_radius_3 nd_learning_color_white_important nd_learning_display_inline_block">
					<?php echo __('RecuperarContrasena', 'custom-translation') ?>
				</h3>
				<div class="nd_learning_section nd_learning_height_5"></div>


				<form action="/recuperar-contrasena" method="post" data-recuperar-contrasena="1" name="form-recuperar-contrasena" id="form-recuperar-contrasena">
					<p>
						<br>
						<?php echo __('RecuperarContrasenaInfo', 'custom-translation') ?>
					</p>
					<p>
						<label class="nd_learning_section nd_learning_margin_top_20"><?php echo __('Correo', 'custom-translation') ?></label>
						<input type="text" name="email" class=" nd_learning_section" value="">
					</p>

					<p class="login-submit">
						<input type="submit" name="wp-submit" id="nd_learning_login_form_submit" class="button button-primary nd_learning_margin_top_20" value="<?php echo __('Enviar', 'custom-translation') ?>">
					</p>
				</form>
			</div>
		</div>


	</div>
</div>
<!--end container-->


<div class="nicdark_section nicdark_height_60"></div> 

<div class="nd_options_section nd_options_height_50"></div>
<?php get_footer(); ?>