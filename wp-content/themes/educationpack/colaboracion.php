<?php

// CUSTOM WORDPRESS
if (empty($_SESSION))
	session_start();

ignore_user_abort(true);
set_time_limit(0);
?>


<?php
// MODELO
function comparekeyUser($id, $key) {
	$conn = connectDBCampus();
	$sql = "SELECT * FROM acuerdo_colaboracion_ase WHERE id_alumno = '" . $id . "' AND clave = '" . $key . "' AND anio = '". date('Y')."'";

	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		if ($user = $result->fetch_assoc()) {
			return $user;
		}
	}
	return false;

}

function updateStatusKey($id, $idCurso, $fechaCurso, $fechaFin) {
	$conn = connectDBCampus();
	$sql = "UPDATE acuerdo_colaboracion_ase SET estado_clave = 1, pendiente = 1, idcurso_matriculado = " . $idCurso . ", fecha_matriculado = '" . $fechaCurso . "', fecha_fin = '" . $fechaFin . "' WHERE id_alumno = '" . $id . "'";
	if ($conn->query($sql)) {
		return true;
	}
	return false;
}
function cleanStatusKey($id) {
	$conn = connectDBCampus();
	$sql = "UPDATE acuerdo_colaboracion_ase SET estado_clave = 0, pendiente = 0, idcurso_matriculado = NULL, fecha_matriculado = NULL, fecha_fin = NULL WHERE id_alumno = '" . $id . "'";
	if ($conn->query($sql)) {
		return true;
	}
	return false;
}

// *****************************************************************************************************************************************

//Compruebo si existe el curso en la base de datos
function checkCourseCreated($codigoAF, $codigoIG){
	$conn = connectDBCampus();
	$sql = "SELECT * FROM curso WHERE titulo LIKE '%(" . $codigoAF . "/" . $codigoIG . ")' AND borrado = 0";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		if ($course = $result->fetch_assoc()) {
			return $course;
		}
	}

	return false;	
}

// FUNCTIONS TO CAMPUS DATABASE
function createCourseCampus($post) {
	$params = http_build_query($post);
	$curl_handle=curl_init();
	curl_setopt($curl_handle, CURLOPT_URL, 'http://campusaulainteractiva.com/admin/index.php?m=cursos&c=curso_web&t=simple&' . $params);
	curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	// curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $post);
	curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, true);
	// curl_setopt($curl_handle, CURLOPT_POSTFIELDS, http_build_query($post));

	$buffer = curl_exec($curl_handle);
	curl_close($curl_handle);
	if (!empty($buffer)){
		return $buffer;
	} else {
		return false;
	}
}

function ModuloByReferenceCampus($referenciaModulo) {
	$conn = connectDBCampus();
	$sql = "SELECT * FROM modulo WHERE referencia_modulo = " . $referenciaModulo . " AND borrado = 0";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		if ($modulo = $result->fetch_assoc()) {
			return $modulo;
		}
	}
	return false;		
}

/* FUNCIONES PARA LA MATRICULACION */
function obtenerUnaMatriculaPorAlumnoCurso($idAlumno, $idCurso)
{
	$conn = connectDBCampus();
	$sql = 'SELECT m.idmatricula FROM matricula AS m WHERE m.idalumnos = ' . $idAlumno . ' AND m.idcurso = ' . $idCurso;

	$resultado = $conn->query($sql);

	return $resultado;
}

function insertarMatricula($idAlumno, $idCurso)
{
	$conn = connectDBCampus();
	$sql = 'INSERT INTO matricula (fecha, idalumnos, idcurso)' .
	' VALUES ("' . date('Y-m-d') . '", ' . $idAlumno . ', ' . $idCurso . ')';
	$resultado = $conn->query($sql);

	return $resultado;
}

function reactivarMatricula($idMatricula)
{
	$conn = connectDBCampus();
	$sql = 'UPDATE matricula SET borrado = 0 WHERE idmatricula = ' . $idMatricula;
	$resultado = $conn->query($sql);

	return $resultado;
}

function _reverseDate($fecha, $separador = '-', $separador2 = '-') {
	$f = explode($separador, $fecha);
	if (count($f) == 3) {
		$fecha = $f[2] . $separador2 . $f[1] . $separador2 . $f[0];
	} else {
		$fecha = null;
	}
	return $fecha;
}


//******************************************************************************************************************************************************

// FUNCIONES AUXILIARES
if (!function_exists('notification')) {
	function notification($status, $msg) {
		$_SESSION['notification']['msg'] = $msg;
		$_SESSION['notification']['status'] = $status;
	}
}

if (!function_exists('redirect')) {
	function redirect($path = '') {
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $path;
		echo "<script type='text/javascript'>"
				. "window.location='" . $url . "'"
			. "</script>";
		exit();
	}
}

if (!function_exists('redirectBack')) {
	function redirectBack($steps = 1) {
		echo "<script type='text/javascript'>window.history.go(-".$steps.")</script>";
		exit();
	}
}

function sendMailAcuerdo($asunto, $name, $emails, $linkPlantilla, $dataMail, $adjuntos = array()) {
	ob_start();
	include $linkPlantilla;
	$message = ob_get_clean();
	foreach($emails as $email){
		$data = array(
					'email' 		=> $email,
					'subject' 		=> $asunto,
					'message' 		=> $message,
					'attachments'	=> $adjuntos	
			);
		sendMail($data);
	}
}

function checkDateExist($id , $date){
	if(!empty($date)){
		$dates =array();

		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date2', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date3', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date4', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date5', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date6', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date7', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date8', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date9', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date10', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date11', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date12', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date13', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date14', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date15', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date16', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date17', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date18', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date19', true);
		$dates[] = get_post_meta($id, 'nd_learning_meta_box_date20', true);

		if(in_array($date, $dates)){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function checkDateEndExist($id , $date){
	$dates =array();

	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date2', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date3', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date4', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date5', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date6', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date7', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date8', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date9', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date10', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date11', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date12', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date13', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date14', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date15', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date16', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date17', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date18', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date19', true);
	$dates[] = get_post_meta($id, 'nd_learning_meta_box_date20', true);

	$key = array_search($date, $dates);
	if($key==0){
		return get_post_meta($id, 'nd_learning_meta_box_date_end', true);
	}else if($key>0){
		return get_post_meta($id, 'nd_learning_meta_box_date_end'.($key+1), true);
	}else{
		return false;
	}
}

function quitarPlaza($id, $name){
	$plazas =  get_post_meta($id, 'nd_learning_meta_box_max_availability', true);
	if($plazas > 0){
		$plazas--;
		update_post_meta($id, 'nd_learning_meta_box_max_availability', wp_kses($plazas, $allowed));
		$plazas =  get_post_meta($id, 'nd_learning_meta_box_max_availability', true); // Por precaución con la concurrencia
		if($plazas==0){
			$dataMail= array(
								'nombreCurso' 		=> $name,
							);
			sendMailAcuerdo('Curso completo en ASE', 
							'Curso completo en ASE', 
							['gabrielvalero@ibericasur.com'], 
							'wp-content/themes/educationpack/mail-matriculacion-completa.php',
							$dataMail);
		}
	}else{
		notification('error', __('NoCursoCompleto', 'custom-translation'));
		redirectBack();
	}
}

?>




<?php

// CUSTOM WORDPRESS

/*
  Template Name: Colaboracion
 */

if (isLoginCampus()) { // Si no hay usuario logueado

	if (isset($_POST['checkSubmitForm']) && $_POST['checkSubmitForm'] == 1){		
		if($_POST['terminos'] == on){
			if($_POST['acuerdoColaboracion'] == 0) {
				//Cuando se envia el formulario de NO ACUERDO DE COLABORACION
				if(!empty($_POST['telefono'])){
					// Rellenamos los datos para el envio del email
					$dataMail = array(
						'emailUsuario' 		=> $_SESSION['userCampus']['email'],
						'telefonoUsuario' 	=> $_POST['telefono']
					);
					// Enviar mail a Administracion
					sendMailAcuerdo("Solicitud acuerdo de colaboración AULA_SMART Editorial",
									$_SESSION['userCampus']['nombre'], 
									['info@aulasmarteditorial.com','administracion@fundacionaulasmart.org','coordinacion@fundacionaulasmart.org'],
									'wp-content/themes/educationpack/mail-no-acuerdo.php', 
									$dataMail);
					
					notification('success', __('SiAcuerdoColaboracion', 'custom-translation'));
				} else {
					notification('error', __('NoAcuerdoColaboracion', 'custom-translation'));
				}
				
				
			} else {
				// Compruebo si la clave de matriculacion es correcta
				$keyUser = comparekeyUser($_SESSION['userCampus']['idalumnos'], $_POST['key']);
				
				
				//Si la clave de matriculacion es correcta
				if(!empty($keyUser)){
					
					// Si la clave NO esta ya en uso
					if($keyUser['estado_clave'] == 0){ 
						
						// Si la fecha de inicio existe entre las posibles para ese curso
						if(checkDateExist($_POST['idCurso'] , $_POST['fechaCurso'])){
							
							// Rellenamos la fecha de fin del curso
							$_POST['fechaCursoFin'] = checkDateEndExist($_POST['idCurso'] , $_POST['fechaCurso']);

							// Si existe fecha de fin para la fecha de inicio que hemos escogido
							if(!empty($_POST['fechaCursoFin'])){
								
								// Pongo el estado de la clave a 1 para que aparezca como usada
								if(updateStatusKey($_SESSION['userCampus']['idalumnos'], $_POST['idCurso'], $_POST['fechaCurso'], $_POST['fechaCursoFin'])){

									// Devuelve array con los datos del grupo creado en Iberform
									$respuestaCURL =  matricularIberform($_POST);
									$matricula = json_decode($respuestaCURL, true);

									// Si la creacion de Grupos en Iberform se ha realizado correctamente
									if(!empty($matricula['statusGrupo']) && $matricula['statusGrupo'] == 'OK'){
										
										// Si el curso ya estaba creado
										if($course = checkCourseCreated($matricula['codigoAF'], $matricula['codigoIG'])){
											
											// Obtengo el ID del curso
											$idCourse = $course['idcurso'];

											// Matriculo al participante
											$idUsuarioCampus = $_SESSION['userCampus']['idalumnos'];

											//Matricula al participante en el curso
											if(!empty($idCourse) && !empty($idUsuarioCampus)){
												$resultMatricula = obtenerUnaMatriculaPorAlumnoCurso($idUsuarioCampus, $idCourse);
												if($resultMatricula->num_rows == 0) {
													insertarMatricula($idUsuarioCampus, $idCourse);
												} else {
													$rowMatricula = $resultMatricula->fetch_object();
													reactivarMatricula($rowMatricula->idmatricula);
												}
											}

											// Status OK para seguir con el proceso
											$curso['statusCurso']='OK';
											
										} else {
											// Si el curso no estaba creado, hay que crearlo en el Campus
											$modulo = ModuloByReferenceCampus($matricula['referenciaModulo']);
											
											//Informacion del profesor
											$teacher = get_post_meta($_POST['idCurso'], 'nd_learning_meta_box_teacher', true);
											$nd_learning_teacher_email = get_post_meta( $teacher, 'nd_learning_meta_box_teacher_email_campus', true );

											//Creo el curso
											$arrayPost = array(
												'f_inicio' 			=> $matricula['fechaInico'],
												'f_fin' 			=> $matricula['fechaFin'],
												'titulo'			=> $matricula['titulo'],
												'idModulo'			=> $modulo['idmodulo'],
												'i_maticulacion'	=> NULL,
												'f_matriculacion'	=> NULL,
												'n_horas'			=> $matricula['numeroHoras'],
												'metodologia'		=> 'Teleformación',
												'descripcion'		=> '',
												'caf'				=> $matricula['codigoAF'], 
												'cg'				=> $matricula['codigoIG'],
												'emailCampus'		=> $nd_learning_teacher_email,
												'idAlumnoCampus'	=> $_SESSION['userCampus']['idalumnos'],
											);

											// 1. Creo el curso en el campus, con sus contenidos, tutores y configuracion de notas
											// 2. Matriculo al participante
											$respuestaCURL = createCourseCampus($arrayPost);
											$curso = json_decode($respuestaCURL, true);
										}

										// Si la creacion de Cursos en el Campus se ha realizado correctamente
										if(!empty($curso['statusCurso']) && $curso['statusCurso']=='OK'){
											//Mensaje bienvenida Administrador
											$dataMail = array(
												'emailUsuario'			=> $_SESSION['userCampus']['email'],
												'cursoUsuario'			=> $matricula['titulo'],
												'fechaCursoUsuario'		=> _reverseDate($matricula['fechaInico']),
												'fechaCursoFinUsuario'	=> _reverseDate($matricula['fechaFin']),
												'accion_formativa'		=> $matricula['codigoAF'], 
												'codigoIG'				=> $matricula['codigoIG'],
												'esBonificado'			=> $matricula['esBonificado'],
											);
											

											// Restamos una plaza al curso en ASE
											quitarPlaza($_POST['idCurso'], $_POST['nombreCurso']);
											
											// Correos automaticos para el alta del curso para Administración
											sendMailAcuerdo('Petición formalización matriculación', 
															$_SESSION['userCampus']['nombre'], 
															['info@aulasmarteditorial.com','administracion@fundacionaulasmart.org','coordinacion@fundacionaulasmart.org'],
															'wp-content/themes/educationpack/mail-matriculacion-administracion.php',
															$dataMail);

											
											if($matricula['esBonificado']){
												// Correos automaticos para el alta del curso para el cliente
												sendMailAcuerdo('Matriculación curso AULA_SMART Editorial', 
																$_SESSION['userCampus']['nombre'], 
																[$_SESSION['userCampus']['email']], 
																'wp-content/themes/educationpack/mail-matriculacion-usuario.php',
																$dataMail,
																['wp-content/uploads/archivos/Ficha_Inscripcion_Cursos_2017.pdf']);						
											}else{
												// Correos automaticos para el alta del curso para el cliente
												sendMailAcuerdo('Matriculación curso AULA_SMART Editorial', 
																$_SESSION['userCampus']['nombre'], 
																[$_SESSION['userCampus']['email']], 
																'wp-content/themes/educationpack/mail-matriculacion-usuario.php',
																$dataMail);						
											}
											
											
											//////////////////////////////////////////////////
											// MATRICULACION CORRECTA
											//////////////////////////////////////////////////
											// Si es homologado, volcamos la aceptacion de las condiciones del MECD
											if($_POST['input_homologado']){
												$conn = connectDBCampus();
												$sql = "INSERT INTO acuerdo_colaboracion_ase_confirmacion (id_alumno, idcurso_matriculado) VALUES (".$_SESSION['userCampus']['idalumnos'].",".$_POST['idCurso'].")";
												$resultado = $conn->query($sql);
											}
											
											// Notificacion web
											notification('success', __('SiMatriculado', 'custom-translation'));

											// Borramos las fechas de $_SESSION
											unset($_SESSION['fechaCurso']);
											unset($_SESSION['fechaCursoFin']);
											
											// Session para la pantalla de matriculado
											$_SESSION['curso'] = $dataMail;
											redirect ('matriculado');
										}else{
											notification('error', __('NoMatriculadoTecnicos', 'custom-translation'));
										}
									}else{
										cleanStatusKey($_SESSION['userCampus']['idalumnos']);
										notification('error',  $matricula['mensajeError']);
									}
								}	
							}else{
								notification('error', __('NoMatriculadoFecha', 'custom-translation'));
							}
						} else {
							notification('error', __('NoMatriculadoFecha', 'custom-translation'));
						}
					}else {
					   notification('error', __('NoClaveUso', 'custom-translation'));
					}
				} else {
					notification('error', __('NoClave', 'custom-translation'));
				}
			}
		} else {
			notification('error', __('NoTerminos', 'custom-translation'));
		}

		redirectBack(3);
		
	} else {	

		$_SESSION['matriculaCurso']['idCurso'] = (!empty( $_POST['idCurso']))?  $_POST['idCurso'] : '';
		$_SESSION['matriculaCurso']['nombreCurso'] = (!empty( $_POST['nombreCurso']))? $_POST['nombreCurso'] : '';
		$_SESSION['matriculaCurso']['fechaCurso'] = (!empty( $_POST['fechaCurso']))? $_POST['fechaCurso'] : '';
		$_SESSION['matriculaCurso']['input_homologado'] = (!empty( $_POST['input_homologado']))? $_POST['input_homologado'] : '';

		include 'colaboracion_page.php';
	}
} else {
	redirect('login');
}

