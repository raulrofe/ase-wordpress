<div style="float:left; width:100%;" id="post-395" class="post-395 page type-page status-publish hentry">

	<!--automatic title-->
	<h1 class=""><strong><?php echo __('Perfil', 'custom-translation') ?></strong></h1><div class="nd_options_section nd_options_height_20"></div>
	<!--start content-->


	<div class="nd_learning_width_33_percentage nd_learning_float_left nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive">

		<div class="nd_learning_section nd_learning_box_sizing_border_box">

			<!--start preview-->
			<div class="nd_learning_section ">

				<!--image-->
				<div class="nd_learning_section nd_learning_position_relative">



					<img alt="" class="nd_learning_section" src="<?php echo get_avatar_campus_url(); ?>">

					<div class="nd_learning_bg_greydark_alpha_gradient nd_learning_position_absolute nd_learning_left_0 nd_learning_height_100_percentage nd_learning_width_100_percentage nd_learning_padding_20 nd_learning_box_sizing_border_box">
						<div class="nd_learning_position_absolute nd_learning_bottom_20">
							<h2 class="nd_learning_color_white_important">@<?php echo $_SESSION['userCampus']['nombre']; ?></h2>
						</div>

					</div>

				</div>
				<!--image-->

				<div class="nd_learning_section nd_learning_box_sizing_border_box">
					<div class="nd_learning_section nd_learning_bg_greydark">
						<table class="nd_learning_section ">
							<tbody>

								<tr class="">
									<td class="nd_learning_padding_30">
										<h5 class="nd_learning_font_size_13 nd_learning_text_transform_uppercase nd_options_color_grey"><?php _e('Name', 'nd-learning'); ?></h5>
										<div class="nd_learning_section nd_learning_height_5"></div>
										<p class="nd_learning_color_white_important nd_learning_line_height_16"><?php echo $_SESSION['userCampus']['nombre']; ?></p>
									</td>
									<td class="nd_learning_padding_30">
										<h5 class="nd_learning_font_size_13 nd_learning_text_transform_uppercase nd_options_color_grey"><?php _e('Last Name', 'nd-learning'); ?></h5>
										<div class="nd_learning_section nd_learning_height_5"></div>
										<p class="nd_learning_color_white_important nd_learning_line_height_16"><?php echo $_SESSION['userCampus']['apellidos']; ?></p>
									</td>
								</tr>

							</tbody>
						</table>
					</div>

					<div class="nd_learning_section nd_learning_border_1_solid_grey nd_learning_padding_20 nd_learning_box_sizing_border_box" style="overflow:hidden">

						<table class="nd_learning_section">
							<tbody>

								<tr class="">
									<td class="nd_learning_padding_10">

										<div class="nd_learning_display_table nd_learning_float_left">

											<div class="nd_learning_display_table_cell nd_learning_vertical_align_middle">
												<img alt="" class="nd_learning_margin_right_20" width="25" src="<?php echo plugins_url() ?>/nd-learning/assets/img/icons/icon-email-grey.svg">
											</div>

											<div class="nd_learning_display_table_cell nd_learning_vertical_align_middle">
												<h5 class="nd_learning_font_size_13 nd_learning_text_transform_uppercase"><?php _e('Email', 'nd-learning'); ?></h5>
												<div class="nd_learning_section nd_learning_height_5"></div>
												<p class=""><?php echo $_SESSION['userCampus']['email']; ?></p>
											</div>

										</div>

									</td>
								</tr>
								<?php if (!empty($nd_learning_current_user->user_url)): ?>
									<tr class="">
										<td class="nd_learning_padding_10">

											<div class="nd_learning_display_table nd_learning_float_left">

												<div class="nd_learning_display_table_cell nd_learning_vertical_align_middle">
													<img alt="" class="nd_learning_margin_right_20" width="25" src="<?php echo plugins_url() ?>/nd-learning/assets/img/icons/icon-link-grey.svg">
												</div>

												<div class="nd_learning_display_table_cell nd_learning_vertical_align_middle">
													<h5 class="nd_learning_font_size_13 nd_learning_text_transform_uppercase"><strong><?php _e('Url', 'nd-learning'); ?></strong></h5>
													<div class="nd_learning_section nd_learning_height_5"></div>
													<p class=""><?php echo $nd_learning_current_user->user_url; ?></p>
												</div>

											</div>

										</td>
									</tr>
								<?php endif; ?>
								<?php if (!empty($nd_learning_current_user->description)): ?>
									<tr class="">
										<td class="nd_learning_padding_10">
											<h5 class="nd_learning_font_size_13 nd_learning_text_transform_uppercase"><strong><?php _e('About Me', 'nd-learning'); ?></strong></h5>
											<div class="nd_learning_section nd_learning_height_5"></div>
											<p class=""><?php echo $nd_learning_current_user->description; ?></p>
										</td>
									</tr>
								<?php endif; ?>
							</tbody>
						</table>
					</div>


				</div>

				<div class="nd_learning_section nd_learning_padding_10 nd_learning_box_sizing_border_box nd_learning_bg_white ">

					<div class="nd_learning_width_50_percentage nd_learning_padding_10 nd_learning_box_sizing_border_box nd_learning_float_left nd_learning_text_align_center">
						<a class="nd_learning_display_inline_block nd_learning_color_white_important nd_learning_bg_green nd_learning_box_sizing_border_box nd_learning_width_100_percentage nd_options_first_font nd_learning_padding_8 nd_learning_border_radius_3 nd_learning_font_size_13" href="<?php echo get_profile_campus_page(); ?>"><?php echo __('Perfil', 'custom-translation') ?></a>
					</div>

					<div class="nd_learning_width_50_percentage nd_learning_padding_10 nd_learning_box_sizing_border_box nd_learning_float_left nd_learning_text_align_center">
						<a class="nd_learning_display_inline_block nd_learning_color_white_important nd_learning_bg_red nd_learning_box_sizing_border_box nd_learning_width_100_percentage nd_options_first_font nd_learning_padding_8 nd_learning_border_radius_3 nd_learning_font_size_13" href="<?php echo get_logout_campus_page(); ?>"><?php echo __('Salir', 'custom-translation') ?></a>
					</div>

				</div>



			</div>
			<!--start preview-->

		</div>

	</div>






	<div class="nd_learning_width_66_percentage nd_learning_float_left nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive">


		<!--START Tabs-->
		<div id="mi-perfil-tabs" class="nd_learning_tabs nd_learning_section ui-tabs ui-widget ui-widget-content ui-corner-all">

			<ul class="nd_learning_list_style_none nd_learning_margin_0 nd_learning_padding_0 nd_learning_section nd_learning_border_bottom_2_solid_grey ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
				<li class="nd_learning_display_inline_block ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="nd_learning_account_page_tab_order" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
					<h4>
						<a class="nd_learning_outline_0 nd_learning_padding_20 nd_learning_display_inline_block nd_options_first_font nd_options_color_greydark ui-tabs-anchor" href="#nd_learning_account_page_tab_order" role="presentation" tabindex="-1" id="ui-id-1">
							<?php echo __('InformacionPersonal', 'custom-translation') ?>
						</a>
					</h4>
				</li>
				<li class="nd_learning_display_inline_block ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="nd_learning_account_page_tab_bookmark" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false">
					<h4>
						<a class="nd_learning_outline_0 nd_learning_padding_20 nd_learning_display_inline_block nd_options_first_font nd_options_color_greydark ui-tabs-anchor" href="#nd_learning_account_page_tab_bookmark" role="presentation" tabindex="-1" id="ui-id-2">
							<?php echo __('DatosContacto', 'custom-translation') ?>
						</a>
					</h4>
				</li>
				<li class="nd_learning_display_inline_block ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="nd_learning_account_page_tab_compare" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false">
					<h4>
						<a class="nd_learning_outline_0 nd_learning_padding_20 nd_learning_display_inline_block nd_options_first_font nd_options_color_greydark ui-tabs-anchor" href="#nd_learning_account_page_tab_compare" role="presentation" tabindex="-1" id="ui-id-3">
							<?php echo __('Contrasena', 'custom-translation') ?>
						</a>
					</h4>
				</li>
				<li class="nd_learning_display_inline_block ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="nd_learning_account_page_tab_followers" aria-labelledby="ui-id-4" aria-selected="false" aria-expanded="false">
					<h4>
						<a class="nd_learning_outline_0 nd_learning_padding_20 nd_learning_display_inline_block nd_options_first_font nd_options_color_greydark ui-tabs-anchor" href="#nd_learning_account_page_tab_followers" role="presentation" tabindex="-1" id="ui-id-4">
							<?php echo __('Avatar', 'custom-translation') ?>
						</a>
					</h4>
				</li>
			</ul>


			<div class="nd_learning_section ui-tabs-panel ui-widget-content ui-corner-bottom" id="nd_learning_account_page_tab_order" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
				<div class="nd_learning_section nd_learning_height_40"></div>

				<form action="<?php echo get_edit_profile_campus_page() ?>" method="POST">
					<input type="hidden" name="action" value="tab-personal"/>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" name="name" value="<?php echo $_SESSION['userCampus']['nombre'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php _e('Name', 'nd-learning'); ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" name="lastname" value="<?php echo $_SESSION['userCampus']['apellidos'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php _e('Last Name', 'nd-learning'); ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" name="nif" value="<?php echo $_SESSION['userCampus']['dni'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('NIF', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" id="borndate" name="borndate" value="<?php echo date('d/m/Y', strtotime($_SESSION['userCampus']['f_nacimiento'])) ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('FechaNacimiento', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<select name="genre">
								<option value=""><?php echo __('SeleccioneGenero', 'custom-translation') ?></option>
								<option value="1" <?php
								if ($_SESSION['userCampus']['idsexo'] == 1) {
									echo 'selected="selected"';
								}
								?>
										><?php echo __('Hombre', 'custom-translation') ?></option>
								<option value="2" <?php
								if ($_SESSION['userCampus']['idsexo'] == 2) {
									echo 'selected="selected"';
								}
								?>
										><?php echo __('Mujer', 'custom-translation') ?></option>
							</select>
						</span>
					</div>
					<div class="nd_learning_section nd_learning_height_40"></div>
					<input type="submit" class="wpcf7-form-control wpcf7-submit">
				</form>
			</div>


			<div class="nd_learning_section ui-tabs-panel ui-widget-content ui-corner-bottom" id="nd_learning_account_page_tab_bookmark" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
				<div class="nd_learning_section nd_learning_height_40"></div>
				<form action="<?php echo get_edit_profile_campus_page() ?>" method="POST">
					<input type="hidden" name="action" value="tab-contact"/>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" name="telephone" value="<?php echo $_SESSION['userCampus']['telefono'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('Telefono', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" name="telephone2" value="<?php echo $_SESSION['userCampus']['telefono_secundario'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('TelefonoSecundario', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" name="address" value="<?php echo $_SESSION['userCampus']['direccion'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('Direccion', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" name="zip" value="<?php echo $_SESSION['userCampus']['cp'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('ZIP', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" name="poblation" value="<?php echo $_SESSION['userCampus']['poblacion'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('Poblacion', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" name="city" value="<?php echo $_SESSION['userCampus']['provincia'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('Provincia', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" name="country" value="<?php echo $_SESSION['userCampus']['pais'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('Pais', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_learning_section nd_learning_height_40"></div>
					<input type="submit" class="wpcf7-form-control wpcf7-submit">
				</form>
			</div>


			<div class="nd_learning_section ui-tabs-panel ui-widget-content ui-corner-bottom" id="nd_learning_account_page_tab_compare" aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true" style="display: none;">
				<div class="nd_learning_section nd_learning_height_40"></div>
				<form action="<?php echo get_edit_profile_campus_page() ?>" method="POST">
					<input type="hidden" name="action" value="tab-pass"/>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="password" name="pass" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('ContrasenaActual', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="password" name="pass1" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('ContrasenaNueva', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="password" name="pass2" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('ContrasenaConfirmar', 'custom-translation') ?>">
						</span>
					</div>
					<div class="nd_learning_section nd_learning_height_40"></div>
					<input type="submit" class="wpcf7-form-control wpcf7-submit">
				</form>
			</div>


			<div class="nd_learning_section ui-tabs-panel ui-widget-content ui-corner-bottom" id="nd_learning_account_page_tab_followers" aria-labelledby="ui-id-4" role="tabpanel" aria-hidden="true" style="display: none;">
				<div class="nd_learning_section nd_learning_height_40"></div>
				<form action="<?php echo get_edit_profile_campus_page() ?>" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="action" value="tab-avatar"/>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone " style="padding: 10px 0px; width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<img alt="" id="preview-avatar" class="section max_width left_0 top_0" src="">
							<div class="nd_learning_section nd_learning_height_40"></div>
							<!--image-->
							<input id="avatar" name="avatar" type="file" class="display_none"/>						</span>
					</div>
					<div class="nd_learning_section nd_learning_height_40"></div>
					<input type="submit" class="wpcf7-form-control wpcf7-submit">
				</form>
				<script
					src="https://code.jquery.com/jquery-3.2.1.js"
					integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
				crossorigin="anonymous"></script>
				<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
				<script type="text/javascript" src="http://aulasmarteditorial.com/wp-content/plugins/nd-shortcodes/shortcodes/custom/magic-popup/js/jquery.magnific-popup.min.js?ver=4.7.3"></script>
			
			
				<script>
					$('#avatar').change(function (e) {
						console.log(e);
						var file = e.target.files[0]; // FileList object
						if (file) {
							var reader = new FileReader();
							reader.onload = (function (theFile) {
								return function (e) {
									// Creamos la imagen.
									$('#preview-avatar').attr('src', e.target.result);
									$('#preview-avatar').attr('title', escape(theFile.name));
								};
							})(file);
							reader.readAsDataURL(file);
						} else {
							$('#preview-avatar').attr('src', '');
							$('#preview-avatar').attr('title', '');
						}
					});




					$.datepicker.regional["es"] = {
						closeText: "<?php echo __('Cerrar', 'custom-translation')?>",
						prevText: "< <?php echo __('AnteriorMin', 'custom-translation')?>",
						nextText: "<?php echo __('SiguienteMin', 'custom-translation')?> >",
						currentText: "<?php echo __('Hoy', 'custom-translation')?>",
						monthNames: ["<?php echo __('Enero', 'custom-translation')?>", "<?php echo __('Febrero', 'custom-translation')?>", "<?php echo __('Marzo', 'custom-translation')?>", "<?php echo __('Abril', 'custom-translation')?>", "<?php echo __('Mayo', 'custom-translation')?>", "<?php echo __('Junio', 'custom-translation')?>", "<?php echo __('Julio', 'custom-translation')?>", "<?php echo __('Agosto', 'custom-translation')?>", "<?php echo __('Septiembre', 'custom-translation')?>", "<?php echo __('Octubre', 'custom-translation')?>", "<?php echo __('Noviembre', 'custom-translation')?>", "<?php echo __('Diciembre', 'custom-translation')?>"],
						monthNamesShort: ["<?php echo __('Ene', 'custom-translation')?>", "<?php echo __('Feb', 'custom-translation')?>", "<?php echo __('Mar', 'custom-translation')?>", "<?php echo __('Abr', 'custom-translation')?>", "<?php echo __('May', 'custom-translation')?>", "<?php echo __('Jun', 'custom-translation')?>", "<?php echo __('Jul', 'custom-translation')?>", "<?php echo __('Ago', 'custom-translation')?>", "<?php echo __('Sep', 'custom-translation')?>", "<?php echo __('Oct', 'custom-translation')?>", "<?php echo __('Nov', 'custom-translation')?>", "<?php echo __('Dic', 'custom-translation')?>"],
						dayNames: ["<?php echo __('Domingo', 'custom-translation')?>", "<?php echo __('Lunes', 'custom-translation')?>", "<?php echo __('Martes', 'custom-translation')?>", "<?php echo __('Miércoles', 'custom-translation')?>", "<?php echo __('Jueves', 'custom-translation')?>", "<?php echo __('Viernes', 'custom-translation')?>", "<?php echo __('Sábado', 'custom-translation')?>"],
						dayNamesShort: ["<?php echo __('Dom', 'custom-translation')?>", "<?php echo __('Lun', 'custom-translation')?>", "<?php echo __('Mar', 'custom-translation')?>", "<?php echo __('Mie', 'custom-translation')?>", "<?php echo __('Jue', 'custom-translation')?>", "<?php echo __('Vie', 'custom-translation')?>", "<?php echo __('Sab', 'custom-translation')?>"],
						dayNamesMin: ["<?php echo __('Do', 'custom-translation')?>", "<?php echo __('Lu', 'custom-translation')?>", "<?php echo __('Ma', 'custom-translation')?>", "<?php echo __('Mi', 'custom-translation')?>", "<?php echo __('Ju', 'custom-translation')?>", "<?php echo __('Vi', 'custom-translation')?>", "<?php echo __('Sa', 'custom-translation')?>"],
						weekHeader: "Sm",
						dateFormat: "dd/mm/yy",
						firstDay: 1,
						isRTL: false,
						showMonthAfterYear: false,
						yearSuffix: ""
					};

					$.datepicker.setDefaults($.datepicker.regional["es"]);

					$("#borndate").datepicker({
						firstDay: "1",
						changeMonth: true,
						changeYear: true,
						 yearRange: '1900:+0'
					});
				</script>
			</div>
		</div>
		<!--END tabs-->



		<script type="text/javascript">
			<!--//--><![CDATA[//><!--
			jQuery(document).ready(function ($) {
				$('.nd_learning_tabs').tabs();
			});
			//--><!]]>
		</script>


	</div>








	<!--end content-->

</div>