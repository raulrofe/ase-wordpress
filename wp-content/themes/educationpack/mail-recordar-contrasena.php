<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p style="font-size: 15px;">Se ha solicitado la recuperación de su contraseña en AULA_SMART Editorial, para continuar con el proceso haga click en el siguiente enlace:</p>
		<p style="font-size: 15px;">www.aulasmarteditorial.com/recuperar-contrasena?id=<?php echo $dataMail['id'] ?>&token=<?php echo $dataMail['token'] ?></p>

		<p style="font-size: 15px;">
			<table>
				<tr>
					<td>
						<img src="http://fundacionaulasmart.org/images/logos/logo_ASE.png" alt="AULA_SMART Editorial" style="height:70px;width:auto;float:left;">
					</td>
					<td>
						AULA_SMART Editorial<br/>
						Tel. 951 41 18 00<br/>
						Fax  951 28 63 29<br/>
						<a href='www.aulasmarteditorial.com'>www.aulasmarteditorial.com</a>
					</td>
				</tr>
			</table>
		</p>

		<hr style="opacity:0.5">

		<font color='#555' size='1px'>
		<p style="font-size: 10px;">
			AVISO LEGAL<br/>

			Este mensaje y sus anexos pueden contener información confidencial, por lo que se
			informa de que su uso no autorizado está prohibido por la ley. Si Vd. considera que
			no es el destinatario pretendido por el remitente, por favor póngalo en su
			conocimiento por esta misma vía o por cualquier otro medio y elimine esta
			comunicación y los anexos de su sistema, sin copiar, remitir o revelar los
			contenidos del mismo a cualquier otra persona. Cualquier información, opinión,
			conclusión, recomendación, etc. contenida en el presente mensaje no relacionada con
			la actividad de la Fundación AULA_SMART y/o emitida por persona sin capacidad para
			ello, deberá considerarse como no proporcionada ni aprobada por la Fundación
			AULA_SMART. Asimismo la Fundación AULA_SMART pone los medios a su alcance para
			garantizar la seguridad y ausencia de errores en la correspondencia electrónica,
			pero no puede asegurar la inexistencia de virus o la no alteración de los documentos
			transmitidos electrónicamente, por lo que declina cualquier responsabilidad a este
			respecto.<br/>

			DISCLAIMER<br/>

			This message and its contents may contain confidential information and its
			non-authorised use is prohibited by law. If you are not the intended recipient of
			this email, please advise the sender of the fact using the same, or other, means and
			delete this message and its contents from your system without copying, forwarding or
			revealing the contents of the message to any other person. Any information, opinion,
			conclusion, recommendation, etc. contained in this message and which is unrelated to
			the business activity of AULA_SMART Foundation and/or issued by unauthorised
			personnel, shall be considered unapproved by AULA_SMART Foundation.
			AULA_SMART Foundation implements control measures to ensure, as far as possible,
			the security and reliability of all its electronic correspondence. However, the
			Foundation does not guarantee that emails are virus-free or that documents have
			not be altered and takes no responsibility in this respect.
		</p>
	</body>
</html>


