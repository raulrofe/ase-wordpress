<?php
if (!function_exists('redirectBack')) {

	function redirectBack($steps = 1) {
		echo "<script type='text/javascript'>window.history.go(-" . $steps . ")</script>";
		exit();
	}

}

if (!empty($_SESSION['userCampus']) && !empty($_SESSION['userCampus']['centro_ase']) && $_SESSION['userCampus']['centro_ase'] == 0) {
	redirectBack();
} else {
	?>







	<!--// COMPROBAR SI ES CENTRO O EXPULSA-->




	<div style="float:left; width:100%;" id="post-395" class="post-395 page type-page status-publish hentry">

		<!--automatic title-->
		<!--<h1 class=""><strong><?php // echo __('AcuerdoColaboracion', 'custom-translation')    ?></strong></h1><div class="nd_options_section nd_options_height_20"></div>-->
		<!--start content-->

		<div class="nd_learning_width_100_percentage nd_learning_float_left nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive">


			<!--START Tabs-->
			<div id="mi-perfil-tabs" class="nd_learning_tabs nd_learning_section ui-tabs ui-widget ui-widget-content ui-corner-all">

				<ul class="nd_learning_list_style_none nd_learning_margin_0 nd_learning_padding_0 nd_learning_section nd_learning_border_bottom_2_solid_grey ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
					<li class="nd_learning_display_inline_block ui-state-default ui-corner-top" role="tab" tabindex="0" aria-controls="nd_learning_account_page_tab_order" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
						<h4>
							<a class="nd_learning_outline_0 nd_learning_padding_20 nd_learning_display_inline_block nd_options_first_font nd_options_color_greydark ui-tabs-anchor" href="#nd_learning_account_page_tab_acuerdo" role="presentation" tabindex="-1" id="ui-id-1">
								<?php echo __('AcuerdoColaboracion', 'custom-translation') ?>
							</a>
						</h4>
					</li>
					<li class="nd_learning_display_inline_block ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="nd_learning_account_page_tab_bookmark" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false">
						<h4>
							<a class="nd_learning_outline_0 nd_learning_padding_20 nd_learning_display_inline_block nd_options_first_font nd_options_color_greydark ui-tabs-anchor" href="#nd_learning_account_page_tab_acuerdo_profesores" role="presentation" tabindex="-1" id="ui-id-2">
								<?php echo __('AcuerdoProfesores', 'custom-translation') ?>
							</a>
						</h4>
					</li>
					<li class="nd_learning_display_inline_block ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="nd_learning_account_page_tab_compare" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false">
						<h4>
							<a class="nd_learning_outline_0 nd_learning_padding_20 nd_learning_display_inline_block nd_options_first_font nd_options_color_greydark ui-tabs-anchor" href="#nd_learning_account_page_tab_acuerdo_historial" role="presentation" tabindex="-1" id="ui-id-3">
								<?php echo __('Historial', 'custom-translation') ?>
							</a>
						</h4>
					</li>
				</ul>


				<div class="nd_learning_section nd_learning_height_40"></div>




				<!-- PESTAÑA ACUERDO-->
				<div class="nd_learning_section ui-tabs-panel ui-widget-content ui-corner-bottom tabs-miacuerdo" id="nd_learning_account_page_tab_acuerdo" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false" style="margin: 0 !important;">
					<h3 class="h3-acuerdo"><?php echo __('DatosCentro', 'custom-translation') ?></h3>

					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20" style="width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" readonly="readonly" value="<?php echo $_SESSION['userCampus']['nombre'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php _e('Name', 'nd-learning'); ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20" style="width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" readonly="readonly" value="<?php echo $_SESSION['userCampus']['apellidos'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php _e('Last Name', 'nd-learning'); ?>">
						</span>
					</div>
					<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20" style="width:100%;">
						<span class="wpcf7-form-control-wrap nombre">
							<input type="text" readonly="readonly" value="<?php echo $_SESSION['userCampus']['email'] ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('Email', 'custom-translation') ?>">
						</span>
					</div>

					<?php include ('declaracion_responsable.php'); ?>
				</div>




				<!-- PESTAÑA PROFESORES-->
				<div class="nd_learning_section ui-tabs-panel ui-widget-content ui-corner-bottom tabs-miacuerdo" id="nd_learning_account_page_tab_acuerdo_profesores" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;margin: 0 !important;">
					<form action="<?php echo get_profile_acuerdo() . '/#nd_learning_account_page_tab_acuerdo_profesores'; ?>" method="POST">
						<input type="hidden" name="action" value="tab-contact"/>
						<input type="hidden" name="emailCentro" value="<?php echo $_SESSION['userCampus']['email'] ?>" />

						<h3 class="h3-acuerdo"><?php echo __('ProfesoresTarifa', 'custom-translation') ?></h3>

						<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_33_percentage nd_options_width_100_percentage nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20">
							<span class="wpcf7-form-control-wrap nombre">
								<input type="text" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('Name', 'custom-translation'); ?>" name="firstName">
							</span>
						</div>

						<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_33_percentage nd_options_width_100_percentage nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20">
							<span class="wpcf7-form-control-wrap nombre">
								<input type="text" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('LastName1', 'custom-translation'); ?>" name="lastName1">
							</span>
						</div>

						<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_33_percentage nd_options_width_100_percentage nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20">						<span class="wpcf7-form-control-wrap nombre">
								<input type="text" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('LastName2', 'custom-translation'); ?>" name="lastName2">
							</span>
						</div>


						<div class="nd_learning_clearfix"></div>


						<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_33_percentage nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20">
							<span class="wpcf7-form-control-wrap nombre">
								<input type="text" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('DNI', 'custom-translation'); ?>" name="nif">
							</span>
						</div>

						<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_33_percentage nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20">
							<span class="wpcf7-form-control-wrap nombre">
								<input type="text" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="<?php echo __('Email', 'custom-translation') ?>" name="email">
							</span>
						</div>
						<div class=" nd_options_width_100_percentage">
							<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_33_percentage nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20">
								<span class="wpcf7-form-control-wrap your-name">
									<select name="autonomo">
										<option value="1"><?php echo __('Autonomo', 'custom-translation') ?></option>
										<option value="0" selected="selected"><?php echo __('TrabajadorGeneral', 'custom-translation') ?></option>
									</select>
								</span>
							</div>
						</div>

						<div class="nd_learning_clearfix"></div>
						<div class="nd_learning_section nd_learning_height_20"></div>

						<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage nd_options_width_100_percentage nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20">
							<span class="wpcf7-form-control-wrap nombre">
								<input type="submit" value="<?php echo __('AddProfesor', 'custom-translation') ?>" />
							</span>
						</div>
					</form>


					<div class="nd_learning_section nd_learning_height_20"></div>

					<?php
					if (count($profesoresTarifaPlana) > 0):
						?>
						<table>
							<thead>
								<tr>
									<th><?php echo __('Nombre', 'custom-translation') ?></th>
									<th><?php echo __('Apellidos', 'custom-translation') ?></th>
									<th><?php echo __('Autonomo', 'custom-translation') ?></th>
									<th><?php echo __('Estado', 'custom-translation') ?></th>
									<th></th>
								</tr>
							</thead>

							<?php
							?>
							<?php
							foreach ($profesoresTarifaPlana as $profesor):
								?>

								<tr>
									<td><?php echo $profesor->nombre ?></td>
									<td><?php echo $profesor->apellidos ?></td>
									<td><?php echo ($profesor->autonomo == 1) ? __('SI', 'custom-translation') : __('NO', 'custom-translation') ?></td>
									<td><?php echo ($profesor->pendiente == 1) ? __('PENDIENTE', 'custom-translation') : __('VALIDO', 'custom-translation') ?></td>
									<td>
										<span title="<?php echo __('EliminarTrabajador', 'custom-translation') ?>" alt="<?php echo __('EliminarTrabajador', 'custom-translation') ?>" class="delete">
											<a href="<?php echo get_profile_acuerdo() . '?eliminar=' . $profesor->idprofesores; ?>">
												X
											</a>
										</span>
									</td>
								</tr>

								<?php
							endforeach;
							?>
						</table>
						<?php
					else:
						?>
						<div class=" nd_options_width_100_percentage">
							<div class="nd_options_float_left nd_options_box_sizing_border_box nd_options_width_100_percentage nd_options_width_100_percentage_all_iphone_important nd_options_padding_0_right_important_all_iphone nd_options_padding_0_left_important_all_iphone nd_options_padding_10_20">
								<p style="width:100%; text-align:center">No existen profesores inscritos en esta tarifa</p>
							</div>
						</div>
					<?php
					endif;
					?>
				</div>




				<!-- PESTAÑA HISTORIAL-->
				<div class="nd_learning_section ui-tabs-panel ui-widget-content ui-corner-bottom tabs-miacuerdo" id="nd_learning_account_page_tab_acuerdo_historial" aria-labelledby="ui-id-4" role="tabpanel" aria-hidden="false" style="margin: 0 !important;display: none;">
					<h3 class="h3-acuerdo"><?php echo __('HistorialProfesores', 'custom-translation') ?></h3>


					<script type="text/javascript" src="http://aulasmarteditorial.com/wp-content/plugins/js_composer/assets/lib/vc_accordion/vc-accordion.min.js?ver=4.12.1"></script>


					<div class="nd_learning_section nd_learning_position_relative container-historial-acuerdo">
						<div class="wpb_wrapper">
							<div class="vc_tta-container" data-vc-action="collapse">
								<div class="vc_general vc_tta vc_tta-accordion vc_tta-color-grey vc_tta-style-classic vc_tta-shape-rounded vc_tta-o-shape-group vc_tta-controls-align-left">
									<div class="vc_tta-panels-container">
										<div class="vc_tta-panels">
											<?php foreach ($profesoresTarifaPlana as $profesor): ?>

												<div class="vc_tta-panel" id="profesor<?php echo $profesor->idalumnos ?>" data-vc-content=".vc_tta-panel-body">
													<div class="vc_tta-panel-heading">
														<h4 class="vc_tta-panel-title vc_tta-controls-icon-position-left">
															<a href="#profesor<?php echo $profesor->idalumnos ?>" data-vc-accordion="" data-vc-container=".vc_tta-container">
																<span class="vc_tta-title-text"><?php echo ucwords(strtolower($profesor->nombre)) . ' ' . ucwords(strtolower($profesor->apellidos)) ?></span>
																<i class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i>
															</a>
														</h4>
													</div>
													<div class="vc_tta-panel-body" style="height: 0px;">
														<p>
															<?php if (!empty($profesor->cursoActual['idcurso_matriculado'])): ?>
																<span class="img-curso-acuerdo">
																	<img src="<?php echo nd_learning_get_course_image_url($profesor->cursoActual['idcurso_matriculado']) ?>"/>
																</span>
																<span>
																	<a target="_blank" href="<?php echo get_permalink($profesor->cursoActual['idcurso_matriculado']) ?>">
																		<?php echo get_the_title($profesor->cursoActual['idcurso_matriculado']) ?> 
																	</a>
																</span>
															<?php endif; ?>
														</p>

														<?php foreach ($profesor->cursos as $cursoMatriculado): ?>
															<p>
																<span class="img-curso-acuerdo">
																	<img src="<?php echo nd_learning_get_course_image_url($cursoMatriculado['id_ase']) ?>"/>
																</span>
																<span>
																	<a target="_blank" href="<?php echo get_permalink($cursoMatriculado['id_ase']) ?>">
																		<?php echo get_the_title($cursoMatriculado['id_ase']) ?> 
																	</a>
																</span>
															</p>							
														<?php endforeach; ?>
															
															
														<?php if (empty($profesor->cursoActual['idcurso_matriculado']) && $profesor->cursos->num_rows==0): ?>
															<p>Este profesor no ha relizado ningún curso</p>
														<?php endif; ?>
													</div>
												</div>
											
											<?php endforeach; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="nd_learning_section nd_learning_height_40"></div>
			</div>
			<!--END tabs-->



			<script type="text/javascript">
				<!--//--><![CDATA[//><!--
				jQuery(document).ready(function ($) {
					$('.nd_learning_tabs').tabs();
				});
				//--><!]]>
			</script>


		</div>
		<!--end content-->
	</div>




	<?php
}
?>