<?php
if (empty($_SESSION))
	session_start();

if(empty($_SESSION['curso'])){
	$url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
	echo "<script type='text/javascript'>"
			. "window.location='" . $url . "'"
		. "</script>";
	exit();
}
get_header();
?>
<?php
// CUSTOM WORDPRESS
/*
  Template Name: Matriculado
 */
?>


<?php
$nd_options_meta_box_page_header_img = get_post_meta(get_the_ID(), 'nd_options_meta_box_page_header_img', true);
$nd_options_meta_box_page_header_img_title = get_post_meta(get_the_ID(), 'nd_options_meta_box_page_header_img_title', true);
$nd_options_meta_box_page_header_img_position = get_post_meta(get_the_ID(), 'nd_options_meta_box_page_header_img_position', true);
?>
<!--start section-->
<div class="nd_options_section nd_options_background_size_cover <?php echo $nd_options_meta_box_page_header_img_position ?>" style="background-image:url(<?php echo $nd_options_meta_box_page_header_img; ?>);">
	<div class="nd_options_section nd_options_bg_greydark_alpha_gradient_2">
		<!--start nd_options_container-->
		<div class="nd_options_container nd_options_clearfix">
			<div class="nd_options_section nd_options_height_200"></div>
			<div class="nd_options_section nd_options_padding_15 nd_options_box_sizing_border_box">
				<strong class="nd_options_color_white nd_options_font_size_60 nd_options_font_size_40_all_iphone nd_options_line_height_40_all_iphone nd_options_first_font"><?php echo $nd_options_meta_box_page_header_img_title; ?></strong>
				<div class="nd_options_section nd_options_height_20"></div>
			</div>
		</div>
		<!--end container-->
	</div>
</div>
<!--end section-->

<!--Breadcrumbs-->
<?php do_action('nd_options_end_header_img_page_hook'); ?>
<!--Breadcrumbs-->


<div class="nicdark_section nicdark_height_50"></div>

<!--page margin-->
<?php
if (get_post_meta(get_the_ID(), 'nd_options_meta_box_page_margin', true) != 1) {
	echo '<div class="nd_options_section nd_options_height_50"></div>';
}
?>

<!--start nd_options_container-->
<div class="nd_options_container nd_options_padding_0_15 nd_options_box_sizing_border_box nd_options_clearfix">
	<span style="color:#0093C9">
		El proceso de matriculación se ha relizado correctamente.
	</span>
	<br>
	<p style="color:black">
		Bienvenido a AULA SMART,<br>
		Te has matriculado de forma satisfactoria en el curso <?php echo $_SESSION['curso']['cursoUsuario'] ?><br>
		El curso comienza el <?php echo $_SESSION['curso']['fechaCursoUsuario'] ?> y termina el <?php echo $_SESSION['curso']['fechaCursoFinUsuario'] ?><br>
		Puedes acceder a través de la siguiente URL: <a href="http://campusaulainteractiva.aulasmart.net/">http://campusaulainteractiva.com</a><br>
		Si tienes cualquier duda o consulta puedes contactar con tu tutor a través el mail <a href="mailto:profesores@aulasmarteditorial.com">profesores@aulasmarteditorial.com</a><br>
		Gracias por confiar en nosotros.
		
		<br>
		<br>
		
		<?php
			unset($_SESSION['curso']);
		?>
	</p>
</div>

<br>
<br>
<br>
<br>
<!--end container-->


<div class="nicdark_section nicdark_height_60"></div> 


<?php get_footer(); ?>