<?php
// CUSTOM WORDPRESS
if (empty($_SESSION))
	session_start();
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
	<head>

		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="Formación Profesional, Formación profesional a distancia, Formación del profesorado, IPad gratis, Universidad virtual, Cursos gratuitos, Becas, Magisterio, Magisterio infantil, Magisterio primaria, Estudiar gratis, Cursos para trabajadores, Cursos para desempleados"/>
		<script src="https://cdn.jsdelivr.net/sharer.js/latest/sharer.min.js"></script>
		<link rel='stylesheet' id='js_composer_front-css'  href='/wp-content/plugins/js_composer/assets/css/js_composer_tta.min.css?ver=4.12.1' type='text/css' media='all' />

		<?php wp_head(); ?>
		<link rel='stylesheet' href='/wp-content/themes/educationpack/style_force_color.css' type='text/css' media='all' />
		<link rel="stylesheet" id="nd_options_magnific_popup_style-css" href="/wp-content/plugins/nd-shortcodes/shortcodes/custom/magic-popup/css/magnific-popup.css?ver=4.7.3" type="text/css" media="all">
		<!--NO CACHE-->
		<meta http-equiv="Expires" content="0">
		<meta http-equiv="Last-Modified" content="0">
		<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
		<meta http-equiv="Pragma" content="no-cache">
		<!--NO CACHE-->
		
		<!--NORTON SAFEWEB-->
		<meta name="norton-safeweb-site-verification" content="vznrn-6troi4iebibhbfzvtoa4rfygc03-uz7l2e4mjxghldbzmkkie6ptfr6eqckpl8s2e6nxfyjejt6f8gsrof490julz-xi-zfumnpift40-v7z8xtexixokraw41" />
	</head>
	<body id="start_nicdark_framework" <?php body_class(); ?>>
		<script type="application/ld+json">
			{
			  "@context" : "http://schema.org",
			  "@type" : "Organization",
			  "name" : "AULA_SMART Editorial",
			  "url" : "http://aulasmarteditorial.com",
			  "sameAs" : [
				"https://www.facebook.com/fundacionaulasmart/",
				"https://twitter.com/aula_smart",
				"https://www.linkedin.com/company/fundaci%C3%B3n-aula-smart",
				"https://www.youtube.com/channel/UCemJy2V8SKhxBxMTuSMStKQ"
			 ]
			}
			</script> 
		<?php session_start(); ?>
			<script
				src="https://code.jquery.com/jquery-3.2.1.js"
				integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
			crossorigin="anonymous"></script>
		
			<script type="text/javascript" src="http://aulasmarteditorial.com/wp-content/plugins/nd-shortcodes/shortcodes/custom/magic-popup/js/jquery.magnific-popup.min.js?ver=4.7.3"></script>

			
		<?php if (!empty($_SESSION['notification']['msg'] && !empty($_SESSION['notification']['status']))) { ?>
			<!--		
			NOTIFICACIONES ANTIGUAS 
			<script>
				$(document).ready(function () {
					setTimeout(function () {
						$('#notification').slideUp();
					}, 5000);
					$("#target").click(function () {
						$('#notification').slideUp();
					});
				});
			</script>
<!--			<div id="notification" class="<?php // echo $_SESSION['notification']['status'] ?>">
				<p>
					//<?php // echo $_SESSION['notification']['msg'] ?>
					<a id="closeNotification">X</a>
				</p>
			</div>-->
			
			
			<!--NOTIFICACION POPUP-->
			<?php
				$strPopup = 
					'<div class="white-popup '. $_SESSION["notification"]["status"] .'" style="position: relative;background: #FFF;padding: 30px;width: auto;max-width: 500px;margin: 20px auto;color:black;text-align: center;font-size: 1.2em;">'.
						'<img class="image-popup error" src="/wp-content/uploads/logos/error.png" style="margin: 0px auto;width: 30px;top: 8px;opacity: 0.5;position: relative;	margin-right: 15px;	display:none;">'.
						'<img class="image-popup success" src="/wp-content/uploads/logos/success.png" style="margin: 0px auto;width: 30px;top: 8px;opacity: 0.5;position: relative;	margin-right: 15px;	display:none;">'.
						$_SESSION['notification']['msg'] .
					'</div>';
			?>
			
			<script>
				$(document).ready(function(){
					$.magnificPopup.open({
					  items: {
						src: '<?php echo $strPopup ?>',
						type: 'inline'
					  }
					});
				});
			</script>
			
			<?php $_SESSION['notification']['msg'] = ''; ?>
			<?php $_SESSION['notification']['status'] = ''; ?>
			
		<?php } ?>
			
			
			
		<script>
			function changeLinks(){
				var links = $('html a[href*="http://aulasmarteditorial"]');
				var host = location.hostname;
				for(var i=0;i<links.length;i++){
					var href=links[i].href;
					var res = href.replace("aulasmarteditorial.com", host);
					links[i].href = res;
				}
			}
			$( document ).ready(function() {
				changeLinks();
			});	
		</script>
		
		
			
		
		<!--KNOW IP COUNTRY-->
		<?php
		// Get country and city by IP
//		$url = 'http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR'];
//		$meta = unserialize(file_get_contents($url));
//		echo '<pre>';
//		print_r($meta);
//		echo '</pre>';
//		exit;
		?>
		<!--START theme-->
		<div class="nicdark_site nicdark_bg_white <?php
		if (is_front_page()) {
			echo esc_html("nicdark_front_page");
		}
		?> ">

			<?php
			if (function_exists('nicdark_headers')) {
				do_action("nicdark_header_nd");
			} else {
				?>

				<!--START section-->
				<div class="nicdark_section nicdark_bg_greydark">

					<!--start container-->
					<div class="nicdark_container nicdark_clearfix">


						<!--START LOGO OR TAGLINE-->
						<?php
						$nicdark_customizer_logo_img = get_option('nicdark_customizer_logo_img');
						if ($nicdark_customizer_logo_img == '' or $nicdark_customizer_logo_img == 0) {
							?>

							<div class="nicdark_grid_3 nicdark_text_align_center_responsive">
								<div class="nicdark_section nicdark_height_10"></div>
								<a class="" href="<?php echo esc_url(home_url()); ?>"><h3 class="nicdark_color_white"><?php echo esc_html(get_bloginfo('name')); ?></h3></a>
								<div class="nicdark_section nicdark_height_10"></div>
								<a href="<?php echo esc_url(home_url()); ?>"><p class="nicdark_font_size_13"><?php echo esc_html(get_bloginfo('description')); ?></p></a>
								<div class="nicdark_section nicdark_height_10"></div>
							</div>

							<?php
						} else {

							$nicdark_customizer_logo_img = wp_get_attachment_url($nicdark_customizer_logo_img);
							?>

							<div class="nicdark_grid_3 nicdark_text_align_center_responsive">
								<a class="" href="<?php echo esc_url(home_url()); ?>">
									<img class="nicdark_section" src="<?php echo esc_url($nicdark_customizer_logo_img); ?>">
								</a>
							</div>

						<?php } ?>
						<!--END LOGO OR TAGLINE-->



						<div class="nicdark_grid_9 nicdark_text_align_center_responsive">

							<div class="nicdark_section nicdark_height_10"></div>

							<div class="nicdark_section nicdark_navigation_1">
								<?php wp_nav_menu(array('theme_location' => 'main-menu')); ?>
							</div>

							<div class="nicdark_section nicdark_height_10"></div>

						</div>

					</div>
					<!--end container-->

				</div>
				<!--END section-->

			<?php } ?>

