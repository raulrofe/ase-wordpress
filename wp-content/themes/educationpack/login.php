<?php
// CUSTOM WORDPRESS
if (empty($_SESSION))
	session_start();
?>
<?php
if (!function_exists('notification')) {
	function notification($status, $msg) {
		$_SESSION['notification']['msg'] = $msg;
		$_SESSION['notification']['status'] = $status;
	}
}

if (!function_exists('redirect')) {
	function redirect($path = '') {
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $path;
		echo "<script type='text/javascript'>window.location='" . $url . "'</script>";
		exit();
	}
}

if (!function_exists('redirectBackLogin')) {
	function redirectBackLogin($steps = 1) {
		echo "<script type='text/javascript'>window.history.go(-".$steps.")</script>";
		exit();
	}
}

function userExist($email) {
	$conn = connectDBCampus();
	$sql = "SELECT * FROM alumnos WHERE email = '" . $email . "'";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		if ($user = $result->fetch_assoc()) {
			return true;
		}
	}
	return false;
}

function sendMailRegister($name, $email, $token) {
	$link = 'http://' . $_SERVER[HTTP_HOST] . '/activacion-usuario/?token=' . $token;
	ob_start();
	include 'wp-content/themes/educationpack/mail-activation.php';
	$message = ob_get_clean();
	$data = array(
				'email' => $email,
				'subject' => __('RegistroASE', 'custom-translation'),
				'message' => $message,
		);
	sendMail($data);
}
function str_random($length = 16){
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
}
function getToken() {
	return hash_hmac('sha256', str_random(40), 'AULA_SMART Editorial');
}


?>
<?php
// CUSTOM WORDPRESS
/*
  Template Name: Login
 */
if (isLoginCampus()) {
	redirect('');
} else {
	if ($_POST) { // Si es un login
		if ((!empty($_POST['log']) && !empty($_POST['pwd']))) { // Si es un login
			if ($_POST['rememberme'])
				$remember = true;
			else
				$remember = false;
			$login_data = array();
			$login_data['user_login'] = $_REQUEST['log'];
			$login_data['user_password'] = $_REQUEST['pwd'];
			$login_data['remember'] = $remember;

			$conn = connectDBCampus();
			$sql = "SELECT * FROM alumnos WHERE email = '" . $login_data['user_login'] . "' AND pass = '" . md5($login_data['user_password']) . "'";

			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				if ($user = $result->fetch_assoc()) {
					
					if ($user['activated'] != 1) {
						// Si el usuario no esta activo e intenta loguearse, generamos un nuevo token y un nuevo correo al usuario
						// Generamos un token de activacion y lo guardamos
						$token = getToken();
						$sql = "UPDATE activation_user SET token='". $token . "' WHERE iduser='" . $user['idalumnos'] . "'";
						if ($conn->query($sql) === TRUE) { // Si se ha actualizado bien el token
							$sql = "SELECT * FROM alumnos WHERE idalumnos = '" . $user['idalumnos'] . "'";

							$result= $conn->query($sql);
							if ($result->num_rows > 0) {
								if ($user = $result->fetch_assoc()) {
									sendMailRegister( $user['nombre'], $user['email'], $token);
									notification('success', __('SiNuevaActivacion', 'custom-translation'));
								}
							}
						}
						redirect('');
					} else {
						if($login_data['remember']){
							setcookie( "ase_r", 1,strtotime('+30 days'));
							setcookie( "ase_u", encrypt($login_data['user_login']), strtotime( '+30 days' ) );
							setcookie( "ase_p", encrypt($login_data['user_password']), strtotime( '+30 days' ) );
						}else{
							setcookie( "ase_r", 0, strtotime('-30 days'));
							setcookie( "ase_u", null, strtotime( '-30 days' ) );
							setcookie( "ase_p", null, strtotime( '-30 days' ) );
						}
						
						// Check if course enrolled is finished to delete from "Acuerdo_colaboracion_ase" table
						isFinishedCourse($user);
							
						$_SESSION['userCampus'] = $user;
						redirectBackLogin(2);
					}
					
				}
			} else {
				notification('error', __('NoCredenciales', 'custom-translation'));
				redirect('login');
			}
		} elseif ($_POST && (!empty($_POST['nd_learning_first_name']) && !empty($_POST['nd_learning_last_name']) && !empty($_POST['nd_learning_email']) && !empty($_POST['nd_learning_password']))) {
			//
			// SI ES UN REGISTRO
			//
			$email = $_POST['nd_learning_email'];
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				if(userExist($email)){
					notification('error', __('NoEmailUsado', 'custom-translation'));
					redirect('login');
				}else{
					$conn = connectDBCampus();
					$sql = "INSERT INTO alumnos( nombre, apellidos, email, pass, activated) "
						. "VALUES ('" . $_POST['nd_learning_first_name'] . "','"
						. $_POST['nd_learning_last_name'] . "','"
						. $email . "','"
						. md5($_POST['nd_learning_password']) . "',"
						. "0);";
					if ($conn->query($sql) === TRUE) { // Si se ha insertado bien el usuario
						// Generamos un token de activacion y lo guardamos
						$last_id = $conn->insert_id;
						$token = getToken();
						$sql = "INSERT INTO activation_user( iduser, token) VALUES ('" . $last_id . "','". $token . "')";
						if ($conn->query($sql) === TRUE) { // Si se ha insertado bien el token

							sendMailRegister( $_POST['nd_learning_first_name'], $email, $token);
							notification('success', __('SiRegistrado', 'custom-translation'));
							redirect('');

						}
					}else{
						notification('error', __('NoRegistrado', 'custom-translation'));
					}
				}
			}else{
				notification('error', __('NoEmailValido', 'custom-translation'));
			}

			redirectBack();
		} else {
			redirect('login');
		}
	} else {
		// NO LOGIN NI REGISTRO
		include 'page.php';
	}
}
