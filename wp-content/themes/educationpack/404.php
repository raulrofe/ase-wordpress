<?php 
if (empty($_SESSION))
	session_start();
	get_header(); 
?>
<!--start section-->
<div class="nd_options_section nd_options_background_size_cover nd_options_background_position_center_bottom" style="background-image:url(http://aulasmarteditorial.com/wp-content/uploads/2017/03/escribir.jpg);">
	<div class="nd_options_section nd_options_bg_greydark_alpha_gradient_2">
		<!--start nd_options_container-->
		<div class="nd_options_container nd_options_clearfix">
			<div class="nd_options_section nd_options_height_200"></div>
			<div class="nd_options_section nd_options_padding_15 nd_options_box_sizing_border_box">
				<strong class="nd_options_color_white nd_options_font_size_60 nd_options_font_size_40_all_iphone nd_options_line_height_40_all_iphone nd_options_first_font"></strong>
				<div class="nd_options_section nd_options_height_20"></div>
			</div>
		</div>
		<!--end container-->
	</div>
</div>
<!--end section-->

<!--Breadcrumbs-->
<?php do_action('nd_options_end_header_img_page_hook'); ?>
<!--Breadcrumbs-->


<div class="nicdark_section nicdark_height_50"></div>

<!--page margin-->
<?php
if (get_post_meta(get_the_ID(), 'nd_options_meta_box_page_margin', true) != 1) {
	echo '<div class="nd_options_section nd_options_height_50"></div>';
}
?>


<?php if( function_exists('nicdark_404_content')){ do_action( "nicdark_404_nd" ); }else{ ?>

<div class="nicdark_section nicdark_height_150"></div>

<section class="nicdark_section">
    <div class="nicdark_container nicdark_clearfix">
        <div class="nicdark_grid_12 nicdark_text_align_center">

        	<h1 class="nicdark_font_size_100"><strong>404</strong></h1>
        	<div class="nicdark_section nicdark_height_20"></div>
            <p><?php esc_html_e('That page can not be found','educationpack'); ?></p>
            <p>
				<?php echo __('Info404', 'custom-translation') ?>
				<a href="mailto:info@aulasmarteditorial.com" style="color:#0093C9">info@aulasmarteditorial.com</a>
			</p>
                 
        </div>    
    </div>
</section>

<div class="nicdark_section nicdark_height_150"></div>

<?php } ?>

<?php get_footer(); ?>