<?php
if (empty($_SESSION))
	session_start();
get_header();
?>

<?php
$nd_options_meta_box_page_header_img = get_post_meta(get_the_ID(), 'nd_options_meta_box_page_header_img', true);
$nd_options_meta_box_page_header_img_title = get_post_meta(get_the_ID(), 'nd_options_meta_box_page_header_img_title', true);
$nd_options_meta_box_page_header_img_position = get_post_meta(get_the_ID(), 'nd_options_meta_box_page_header_img_position', true);
?>
<!--start section-->
<div class="nd_options_section nd_options_background_size_cover <?php echo $nd_options_meta_box_page_header_img_position ?>" style="background-image:url(<?php echo $nd_options_meta_box_page_header_img; ?>);">
	<div class="nd_options_section nd_options_bg_greydark_alpha_gradient_2">
		<!--start nd_options_container-->
		<div class="nd_options_container nd_options_clearfix">
			<div class="nd_options_section nd_options_height_200"></div>
			<div class="nd_options_section nd_options_padding_15 nd_options_box_sizing_border_box">
				<strong class="nd_options_color_white nd_options_font_size_60 nd_options_font_size_40_all_iphone nd_options_line_height_40_all_iphone nd_options_first_font"><?php echo $nd_options_meta_box_page_header_img_title; ?></strong>
				<div class="nd_options_section nd_options_height_20"></div>
			</div>
		</div>
		<!--end container-->
	</div>
</div>
<!--end section-->

<!--Breadcrumbs-->
<?php do_action('nd_options_end_header_img_page_hook'); ?>
<!--Breadcrumbs-->


<div class="nicdark_section nicdark_height_50"></div>

<!--page margin-->
<?php
if (get_post_meta(get_the_ID(), 'nd_options_meta_box_page_margin', true) != 1) {
	echo '<div class="nd_options_section nd_options_height_50"></div>';
}
?>

<!--start nd_options_container-->
<div class="nd_options_container nd_options_padding_0_15 nd_options_box_sizing_border_box nd_options_clearfix">

	<div class="nd_learning_section">
		<div class="nd_learning_width_50_percentage nd_learning_float_left nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive">

			<div class="nd_learning_section nd_learning_border_radius_3 nd_learning_border_1_solid_grey nd_learning_padding_20 nd_learning_box_sizing_border_box">

				<h3 class="nd_options_second_font nd_learning_bg_green nd_learning_padding_5 nd_learning_border_radius_3 nd_learning_color_white_important nd_learning_display_inline_block"><?php echo __('YaAcuerdo', 'custom-translation') ?></h3>
				<div class="nd_learning_section nd_learning_height_5"></div>


				<form action="/acuerdo-colaboracion" method="post" data-acuerdo-colaboracion="1" name="nd_learning_shortcode_account_login_form" id="nd_learning_shortcode_account_login_form">
					<p>
						<br>
						<?php echo __('YaAcuerdoInfo', 'custom-translation') ?>
					</p>
					<p>
						<label class="nd_learning_section nd_learning_margin_top_20"><?php echo __('Correo', 'custom-translation') ?></label>
						<input readonly type="text" name="email" class=" nd_learning_section" value="<?php echo $_SESSION['userCampus']['email'] ?>">
					</p>
					<p class="login-password">
						<label for="nd_learning_login_form_password"><?php echo __('Clave', 'custom-translation') ?></label>
						<input type="text" name="key" id="key" class="input" value="" size="20">
					</p>

					<p style="text-align:center;">
						<label>
							<input type="checkbox" id="terminos" name="terminos"/><?php echo __('Terminos', 'custom-translation') ?>
						</label>
					</p>

					<input type="hidden" name="acuerdoColaboracion" value="1" />
					<input type="hidden" name="idCurso" value="<?php echo $_SESSION['matriculaCurso']['idCurso'] ?>" />
					<input type="hidden" name="nombreCurso" value="<?php echo $_SESSION['matriculaCurso']['nombreCurso'] ?>" />
					<input type="hidden" name="fechaCurso" value="<?php echo $_SESSION['matriculaCurso']['fechaCurso'] ?>" />
					<input type="hidden" name="input_homologado" value="<?php echo $_SESSION['matriculaCurso']['input_homologado'] ?>" />

					<input type="hidden" name="checkSubmitForm" value="1" />

					<p class="login-submit">
						<input type="submit" name="wp-submit" id="nd_learning_login_form_submit" class="button button-primary" value="<?php echo __('AccederCurso', 'custom-translation') ?>">
					</p>
				</form>
			</div>
		</div>
		
		<!--SCRIPT TO BLOCKED RECHARGE FORM -->
		<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="http://aulasmarteditorial.com/wp-content/plugins/nd-shortcodes/shortcodes/custom/magic-popup/js/jquery.magnific-popup.min.js?ver=4.7.3"></script>
		<script>
			$(document).ready(function(){
				$('form[data-acuerdo-colaboracion]').submit(function(){
					// Disabled buttons
					$(this).find('input[type="submit"]').prop('disabled', true);
					// Generate popup without close function, to blocked screen while script is running
					var strPopup = '<div class="white-popup loading" style="position: relative;background: #FFF;padding: 30px;width: auto;max-width: 500px;margin: 20px auto;color:black;text-align: center;font-size: 1.2em;">'+
										'<img class="image-popup loading" src="/wp-content/uploads/logos/loading.gif" style="margin: 0px auto;width: 120px;position: relative;display: block;margin-right: auto;margin-left: auto;top: 0px;">'+
										'El proceso puede durar varios minutos, espere un momento...'+
									'</div>';
					$.magnificPopup.open({
						items: {
						  src:			strPopup,
						  type:			'inline',
						},
						closeOnBgClick: false,
						closeBtnInside: false,
						showCloseBtn:	false,
						enableEscapeKey:false,
					});
				});
			});
		</script>
		
		
		
		


		<div class="nd_learning_width_50_percentage nd_learning_float_left nd_learning_box_sizing_border_box nd_learning_padding_15 nd_learning_width_100_percentage_responsive">

			<div class="nd_learning_section nd_learning_bg_white nd_learning_border_radius_3 nd_learning_border_1_solid_grey nd_learning_padding_20 nd_learning_box_sizing_border_box">

				<h3 class="nd_options_second_font nd_learning_bg_orange nd_learning_padding_5 nd_learning_border_radius_3 nd_learning_color_white_important nd_learning_display_inline_block"><?php echo __('NoAcuerdo', 'custom-translation') ?></h3>
				<div class="nd_learning_section nd_learning_height_5"></div>


				<form action="/acuerdo-colaboracion" method="post">
					<p>
						<br>
						<?php echo __('NoAcuerdoInfo', 'custom-translation') ?>
					</p>
					<p>
						<label class="nd_learning_section nd_learning_margin_top_20"><?php echo __('Correo', 'custom-translation') ?></label>
						<input readonly type="text" name="email" class=" nd_learning_section" value="<?php echo $_SESSION['userCampus']['email'] ?>">
					</p>
					<p>
						<label class="nd_learning_section nd_learning_margin_top_20"><?php echo __('Telefono', 'custom-translation') ?></label>
						<input type="text" name="telefono" class=" nd_learning_section" >
					</p>
					<p style="text-align:center;">
						<label class="nd_learning_section nd_learning_margin_top_20">
							<input type="checkbox" id="terminos" name="terminos"/><?php echo __('Terminos', 'custom-translation') ?>
						</label>
					</p>

					<input type="hidden" name="acuerdoColaboracion" value="0" />
					<input type="hidden" name="checkSubmitForm" value="1" />

					<input class="nd_learning_section nd_learning_margin_top_20" type="submit" name="submit" value="<?php echo __('SolicitarInformacion', 'custom-translation') ?>">
				</form>
			</div>

		</div>
	</div>
</div>
<!--end container-->


<div class="nicdark_section nicdark_height_60"></div> 


<?php get_footer(); ?>