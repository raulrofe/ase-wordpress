<?php 

// VARIABLES
$dirFile= 'extractos/';
include $dirFile . 'simplexlsx.class.php';
$nameFile= 'Temarios.xlsx';
$file= $dirFile.$nameFile;
$data=array();
$conn = connectDB();

$data['form'] = '1198';
$data['colour'] = '#0093C9';
$data['today'] = date("l jS \of F Y h:i:s A");
$data['today_short'] = date('Y-m-d H:i:s');
$data['today_short'] = date('Y-m-d H:i:s', strtotime($data['today_short']  . ' -1 day'));
$data['date_start'] = date("Y-m-d H:i:s", mktime(0, 0, 0, 6, 1, 2017));

// START SCRIPT
if(!empty($conn)){
    $startXML = startHeaderXML($data);
    $endXML = endHeaderXML();
    $contentXML = contentXML($file, $data);
    $xml = $startXML . $contentXML . $endXML;
    saveXML($xml); 
    downloadXML();
}


function toSlug($string,$space="-") {
    if (function_exists('iconv')) {
        $string = @iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    }
    $string = preg_replace("/[^a-zA-Z0-9 -]/", "", $string);
    $string = mb_strtolower ($string);
    $string = str_replace(" ", $space, $string);
    return $string;
}


function connectDB(){
    // CONNECT TO DATABASE
    $host_name = "db674908698.db.1and1.com";
    $database = "db674908698";
    $user_name = "dbo674908698";
    $password = "toxlIgVu";
    $conn  = mysqli_connect($host_name, $user_name, $password, $database);
    if(mysqli_connect_errno()){
        echo '<p>Error al conectar con servidor MySQL: '.mysqli_connect_error().'</p>';
        return;
    }
    $conn->query("SET NAMES utf8");
    return $conn;
}


function saveXML($xml){
    $file=fopen("export.txt","w") or die("Problemas en la creacion");//En esta linea lo que hace PHP es crear el archivo, si ya existe lo sobreescribe 
    fputs($file, $xml);//En esta linea abre el archivo creado anteriormente e ingresa el resultado de tu script PHP 
    fclose($file);//Finalmente lo cierra  
}


function downloadXML(){
    $ruta="export.txt"; 
    header ("Content-Disposition: attachment; filename=".$ruta); 
    header ("Content-Type: application/octet-stream"); 
    header ("Content-Length: ".filesize($ruta)); 
    readfile($ruta);  
}


function startHeaderXML($data){
    $str = 
        '<?xml version="1.0" encoding="UTF-8" ?>

        <rss version="2.0"
            xmlns:excerpt="http://wordpress.org/export/1.2/excerpt/"
            xmlns:content="http://purl.org/rss/1.0/modules/content/"
            xmlns:wfw="http://wellformedweb.org/CommentAPI/"
            xmlns:dc="http://purl.org/dc/elements/1.1/"
            xmlns:wp="http://wordpress.org/export/1.2/"
        >

        <channel>
            <title>AULA_SMART Editorial</title>
            <link>http://www.aulasmarteditorial.com</link>
            <description>AULA_SMART Editorial</description>
            <pubDate>' . $data['today'] . '</pubDate>
            <language>es-ES</language>
            <wp:wxr_version>1.2</wp:wxr_version>
            <wp:base_site_url>http://www.aulasmarteditorial.com/</wp:base_site_url>
            <wp:base_blog_url>http://www.aulasmarteditorial.com</wp:base_blog_url>

            <wp:author><wp:author_id>1</wp:author_id>
            <wp:author_login><![CDATA[admin]]></wp:author_login>
            <wp:author_email><![CDATA[gabrielvalero@ibericasur.com]]></wp:author_email>
            <wp:author_display_name><![CDATA[admin]]></wp:author_display_name>
            <wp:author_first_name><![CDATA[]]></wp:author_first_name>
            <wp:author_last_name><![CDATA[]]></wp:author_last_name></wp:author>';
        return $str;    
    }

         
function endHeaderXML(){
    $str = '</channel>
            </rss>';
    return $str;
}       


function contentXML($file, $data){
    $xlsx = new SimpleXLSX($file);
    $contentFile = $xlsx->rows();
    $n=0;
    $subn=0;
    $str='';

    foreach($contentFile as $register){
         if(count($register)==1 && !empty($register[1])){ 
         // Si no tiene numero (un solo campo) es un subtema
                $name=$register[1];
                $subUnit= ($subn==0)? '' : '.'.$subn;
                $str .= 'Tema '. $n.$subUnit. ' '.$name.'<br>';
        }elseif(count($register)>1 && !empty($register[0])){ 
        // Si tiene numero es un tema
            $subn=0;
            $n = $register[0];
            $name=$register[1];

            if(!empty($str)){ // Añadimos el final del tema para empezar otro, por eso el mirar si esta vacia la variable $str
                $str .= endItem($n, $name, $data);
            }

            // Principio del tema
            $str.='<item>
                <title>'.$name.'</title>
                <pubDate>' . $data['today'] . '</pubDate>
                <dc:creator><![CDATA[Gabriel]]></dc:creator>
                <description></description>
                <content:encoded><![CDATA[';

        }

        

           

        $subn= (int)$subn + (int)1;

    }
    
    $str .= endItem($n, $name, $data);
    return $str;
}

function endItem($n, $name, $data){
    $str=']]></content:encoded>
        <excerpt:encoded><![CDATA[]]></excerpt:encoded>
        <wp:post_date><![CDATA[' . $data['today_short'] . ']]></wp:post_date>
        <wp:post_date_gmt><![CDATA[' . $data['today_short'] . ']]></wp:post_date_gmt>
        <wp:comment_status><![CDATA[closed]]></wp:comment_status>
        <wp:ping_status><![CDATA[closed]]></wp:ping_status>
        <wp:post_name><![CDATA['.  toSlug($name).']]></wp:post_name>
        <wp:status><![CDATA[publish]]></wp:status>
        <wp:post_parent>0</wp:post_parent>
        <wp:menu_order>0</wp:menu_order>
        <wp:post_type><![CDATA[nd_learning_docs]]></wp:post_type>
        <wp:post_password><![CDATA[]]></wp:post_password>
        <wp:is_sticky>0</wp:is_sticky>
        <wp:postmeta>
            <wp:meta_key><![CDATA[nd_options_meta_box_page_title]]></wp:meta_key>
            <wp:meta_value><![CDATA[0]]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[nd_options_meta_box_page_margin]]></wp:meta_key>
            <wp:meta_value><![CDATA[0]]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[nd_options_meta_box_post_title]]></wp:meta_key>
            <wp:meta_value><![CDATA[0]]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[nd_options_meta_box_post_margin]]></wp:meta_key>
            <wp:meta_value><![CDATA[0]]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[_vc_post_settings]]></wp:meta_key>
            <wp:meta_value><![CDATA[a:1:{s:10:"vc_grid_id";a:0:{}}]]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[_edit_last]]></wp:meta_key>
            <wp:meta_value><![CDATA[1]]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[nd_learning_meta_box_document_subtitle]]></wp:meta_key>
            <wp:meta_value><![CDATA[Tema '.$n.']]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[nd_learning_meta_box_document_color]]></wp:meta_key>
            <wp:meta_value><![CDATA[]]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[nd_learning_meta_box_document_icon]]></wp:meta_key>
            <wp:meta_value><![CDATA[http://aulasmarteditorial.com/wp-content/uploads/2016/08/01-document-icon-grey.png]]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[nd_learning_meta_box_document_visibility]]></wp:meta_key>
            <wp:meta_value><![CDATA[nd_learning_meta_box_document_visibility_public]]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[slide_template]]></wp:meta_key>
            <wp:meta_value><![CDATA[default]]></wp:meta_value>
        </wp:postmeta>
        <wp:postmeta>
            <wp:meta_key><![CDATA[_thumbnail_id]]></wp:meta_key>
            <wp:meta_value><![CDATA[]]></wp:meta_value>
        </wp:postmeta>
        </item>';   
        return $str;    
}

?>