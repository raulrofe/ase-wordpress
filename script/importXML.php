<?php 
// VARIABLES
$dir= 'catalogo';


// CONNECT TO DATABASE
$host_name = "db674908698.db.1and1.com";
$database = "db674908698";
$user_name = "dbo674908698";
$password = "toxlIgVu";
$conn  = mysqli_connect($host_name, $user_name, $password, $database);
if(mysqli_connect_errno()){
	echo '<p>Error al conectar con servidor MySQL: '.mysqli_connect_error().'</p>';
}


// GET XML FILE ON DIR
$array = array();
$directorio = opendir("./" . $dir); //ruta actual
while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
{
    if (is_dir($archivo)){//verificamos si es o no un directorio
        echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
    }else {
        $extension = get_mime_type($archivo);
       if($extension == 'application/xml'){
       		array_push($array, $archivo);
       }
    }
}

for ($i = 0; $i < count($array); $i++) {
	$file= $array[$i];
	$name = $dir . '/' . $file;
	echo 'Nº'.($i+1) . ' . ' . $name . ' => ';

	if (file_exists($name) ) {
	    
	    $xml = simplexml_load_file($name);
	 
	    $nombre_familia = $xml->familia->nombre_familia;
	    $curso = $xml->familia->articulos->articulo;

	    $codigo_articulo = $curso->cod_articulo;
	    $nombre_articulo = $curso->nombre_articulo;
	    $horas = $curso->horas;
	    $modalidad =  $curso->modalidad;
	    // capacita + objetivos + dirigido + otros datos + contenido_formativo
	    $objetivos = $curso->contenido->objetivos;
	    $capacita = $curso->contenido->capacita_para;
	    $dirigido = $curso->contenido->dirigido_a;
	    $otrosdatos = $curso->contenido->otros_datos;
	    $contenido_formativo = $curso->contenido->contenido_formativo;

        // Parseamos el campo "nombre_articulo" para obtener los distintos datos
        $arrayName = explode(".", $nombre_articulo);
        $nombre = trim($arrayName[0]);
        // Si existe string depues del primer punto
        if(!empty($arrayName[1])){
            $arrayCodigo = explode(" ", trim($arrayName[1]));
            $codigo_certificado = trim($arrayCodigo[0]);
        }else{
            $codigo_certificado = 0;
        }

        // Si existe string depues del código del curso
        if(!empty($arrayCodigo[1])){
            unset($arrayCodigo[0]); // Quitamos el propio codigo
            $str = implode(" ", $arrayCodigo); // Y lo volvemos a unir
            $str = trim($str, "-"); // Quitamos el primer guion si tiene
            $arrayStr = explode("-", trim($str));
            $category = trim($arrayStr[0]);
        }else{
            $category = NULL;
        }
 

	    $sql = "SELECT * FROM tabla_xml WHERE codigo_articulo = ".$codigo_articulo;
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			echo 'Ya existe ese curso ('. $codigo_articulo.')<br>';
		}else{
		    // INSERTAMOS LOS DATOS
		    $tildes = $conn ->query("SET NAMES 'utf8'"); 
		    $sql = "INSERT INTO tabla_xml VALUES ("
				. "'', " // codigo_certificado
				. "'" .  $codigo_articulo ."', "
                . "'" .  $codigo_certificado ."', "
                . "'', " // codigo_curso
				. "'" .   ucfirst(mb_strtolower($nombre_familia)) . "', " 
                . "'" .   ucfirst(mb_strtolower($category)) . "', " 
				. "'" .   ucfirst(mb_strtolower($nombre)) ."', "
				. "'" .  $horas . "', "
				. "'" .   ucfirst(mb_strtolower($modalidad)) . "', "
                . "'', " // extracto
				. "'" .  $objetivos . "', "
				. "'" .  $capacita . "', "
				. "'" .  $dirigido . "', "
				. "'" .  $otrosdatos . "',"
				. "'" .  $contenido_formativo."')";
			//echo $sql;
			mysqli_query($conn , $sql);
			echo 'Insertando ('.$codigo_articulo.')<br>';
		}
			//mysqli_close($conn ); 



	} else {
	    echo 'No existe el fichero ' . $file.'<br>';
	}
 
}




function get_mime_type($filename) {
    $idx = explode( '.', $filename );
    $count_explode = count($idx);
    $idx = strtolower($idx[$count_explode-1]);

    $mimet = array( 
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',

        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',

        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',

        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',

        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',

        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',


        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    );

    if (isset( $mimet[$idx] )) {
     return $mimet[$idx];
    } else {
     return 'application/octet-stream';
    }
 }
?>