<?php 

// VARIABLES
$num_courses_page=200;
$dir= 'catalogo';
$data=array();
$conn = connectDB();

$data['form'] = '1198';
$data['colour'] = '#0093C9';
$data['today'] = date("l jS \of F Y h:i:s A");
$data['today_short'] = date('Y-m-d H:i:s');
$data['today_short'] = date('Y-m-d H:i:s', strtotime($data['today_short']  . ' -1 day'));
$data['date_start'] = date("Y-m-d H:i:s", mktime(0, 0, 0, 6, 1, 2017));
$data['price'] = '690';
$data['attendes'] = '100';
$data['header_background'] = 'http://www.nicdarkthemes.com/themes/education/wp/demo/design-school/wp-content/uploads/sites/4/2016/08/parallax1.jpg';
$data['teacher'] = '342';

$data['images'] = array(
        '1' => '1788',
        '2' => '1790',
        '3' => '1791',
        '4' => '1792',
        '5' => '1793',
        '6' => '1794',
        '7' => '1795',
        '8' => '1796',
        '9' => '1797',
        '10' => '1798',
        '11' => '1799',
        '12' => '1800',
        '13' => '1802',
        '14' => '1803',
        '15' => '1804',
        '16' => '1805',
        '17' => '1806',
        '18' => '1807',
        '19' => '1808',
        '20' => '1809',
        '21' => '1810',
        '22' => '1811',
        '23' => '1812',
        '24' => '1813',
        '25' => '1814',
        '26' => '1815',
        '27' => '1816',
        '28' => '1817',
        '29' => '1818',
        '30' => '1819');

$data['teachers'] = array(
        '1' => '332',
        '2' => '338',
        '3' => '334',
        '4' => '1773',
        '5' => '332',
        '6' => '334',
        '7' => '332',
        '8' => '332',
        '9' => '340',
        '10' => '432',
        '11' => '1822',
        '12' => '1822',
        '13' => '1822',
        '14' => '332',
        '15' => '330',
        '16' => '332',
        '17' => '344',
        '18' => '1770',
        '19' => '338',
        '20' => '1822',
        '21' => '432',
        '22' => '1822',
        '23' => '338',
        '24' => '340',
        '25' => '432',
        '26' => '330',
        '27' => '330',
        '28' => '1822',
        '29' => '332',
        '30' => '338');



// START SCRIPT
if(!empty($conn)){
    $startXML = startHeaderXML($data);
    $endXML = endHeaderXML();
    $contentXML = contentXML($conn, $data, $num_courses_page);

    $xml = $startXML . $contentXML . $endXML;
    saveXML($xml);
    downloadXML();
}


function toSlug($string,$space="-") {
    if (function_exists('iconv')) {
        $string = @iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    }
    $string = preg_replace("/[^a-zA-Z0-9 -]/", "", $string);
    $string = mb_strtolower ($string);
    $string = str_replace(" ", $space, $string);
    return $string;
}


function connectDB(){
    // CONNECT TO DATABASE
    $host_name = "db674908698.db.1and1.com";
    $database = "db674908698";
    $user_name = "dbo674908698";
    $password = "toxlIgVu";
    $conn  = mysqli_connect($host_name, $user_name, $password, $database);
    if(mysqli_connect_errno()){
        echo '<p>Error al conectar con servidor MySQL: '.mysqli_connect_error().'</p>';
        return;
    }
    $conn->query("SET NAMES utf8");
    return $conn;
}


function saveXML($xml){
    $file=fopen("export.txt","w") or die("Problemas en la creacion");//En esta linea lo que hace PHP es crear el archivo, si ya existe lo sobreescribe 
    fputs($file, $xml);//En esta linea abre el archivo creado anteriormente e ingresa el resultado de tu script PHP 
    fclose($file);//Finalmente lo cierra  
}


function downloadXML(){
    $ruta="export.txt"; 
    header ("Content-Disposition: attachment; filename=".$ruta); 
    header ("Content-Type: application/octet-stream"); 
    header ("Content-Length: ".filesize($ruta)); 
    readfile($ruta);  
}


function startHeaderXML($data){
    $str = 
        '<?xml version="1.0" encoding="UTF-8" ?>

        <rss version="2.0"
            xmlns:excerpt="http://wordpress.org/export/1.2/excerpt/"
            xmlns:content="http://purl.org/rss/1.0/modules/content/"
            xmlns:wfw="http://wellformedweb.org/CommentAPI/"
            xmlns:dc="http://purl.org/dc/elements/1.1/"
            xmlns:wp="http://wordpress.org/export/1.2/"
        >

        <channel>
            <title>AULA_SMART Editorial</title>
            <link>http://www.aulasmarteditorial.com</link>
            <description>AULA_SMART Editorial</description>
            <pubDate>' . $data['today'] . '</pubDate>
            <language>es-ES</language>
            <wp:wxr_version>1.2</wp:wxr_version>
            <wp:base_site_url>http://www.aulasmarteditorial.com/</wp:base_site_url>
            <wp:base_blog_url>http://www.aulasmarteditorial.com</wp:base_blog_url>

            <wp:author><wp:author_id>1</wp:author_id>
            <wp:author_login><![CDATA[admin]]></wp:author_login>
            <wp:author_email><![CDATA[gabrielvalero@ibericasur.com]]></wp:author_email>
            <wp:author_display_name><![CDATA[admin]]></wp:author_display_name>
            <wp:author_first_name><![CDATA[]]></wp:author_first_name>
            <wp:author_last_name><![CDATA[]]></wp:author_last_name></wp:author>';
        return $str;    
    }

         
function endHeaderXML(){
    $str = '</channel>
            </rss>';
    return $str;
}       


function contentXML($conn, $data, $num_courses_page){
    $str='';
    $sql = "SELECT * FROM tabla_xml";
    $result = $conn->query($sql);



    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
 

            $str.='<item>
                    <title>' . $row["nombre"] . '</title>
                    <pubDate>' . $data['today'] . '</pubDate>
                    <dc:creator><![CDATA[admin]]></dc:creator>
                    <description></description>
                    <content:encoded>
                        <![CDATA[[vc_row][vc_column css=".vc_custom_1467636421128{margin-bottom: 0px !important;}"]';
                    if(!empty($row["capacita"])){
                        $str.='
                        [nd_options_text nd_options_text_tag="h3" nd_options_text_weight="bold" nd_options_text="Capacita para" nd_options_text_color="#727475" nd_options_text_font_size="20" nd_options_text_line_height="20"]
                        [nd_options_spacer nd_options_height="10"]
                        [vc_column_text]' . $row["capacita"] . '[/vc_column_text]
                        [nd_options_spacer nd_options_height="10"]';
                    }
                    if(!empty($row["objetivos"])){
                        $str.='
                        [nd_options_text nd_options_text_tag="h3" nd_options_text_weight="bold" nd_options_text="Objetivos" nd_options_text_color="#727475" nd_options_text_font_size="20" nd_options_text_line_height="20"]
                        [nd_options_spacer nd_options_height="10"]
                        [vc_column_text]' . $row["objetivos"] . '[/vc_column_text]
                        [nd_options_spacer nd_options_height="10"]';
                    }
                    if(!empty($row["dirigido"])){
                        $str.='
                        [nd_options_text nd_options_text_tag="h3" nd_options_text_weight="bold" nd_options_text="Dirigido a" nd_options_text_color="#727475" nd_options_text_font_size="20" nd_options_text_line_height="20"]
                        [nd_options_spacer nd_options_height="10"]
                        [vc_column_text]' . $row["dirigido"] . '[/vc_column_text]
                        [nd_options_spacer nd_options_height="10"]';
                    }
                    if(!empty($row["otrosdatos"])){
                        $str.='
                        [nd_options_text nd_options_text_tag="h3" nd_options_text_weight="bold" nd_options_text="Otros datos" nd_options_text_color="#727475" nd_options_text_font_size="20" nd_options_text_line_height="20"]
                        [nd_options_spacer nd_options_height="10"]
                        [vc_column_text]' . $row["otrosdatos"] . '[/vc_column_text]
                        [nd_options_spacer nd_options_height="10"]';
                    }
                    if(!empty($row["contenido_formativo"])){
                        $str.='
                        [nd_options_text nd_options_text_tag="h3" nd_options_text_weight="bold" nd_options_text="Contenido formativo" nd_options_text_color="#727475" nd_options_text_font_size="20" nd_options_text_line_height="20"]
                        [nd_options_spacer nd_options_height="10"]
                        [vc_column_text]' . $row["contenido_formativo"] . '[/vc_column_text]
                        [nd_options_spacer nd_options_height="10"]';
                    }
                        
                    $str.='
                    [/vc_column][/vc_row]]]>
                    </content:encoded>
                    <excerpt:encoded><![CDATA[' . $row["extracto"] . ']]></excerpt:encoded>
                    <wp:post_date><![CDATA[' . $data['today_short'] . ']]></wp:post_date>
                    <wp:post_date_gmt><![CDATA[' . $data['today_short'] . ']]></wp:post_date_gmt>
                    <wp:comment_status><![CDATA[open]]></wp:comment_status>
                    <wp:ping_status><![CDATA[closed]]></wp:ping_status>
                    <wp:post_name><![CDATA[' . $row["nombre"] . ']]></wp:post_name>
                    <wp:status><![CDATA[publish]]></wp:status>
                    <wp:post_parent>0</wp:post_parent>
                    <wp:menu_order>0</wp:menu_order>
                    <wp:post_type><![CDATA[courses]]></wp:post_type>
                    <wp:post_password><![CDATA[]]></wp:post_password>
                    <wp:is_sticky>0</wp:is_sticky>
                    <category domain="duration-course" nicename="' . toSlug($row["horas"]."-horas") . '"><![CDATA[' . $row["horas"] . ' Horas]]></category>
                    <!-- Tags -->
                    <category domain="post_tag" nicename="' . toSlug($row["nombre_familia"]) . '"><![CDATA[' .$row["nombre_familia"] . ']]></category>
                    <category domain="post_tag" nicename="' . toSlug($row["categoria"]) . '"><![CDATA[' . $row["categoria"] . ']]></category>
                    <!-- Categories -->
                    <category domain="category-course" nicename="' . toSlug($row["nombre_familia"]) . '"><![CDATA[' . $row["nombre_familia"] . ']]></category>
                    <category domain="category-course" nicename="' . toSlug($row["categoria"]) . '"><![CDATA[' . $row["categoria"] . ']]></category>
                    <!-- Levels -->
                    <category domain="difficulty-level-course" nicename="basico"><![CDATA[Básico]]></category>
                    <category domain="difficulty-level-course" nicename="medio"><![CDATA[Medio]]></category>
                    <category domain="difficulty-level-course" nicename="avanzado"><![CDATA[Avanzado]]></category>
                    <!-- Typology -->
                    <category domain="typology-course" nicename="teleformacion"><![CDATA[Teleformación]]></category>
                    <!-- Location -->
                    <category domain="location-course" nicename="aula-smart-editorial"><![CDATA[AULA_SMART Editorial]]></category> 
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[_thumbnail_id]]></wp:meta_key>
                        <wp:meta_value>'. $data['images'][$row["num_familia"]] . '</wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_options_meta_box_page_title]]></wp:meta_key>
                        <wp:meta_value><![CDATA[0]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_options_meta_box_page_margin]]></wp:meta_key>
                        <wp:meta_value><![CDATA[0]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_options_meta_box_post_title]]></wp:meta_key>
                        <wp:meta_value><![CDATA[0]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_options_meta_box_post_margin]]></wp:meta_key>
                        <wp:meta_value><![CDATA[0]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[_vc_post_settings]]></wp:meta_key>
                        <wp:meta_value><![CDATA[a:1:{s:10:"vc_grid_id";a:0:{}}]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[_wpb_shortcodes_custom_css]]></wp:meta_key>
                        <wp:meta_value><![CDATA[.vc_custom_1467636421128{margin-bottom: 0px !important;}]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_options_meta_box_page_title]]></wp:meta_key>
                        <wp:meta_value><![CDATA[0]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_options_meta_box_page_margin]]></wp:meta_key>
                        <wp:meta_value><![CDATA[0]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[_vc_post_settings]]></wp:meta_key>
                        <wp:meta_value><![CDATA[a:1:{s:10:"vc_grid_id";a:0:{}}]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[_edit_last]]></wp:meta_key>
                        <wp:meta_value><![CDATA[1]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_price]]></wp:meta_key>
                        <wp:meta_value><![CDATA[' . $data['price'] . ']]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_max_availability]]></wp:meta_key>
                        <wp:meta_value><![CDATA[' . $data['attendes'] . ']]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_teacher]]></wp:meta_key>
                        <wp:meta_value><![CDATA[' .  $data['teachers'][$row["num_familia"]]  . ']]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_date]]></wp:meta_key>
                        <wp:meta_value><![CDATA[' . $data['date_start'] . ']]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_color]]></wp:meta_key>
                        <wp:meta_value><![CDATA[' . $data['colour'] . ']]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[slide_template]]></wp:meta_key>
                        <wp:meta_value><![CDATA[default]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[_wpb_vc_js_status]]></wp:meta_key>
                        <wp:meta_value><![CDATA[true]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_options_meta_box_post_title]]></wp:meta_key>
                        <wp:meta_value><![CDATA[0]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_options_meta_box_post_margin]]></wp:meta_key>
                        <wp:meta_value><![CDATA[0]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_course_header_img]]></wp:meta_key>
                        <wp:meta_value><![CDATA[' . $data['header_background'] . ']]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_course_header_img_title]]></wp:meta_key>
                        <wp:meta_value><![CDATA[' . $row["nombre_familia"] . ']]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[_wpb_shortcodes_custom_css]]></wp:meta_key>
                        <wp:meta_value><![CDATA[.vc_custom_1467636421128{margin-bottom: 0px !important;}]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_course_header_img_position]]></wp:meta_key>
                        <wp:meta_value><![CDATA[nd_learning_background_position_center_bottom]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_form]]></wp:meta_key>
                        <wp:meta_value><![CDATA[' . $data["form"] . ']]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <!-- Other teachers -->
                        <wp:meta_key><![CDATA[nd_learning_meta_box_teachers]]></wp:meta_key>
                        <wp:meta_value><![CDATA[]]></wp:meta_value> 
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_title_tab]]></wp:meta_key>
                        <wp:meta_value><![CDATA[Temario]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <wp:meta_key><![CDATA[nd_learning_meta_box_title_tab_content]]></wp:meta_key>
                        <wp:meta_value><![CDATA[Nuestro Temario]]></wp:meta_value>
                    </wp:postmeta>
                    <wp:postmeta>
                        <!-- Documentation -->
                        <wp:meta_key><![CDATA[nd_learning_meta_box_docs_courses]]></wp:meta_key>
                        <wp:meta_value><![CDATA[]]></wp:meta_value>
                    </wp:postmeta>
                </item>';


        }
        return $str;
    }
}
?>