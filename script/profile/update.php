<?php
if ( !isset($wp_did_header) ) {

    $wp_did_header = true;

    require_once('/homepages/40/d243808874/htdocs/aulasmarteditorial.com/wp-load.php' );

    wp();

    require_once( ABSPATH . WPINC . '/template-loader.php' );

}
session_start();


if (!function_exists('redirect')) {
	function redirect($path = '') {
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $path;
		echo "<script type='text/javascript'>window.location='" . $url . "'</script>";
		exit();
	}
}

function updateSession() {
	$conn = connectDBCampus();
	$sql = "SELECT * FROM alumnos WHERE idalumnos = '" . $_SESSION['userCampus']['idalumnos'] . "'";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		if ($user = $result->fetch_assoc()) {
			$_SESSION['userCampus'] = $user;
		}
	}
}

function correctDateToDB($date) {
	if ($date) {
		$arrayDate = explode('/', $date);
		$day = $arrayDate[0];
		$month = $arrayDate[1];
		$year = $arrayDate[2];

		$arrayDate[1] = $day;
		$arrayDate[2] = $month;
		$response = implode('-', $arrayDate);
		return $year . '-' . $month . '-' . $day;
	}
	return $date;
}
if (!function_exists('notification')) {
	function notification($status, $msg) {
		$_SESSION['notification']['msg'] = $msg;
		$_SESSION['notification']['status'] = $status;
	}
}

// Si se esta actualizando el perfil y estamos logueadocomo usuario del Campus
if ($_POST && (!empty($_POST['action'])) && !empty($_SESSION['userCampus'])) {
	$conn = connectDBCampus();
	switch ($_POST['action']) {
		case 'tab-personal':
			// ARREGLAR CON CALENDARIO
			$borndate = correctDateToDB($_POST['borndate']);
			$sql = "UPDATE alumnos
					SET nombre='" . $_POST['name'] . "', apellidos='" . $_POST['lastname'] . "',dni='" . $_POST['nif'] . "',f_nacimiento='" . $borndate . "',idsexo='" . $_POST['genre'] . "'
					WHERE idalumnos=" . $_SESSION['userCampus']['idalumnos'];
			break;
		case 'tab-contact':
			$poblation = ucfirst(mb_strtolower($_POST['poblation']));
			$city = ucfirst(mb_strtolower($_POST['city']));
			$country = ucfirst(mb_strtolower($_POST['country']));
			$sql = "UPDATE alumnos
					SET telefono='" . $_POST['telephone'] . "', telefono_secundario='" . $_POST['telephone2'] . "',direccion='" . $_POST['address'] . "',cp='" . $_POST['zip'] . "',poblacion='" . $poblation . "',provincia='" . $city . "',pais='" . $country . "'
					WHERE idalumnos=" . $_SESSION['userCampus']['idalumnos'];
			break;
		case 'tab-pass':
			if ($_POST['pass1'] == $_POST['pass2']) {
				$sqlSelect = "SELECT * FROM alumnos WHERE email = '" . $_SESSION['userCampus']['email'] . "' AND pass = '" . md5($_POST['pass']) . "'";
				$result = $conn->query($sqlSelect);
				if ($result->num_rows > 0) {
					if ($user = $result->fetch_assoc()) {
						print_R('si ' . $sql);
						$sql = "UPDATE alumnos
							SET pass='" . md5($_POST['pass1']) . "'
							WHERE idalumnos=" . $_SESSION['userCampus']['idalumnos'];
					}
				}
			}
			break;
		case 'tab-avatar':
			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["avatar"]["name"]);
			$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
			if (!empty($_FILES)) {
				$check = getimagesize($_FILES["avatar"]["tmp_name"]);
				if ($check !== false) {
					$dir_subida = '/homepages/40/d243808874/htdocs/aulasmarteditorial.com/wp-content/uploads/avatars/';
					$_FILES['avatar']['uniqid'] = uniqid('_');
					$fileName = $_FILES['avatar']['uniqid'] . basename($_FILES['avatar']['name']);
					$fichero_subido = $dir_subida . $fileName;

					if (move_uploaded_file($_FILES['avatar']['tmp_name'], $fichero_subido)) {
//						// Prepare remote upload data
						$uploadRequest = array(
							'fileName' => $fileName,
							'fileData' => base64_encode(file_get_contents($fichero_subido))
						);
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, "http://campusaulainteractiva.aulasmart.net/uploadAvatarRemote.php");
						curl_setopt($curl, CURLOPT_TIMEOUT, 30);
						curl_setopt($curl, CURLOPT_POST, 1);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($curl, CURLOPT_POSTFIELDS, $uploadRequest);
						$response = curl_exec($curl);
						curl_close($curl);

						if ($response == 'OK') {
							$sql = "UPDATE alumnos
							SET foto='" . $fileName . "'
							WHERE idalumnos=" . $_SESSION['userCampus']['idalumnos'];
						} else {
							notification('error',__('NoGuardarImagen', 'custom-translation'));
						}
						// Now delete local temp file
						unlink($fichero_subido);
					} else {
						notification('error',__('NoGuardarImagen', 'custom-translation'));
					}
				}
			}
	}

	if (!empty($sql)) {
		if ($conn->query($sql) === TRUE) {
			notification('success',__('SiPerfil', 'custom-translation'));
			updateSession();
		}
	}

	redirect('mi-perfil');
} else {
	redirect('mi-perfil');
}
?>